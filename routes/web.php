<?php

Route::get('/', [
    'middleware'=>'auth',
    'as' => 'Dashboard',
    'uses' => 'AdminController@dashboard'
]);

Route::get('/signupdata', [
    'middleware'=>'auth',
    'as' => 'Dashboard Sign Up Data',
    'uses' => 'AdminController@signupdata'
]);

Route::get('/actiondata', [
    'middleware'=>'auth',
    'as' => 'Dashboard Action Data',
    'uses' => 'AdminController@actiondata'
]);

Route::get('/home',[
    'middleware'=>'auth',
    'as' => 'Home',
    'uses' => 'AdminController@dashboard'
]); 

Auth::routes();
Route::get('/signin', ['as' => 'login', 'uses' => 'Auth\LoginController@loginView']);
Route::post('/signin', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
Route::get('/forgot-password', ['as' => 'Forgot Password','uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::get('/reset-password/{token}', ['as' => 'Reset Password','uses' => 'Auth\ResetPasswordController@showResetForm']);
Route::get('/notification',[
    'as' => 'Notification List',
    'uses' => 'NotificationController@listNotification'
]);
Route::group(['prefix' => 'profile','middleware'=>'auth'], function () {
    Route::get('/',[
        'as' => 'Profile',
        'uses' => 'AdminController@profile'
    ]); 

    Route::get('/changepassword',[
        'as' => 'Change Password',
        'uses' => 'AdminController@change_password'
    ]); 

    Route::post('/update/profile',[
        'as' => 'Profile Update',
        'uses' => 'AdminController@update_profile'
    ]);

    Route::post('/photo',[
        'as' => 'My Profile Photo',
        'uses' => 'AdminController@update_avatar'
    ]);

    Route::post('/update/password',[
        'as' => 'Password Update',
        'uses' => 'AdminController@update_password'
    ]);
});

Route::group(['prefix' => 'categories','middleware'=>'auth'], function () {
    Route::get('/',[
        'as' => 'Categories',
        'uses' => 'CategoriesController@categories'
    ]);

    Route::get('/label/{type}',[
        'as' => 'Categories Label',
        'uses' => 'CategoriesController@categoriesByLabel'
    ]);

    Route::get('/manage/{category_slug?}',[
        'as' => 'Manage Categories',
        'uses' => 'CategoriesController@mngCategories'
    ]); 

    Route::post('/action/upload_cat_image',[
        'as' => 'Upload Categories Image',
        'uses' => 'CategoriesController@catImageStore'
    ]); 

    Route::post('/action/delete_cat_image',[
        'as' => 'Delete Categories Image',
        'uses' => 'CategoriesController@imageDestroy'
    ]); 

    Route::delete('/delete',[
        'as' => 'Delete Categories',
        'uses' => 'CategoriesController@delCategories'
    ]);

    Route::post('insertUpdate',[
        'as' => 'Action Manage Categories',
        'uses' => 'CategoriesController@saveCategories'
    ]);
});

Route::group(['prefix' => 'feature-image','middleware'=>'auth'], function () {
    Route::get('/',[
        'as' => 'Feature Image',
        'uses' => 'FeatureImageController@feature_image'
    ]);

    Route::post('/action/upload_feature_image',[
        'as' => 'Upload Feature Image',
        'uses' => 'FeatureImageController@upload_feature_image'
    ]); 

    Route::post('/action/save_feature_image',[
        'as' => 'Save Feature Image',
        'uses' => 'FeatureImageController@save_feature_image'
    ]); 
});

Route::group(['prefix' => 'brands','middleware'=>'auth'], function () {
    Route::get('/{status?}',[
        'as' => 'Brands',
        'uses' => 'BrandsController@brands'
    ]); 

    Route::get('/action/add',[
        'as' => 'Add Brands',
        'uses' => 'BrandsController@add_brands'
    ]); 

    Route::get('/action/update/{id?}',[
        'as' => 'Update Brands',
        'uses' => 'BrandsController@update_brands'
    ]);

    Route::post('/action/upload_brand_images',[
        'as' => 'Upload Brand Image',
        'uses' => 'BrandsController@upload_brand_images'
    ]); 

    Route::post('/action/save_brand',[
        'as' => 'Save Brands',
        'uses' => 'BrandsController@save_brands'
    ]); 

    Route::post('/action/delete_brand',[
        'as' => 'Delete Brands',
        'uses' => 'BrandsController@delete_brands'
    ]);

    Route::get('/action/export/{type?}',[
        'as' => 'Export Brands',
        'uses' => 'UserController@export_brands'
    ]);
});

Route::group(['prefix' => 'labels','middleware'=>'auth'], function () {
    Route::get('/products/{type?}/{status?}',[
        'as' => 'Products',
        'uses' => 'LabelsController@products'
    ]);

    Route::get('/products/status/{status?}',[
        'as' => 'Products By Status',
        'uses' => 'LabelsController@products'
    ]);

    Route::post('/action/upload_product_image',[
        'as' => 'Upload Product Image',
        'uses' => 'LabelsController@prodImageStore'
    ]); 

    Route::post('/action/delete_product_image',[
        'as' => 'Delete Product Image',
        'uses' => 'LabelsController@imageDestroy'
    ]); 

    Route::post('/action/upload_product_main_image',[
        'as' => 'Upload Product Main Image',
        'uses' => 'LabelsController@prodMainImageStore'
    ]); 

    Route::post('/action/delete_product_main_image',[
        'as' => 'Delete Product Main Image',
        'uses' => 'LabelsController@imageMainDestroy'
    ]); 

    Route::post('/product_update/',[
        'as' => 'PostManageUpdateProduct',
        'uses' => 'LabelsController@mngProd'
    ]);

    Route::post('/product_remove/',[
        'as' => 'PostDeleteProduct',
        'uses' => 'LabelsController@delProd'
    ]);

    

    Route::get('/services',[
        'as' => 'Services',
        'uses' => 'LabelsController@services'
    ]); 

    Route::get('/movies',[
        'as' => 'Movies',
        'uses' => 'LabelsController@movies'
    ]); 

    Route::get('/edit/{product_service_id?}',[
        'as' => 'Manage Product',
        'uses' => 'LabelsController@manageProduct'
    ]);

    Route::get('/pending/edit/{pending_product_service_id}',[
        'as' => 'Manage Product Pending',
        'uses' => 'LabelsController@manageProductPending'
    ]);
    
    Route::post('/pending/add',[
        'as' => 'PostAddPendingProduct',
        'uses' => 'LabelsController@mngProd'
    ]);
    
    Route::post('/pending/reject',[
        'as' => 'PostRejectPendingProduct',
        'uses' => 'LabelsController@rejectPendingProduct'
    ]);

    Route::get('/action/update_product/{id?}',[
        'as' => 'Update Product',
        'uses' => 'LabelsController@update_product'
    ]); 

    Route::get('/action/update_service/{id?}',[
        'as' => 'Update Service',
        'uses' => 'LabelsController@update_service'
    ]); 

    Route::get('/action/update_movie/{id?}',[
        'as' => 'Update Movie',
        'uses' => 'LabelsController@update_movie'
    ]); 

    Route::delete('/action/delete_product/',[
        'as' => 'Delete Product',
        'uses' => 'LabelsController@delete_product'
    ]);

    Route::post('/action/delete_service/',[
        'as' => 'Delete Service',
        'uses' => 'LabelsController@delete_service'
    ]);

    Route::post('/action/delete_movie/',[
        'as' => 'Delete Movie',
        'uses' => 'LabelsController@delete_movie'
    ]);

    Route::get('/action/export_product/{type?}',[
        'as' => 'Export Product',
        'uses' => 'LabelsController@export_product'
    ]);

    Route::post('/action/save_product',[
        'as' => 'Save Product',
        'uses' => 'LabelsController@save_product'
    ]); 

    Route::post('/action/save_question',[
        'as' => 'Save Service',
        'uses' => 'LabelsController@save_question'
    ]); 

    Route::post('/action/save_movie',[
        'as' => 'Save Movie',
        'uses' => 'LabelsController@save_movie'
    ]); 

});

Route::group(['prefix' => 'actions','middleware'=>'auth'], function () {
    Route::get('/reviews/{status?}',[
        'as' => 'Reviews',
        'uses' => 'ActionController@reviews'
    ]); 
    
    Route::get('/questions/{status?}',[
        'as' => 'Questions',
        'uses' => 'ActionController@questions'
    ]);

    Route::get('/comments/{status?}',[
        'as' => 'Comments',
        'uses' => 'ActionController@comments'
    ]);

    Route::post('/action/update_review/',[
        'as' => 'Update Review',
        'uses' => 'ActionController@update_review'
    ]); 

    Route::post('/action/update_question/',[
        'as' => 'Update Question',
        'uses' => 'ActionController@update_question'
    ]); 

    Route::post('/action/update_comment/',[
        'as' => 'Update Comment',
        'uses' => 'ActionController@update_comment'
    ]); 

    Route::post('/action/delete_review/',[
        'as' => 'Delete Review',
        'uses' => 'ActionController@delete_review'
    ]);

    Route::post('/action/delete_question/',[
        'as' => 'Delete Question',
        'uses' => 'ActionController@delete_question'
    ]);

    Route::post('/action/delete_comment/',[
        'as' => 'Delete Comment',
        'uses' => 'ActionController@delete_comment'
    ]);


    //{status?}
    Route::get('/action/export_review/{type?}',[
        'as' => 'Export Review',
        'uses' => 'ActionController@export_review'
    ]);

    Route::get('/action/export_question/{type?}',[
        'as' => 'Export Question',
        'uses' => 'ActionController@export_question'
    ]);

    Route::get('/action/export_comment/{type?}',[
        'as' => 'Export Comment',
        'uses' => 'ActionController@export_comment'
    ]);
});

Route::group(['prefix' => 'user','middleware'=>'auth'], function () {
    Route::get('/{status?}',[
        'as' => 'User',
        'uses' => 'UserController@user'
    ]);

    Route::get('/action/add',[
        'as' => 'Add User',
        'uses' => 'UserController@add_user'
    ]); 

    Route::get('/action/update/{email?}',[
        'as' => 'Update User',
        'uses' => 'UserController@update_user'
    ]); 

    Route::post('/action/block/',[
        'as' => 'Block User',
        'uses' => 'UserController@block_user'
    ]);

    Route::get('/action/export/{type?}',[
        'as' => 'Export User',
        'uses' => 'UserController@export_user'
    ]);

    Route::post('/action/save',[
        'as' => 'Save User',
        'uses' => 'UserController@save_user'
    ]); 

});

Route::group(['prefix' => 'admin','middleware'=>['auth', 'App\Http\Middleware\SuperAdminMiddleware']], function () {
    Route::get('/{status?}',[
        'as' => 'Admin',
        'uses' => 'AdminController@admin'
    ]);

    Route::get('/action/add',[
        'as' => 'Add Admin',
        'uses' => 'AdminController@add_admin'
    ]); 

    Route::get('/action/update/{email?}',[
        'as' => 'Update Admin',
        'uses' => 'AdminController@update_admin'
    ]); 

    Route::post('/action/block',[
        'as' => 'Block Admin',
        'uses' => 'AdminController@block_admin'
    ]);

    Route::post('/action/save',[
        'as' => 'Save Admin',
        'uses' => 'AdminController@save_admin'
    ]); 
});

Route::group(['prefix' => 'spam-filter','middleware'=>'auth'], function () {
    Route::get('/',[
        'as' => 'Spam Filter',
        'uses' => 'SpamController@spam_filter'
    ]); 
    Route::post('/update',[
        'as' => 'Spam Filter Save',
        'uses' => 'SpamController@saveSpamFilter'
    ]); 
});
