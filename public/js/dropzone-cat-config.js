Dropzone.options.imageDropzone = {
    autoDiscover:false,
    autoProcessQueue: true,
    uploadMultiple: false,
    parallelUploads: 1,
    previewTemplate: document.querySelector('#preview').innerHTML,
    addRemoveLinks: true,
    dictRemoveFile: 'Remove',
    maxFilesize: 1,
    dictFileTooBig: 'Image is larger than 2MB',
    timeout: 10000,
    maxFiles:1,
    acceptedFiles:'image/*',
    url:uploadUrl,
    params: {_token: $('meta[name="csrf-token"]').attr('content')},
    init: function () {
        var myDropzone = this;
        if(preloadImage !== "") {
                let mockFile = {
                    accepted: true,
                    dataURL: preloadImage,
                };

                myDropzone.files.push(mockFile);
                myDropzone.emit("addedfile", mockFile);
                myDropzone.createThumbnailFromUrl(
                    mockFile,
                    myDropzone.options.thumbnailWidth,
                    myDropzone.options.thumbnailHeight,
                    myDropzone.options.thumbnailMethod,
                    true,
                    function(thumbnail) {
                        myDropzone.emit('thumbnail', mockFile, thumbnail);
                        myDropzone.emit("complete", mockFile);
                    }
                );

        }
        this.on("removedfile", function (file) {
            $.post({
                url: delUrl,
                data: {id: file.name, _token: $('meta[name="csrf-token"]').attr('content')},
                dataType: 'json'
            });
        });
    }
};
