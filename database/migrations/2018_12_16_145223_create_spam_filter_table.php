<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
class CreateSpamFilterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('spam_filter')){
            Schema::create('spam_filter', function($table) {
                $table->increments('spam_filter_id');
                $table->string('text',200);
                $table->integer('rate',3)->default(0);
                $table->integer('type',1)->default(0)->comment('0=Abusive,1=Fake');
                $table->enum('status', ['0','1','2'])->default(1)->comment('0=Idle,1=Active,2=Deleted');
                $table->timestamps();
           });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spam_filter');
    }
}
