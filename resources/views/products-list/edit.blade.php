@extends( 'layouts.main' )
@section( 'headCss' )
<link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">

@endsection
<!-- Additional JS -->
@section( 'headJs' )
	<!-- Javascript -->
@endsection
@section( 'content' )
	<!-- Content -->
	<!-- Page Title Area -->
	<div class="row page-title clearfix">
		<div class="page-title-left">
			<h5 class="mr-0 mr-r-5">Product Details</h5>
		</div>
		<!-- /.page-title-left -->
	</div>
<!-- /.page-title -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<style>
@media (max-width: 575px) {
	.product_photos_wrapper .product_photos__wrapper {margin-bottom: 1.5625rem;}
	.product_photos_wrapper .product_photos__wrapper .form-group {margin: 0px;}
}
.newAttr{
	background:#f4f9ff;
}
.table-add a{
	font-size:22px;
}
</style>
<div class="widget-list">
	<div class="row">
		<div class="col-lg-8">
			@if(Session::get('errors')||count( $errors ) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
						<i class="icon-remove-sign"></i> {{ $error }} </br>
					@endforeach
				</div>
			@endif
			<form method="post" action="{{route('PostDeleteProduct')}}" id="delete_product">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="psid" value="{{ $product_services_id }}">
			</form>
			<form method="post" action="{{route('PostManageUpdateProduct')}}" id="manage_product">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="psid" value="{{ $product_services_id }}">
				<input type="hidden" name="key" value="">
				<input type="hidden" name="val" value="">
				<div class="form-group">
					<input class="form-control" placeholder="Product Name" name="product_name" type="text" @if(!empty($productDetail))value="{{$productDetail->product_service_title}}"@endif>
				</div>
				<div class="form-group">
					<input class="form-control" placeholder="Product Description" name="product_desc" type="text" @if(!empty($productDetail))value="{{$productDetail->product_service_desc}}"@endif>
				</div>
				<div class="form-group">
                    <div class="widget-holder">
                        <div class="widget-bg" id="product_spec">
                            <div class="widget-heading clearfix">
								<h6 class="mb-2 mt-0">Product Details</h6>
                            </div>
                                <table class="table table-editable table-responsive">
								<span class="table-add float-right mb-3 mr-2"><a href="#!" class="text-success">+</a></span>
                                    <thead>
                                        <tr>
                                            <th data-editable >Specification</th>
                                            <th data-editable >Details</th>
                                        </tr>
                                    </thead>
                                    <tbody name="test">
									@if(!empty($productDetail))
										@foreach($productDetail->jsonAttribute as $key => $value)
											<tr class='attr'>
												<td contenteditable="true">{!!ucfirst($key)!!}</td>
												<td contenteditable="true">
													@if(is_array($value))
														@foreach($value as $val)
															{{$val}}
														@endforeach
													@else 
														{{$value}}
													@endif
												</td>
											</tr>
										@endforeach
									@endif
									<!-- This is our clonable table line -->
									<tr class="hidden">
											<td contenteditable="true">-</td>
											<td contenteditable="true">-</td>
										</tr>
                                    </tbody>
                                </table>
                            </div>
				</div>
			</div>
			
			<!--<div class="card mb-3">
				<div class="card-header">
					<h6 class="m-0">Official Website Link</h6>
				</div>
				<div class="card-body">
				<div class="form-group m-0">
					<input class="form-control" name='url' placeholder="www.apple.com" type="text">
				</div>
				</div>
			</div>-->
			<div class="card mb-3 product_photos_wrapper">
				<div class="card-body">
						<div class="form-group">
							<label for="text"><small class="nott"></small>Product Main Image (1) [Upload To Replace]</label>
							<div id="product_main_image_dropzone" class="dropzone">
								<input type="file" name="product_main_image" class="hidden">
							</div>
							<div id="preview" style="display: none;">
								<div class="dz-preview dz-file-preview">
									<div class="dz-image"><img data-dz-thumbnail /></div>
									<div class="dz-details">
									</div>
									<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
									<div class="dz-error-message"><span data-dz-errormessage></span></div>
									<div class="dz-success-mark">
										<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
											<!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
											<title></title>
											<desc></desc>
											<defs></defs>
											<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
												<path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
											</g>
										</svg>
						
									</div>
									<div class="dz-error-mark">
										<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
											<!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
											<title></title>
											<desc></desc>
											<defs></defs>
											<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
												<g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
													<path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
												</g>
											</g>
										</svg>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
			<div class="card mb-3 product_photos_wrapper">
				<div class="card-body">
						<div class="form-group">
							<label for="text"><small class="nott"></small>Product Image (Max 5) [Upload To Replace]</label>
							<div id="product_image_dropzone" class="dropzone">
								<input type="file" name="product_image" class="hidden">
							</div>
							<div id="preview" style="display: none;">
								<div class="dz-preview dz-file-preview">
									<div class="dz-image"><img data-dz-thumbnail /></div>
									<div class="dz-details">
									</div>
									<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
									<div class="dz-error-message"><span data-dz-errormessage></span></div>
									<div class="dz-success-mark">
										<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
											<!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
											<title></title>
											<desc></desc>
											<defs></defs>
											<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
												<path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
											</g>
										</svg>
						
									</div>
									<div class="dz-error-mark">
										<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
											<!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
											<title></title>
											<desc></desc>
											<defs></defs>
											<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
												<g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
													<path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
												</g>
											</g>
										</svg>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card mb-3">
				<div class="card-header">
					<h6 class="m-0">Brands</h6>
				</div>
				<div class="card-body">
				<select id='brands_sel' name="brand">
					<option value="0">-</option>
					@foreach($arrBrands as $key => $brands)
						@if(!empty($productDetail))
							 @if($key == $productDetail->brands_id)
								<option value="{{$key}}" selected>{{$brands}}</option>
							@else
								<option value="{{$key}}">{{$brands}}</option>
							@endif
						@else
							<option value="{{$key}}">{{$brands}}</option>
						@endif
					@endforeach
				</select>
				</div>
				<div class="card-action"><a href="{{route('Add Brands')}}" target="_blank" class="card-link fw-500">+ Add New Brand</a>
				</div>
			</div>
			<div class="card mb-3">
			<div class="card-header">
				<h6 class="m-0">Product Categories</h6>
			</div>
			<div class="card-body">
				<select id='categories_sel' name="category">
					<option value="0">-</option>
					@foreach($arrCategories as $key => $categoriesList)
						<optgroup label='{{ucfirst($key)}}'>
							@foreach($categoriesList as $categories)
								@if(!empty($productDetail))
									@if($categories->categories_id == $productDetail->category_id)
										<option value="{{$categories->categories_id}}" selected>{{$categories->categories_name}}</option>
									@else
										<option value="{{$categories->categories_id}}">{{$categories->categories_name}}</option>
									@endif
								@else
									<option value="{{$categories->categories_id}}">{{$categories->categories_name}}</option>
								@endif
							@endforeach
						</optgroup>
					@endforeach
				</select>
			</div>
			<div class="card-action"><a href="{{route('Manage Categories','')}}" target="_blank" class="card-link fw-500">+ Add New Category</a>
			</div>
			</div>
			<div class="text-right">
			<button class="btn btn-color-scheme btn-save">Save Changes</button> 
			<button class="btn btn-outline-color-scheme btn-delete">Delete</button>
			<button class="btn btn-outline-color-scheme" onclick='window.history.back();return false;'>Back</button>
			</div>
		</div>
	</form>
	</div>
</div>

@endsection

@section('tailJs')
<script>
	var delUrl = "{{route('Delete Product Image')}}";
	var uploadUrl = "{{ route('Upload Product Image') }}";
	var delMainUrl = "{{route('Delete Product Main Image')}}";
	var uploadMainUrl = "{{ route('Upload Product Main Image') }}";
	var preloadImage =  '';
	var preloadImageMain =  '';

</script>
<script src="{{ asset('js/dropzone.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/dropzone-product-config.js') }}"></script>	
<script>
	
	$(document).ready(function() {
		$('#brands_sel').select2();
		$('#categories_sel').select2();
	});

	$(function() {
	    var mockFile = { name: "banner2.jpg", size: 12345 };
	    var myDropzone = $(
	    	"#product_main_image_dropzone");
	    myDropzone.options.addedfile.call(myDropzone, mockFile);
	    myDropzone.options.thumbnail.call(myDropzone, mockFile, "wikabo.com/images/product/MQ==/_image(43).png");
	})

	var $TABLE = $('#product_spec');
	$('.table-add').on('click',function () {
		var $clone = $TABLE.find('tr.hidden').clone(true).removeClass('hidden').addClass('newAttr attr');
		$TABLE.find('table').append($clone);
	});
	$('.btn-delete').on('click',function(e){
		e.preventDefault();
		$('#delete_product').submit();
	});
	$('.btn-save').on('click',function(e){
		e.preventDefault();
		var i = 0;
		var key = [];
		var val = [];
		
		$("[name='test']").find('td').each(function(){
			if(i == 0 ){
				let str = $(this).find('span').html();
				if(str != "" && typeof(str) == "string"){
					key.push(str.trim().allTrim());
					i =1;
				} else {
					str = $(this).html();
					let num = str.indexOf("<input");
					if(num != -1 ){
						str = str.substr(0,num);
						key.push(str.trim().allTrim());
						i = 1;
					} else {
						i = 2;	
					}
				}
			} else if(i == 1 ){
				let str = $(this).find('span').html();
				if(str != "" && typeof(str) == "string"){
					val.push(str.trim().allTrim());
					i = 2;
				} else {
					str = $(this).html();
					let num = str.indexOf("<input");
					if(num != -1 ){
						str = str.substr(0,num);
						val.push(str.trim().allTrim());
						i = 2;
					}
				}
				i = 2;
			 } else {
				i = 0;
			}
		});
		var objKey = Object.assign({}, key);
		var objVal = Object.assign({}, val);
		$("[name='key']").val(JSON.stringify(objKey));
		$("[name='val']").val(JSON.stringify(objVal));
		$('#manage_product').submit();
	});
	String.prototype.allTrim = String.prototype.allTrim ||
		function(){
			return this.replace(/\s+/g,' ')
					.replace(/^\s+|\s+$/,'');
		};
</script>
@endsection