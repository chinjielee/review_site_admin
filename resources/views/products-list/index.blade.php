@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
<!-- Additional JS -->
@section( 'headJs' )
	<!-- Javascript -->
@endsection
@section( 'content' )
	<!-- Content -->
	<!-- Page Title Area -->
	<div class="row page-title clearfix">
		<div class="page-title-left">
			<h5 class="mr-0 mr-r-5">{{$pageInfo->page}} List</h5>
		</div>
		<!-- /.page-title-left -->
	</div>
<!-- /.page-title -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="widget-list">
	<div class="row">
		<div class="col-md-7">
			<a class="btn btn-default" href="{{route('Export Product',$type,$status)}}"><i class="material-icons list-icon">get_app</i> Export All .CSV</a>
			<a class="btn btn-default" href="{{route('Manage Product','','')}}"><i class="material-icons list-icon">add</i> Add New</a>
			<a class="btn btn-default btn-del" href="#"><i class="material-icons list-icon">delete</i> Delete</a>
		</div>
		<!-- <div class="col-md-5">
			<div class="input-group">
				<select class="form-control">
					<option>Filter Category</option>
					<option>Option 2</option>
					<option>Option 3</option>
					<option>Option 4</option>
					<option>Option 5</option>
				</select>
				<span class="input-group-btn"><a class="btn btn-default" href="#">Filter</a></span>
			</div>
		</div> -->
	</div>
	
	<div class="row mb-2">
	
		<div class="col-md-7">
			<p class="mt-3 mb-0">
				<a href="{{route('Products',[$type,0])}}">All</a> <span class="mr-lr-10">|</span> <a href="{{route('Products',[$type,1])}}">Published</a> <span class="mr-lr-10">|</span> <a href="{{route('Products',[$type,3])}}">Drafts</a> <span class="mr-lr-10">|</span> <a href="{{route('Products',[$type,2])}}">Blocked</a>
			</p>
		</div>
	<div class="col-md-5 text-right">
	<div class="predefinedRanges mr-tb-10" style="cursor: pointer" data-plugin-options='{"opens": "right"}'><span></span>  <i class="list-icon material-icons">expand_more</i>
                                        </div>
	</div>        
	</div>  
	<div class="widget-bg">
		<div class="widget-body clearfix">
			@if (Session::has('success'))
				<div class="alert alert-success">
					{!! Session::get('success') !!}
				</div>
			@endif
			<table class="table table-striped table-responsive" data-toggle="datatables">
				@if(count($arrProductList) > 0 )
					<thead>
						<tr>
							<th class="bs-checkbox" data-field="state" tabindex="0">
								<div class="th-inner"><input class="sel-all" name="btSelectAll" type="checkbox">
								</div>
								<div class="fht-cell"></div>
							</th>
							@if($status == 3)
								<th>Name</th>
								<th>Brand Name</th>
								<th>Category</th>
								<th>Date</th>
								<th>Status</th>
							@else
								<th>Image</th>
								<th>Name</th>
								<th>Brand</th>
								<th>Category</th>
								<th>Reviews</th>
								<th>Questions</th>
								<th>Date</th>
								<th>Action</th>
							@endif
						</tr>
					</thead>
				@endif
				<tbody>
					@if($status == 3)
						@forelse ($arrProductList as $product)
						<tr>
							<td class="bs-checkbox"><input data-index="0" name="btSelectItem" value="{{$product->pending_product_service_id}}" type="checkbox">
							</td>
							<td>{{$product->product_name}}
							</td>
							<td>@if(!empty($product->brands_id)){{$arrBrands[$product->brands_id]}}@endif
							</td>
							@if(empty($product->category_id))
								<td>{{$product->category_name}}
							@else
								<td>@if(isset($arrCategories[$product->category_id])){{$arrCategories[$product->category_id]->categories_name}}@endif
							@endif
							</td>
							<td>Pending
							<br><small>{{$product->updated_at->diffForHumans()}}</small>
							</td>
							<td>
							<a href='{{route("Manage Product Pending","$product->pending_product_service_id")}}' class="btn btn-default btn-sm">Edit</a><button class="btn btn-default btn-sm btn-single-delete"data-id = "{{$product->pending_product_service_id}}"><i class="material-icons list-icon">delete</i></button>
							</td>
						</tr>
						@empty
						<tr>
							No results
						</tr>
						@endforelse
					@else
						@forelse ($arrProductList as $product)
							<tr>
								<td class="bs-checkbox"><input data-index="0" name="btSelectItem" value="{{$product->product_services_id}}" type="checkbox">
								</td>
								<td>
									@if(empty($product->images))
										<img class="mr-2" width="60" src="{{ asset('/img/default.jpg')}}">
									@else
										<img class="mr-2" width="60" src="{{asset('/images/product')}}/{{$product->display_id}}/{{$product->image}}">
									@endif
								</td>
								<td>{{$product->product_service_title}}
								</td>
								<td>@if(!empty($product->brands_id)){{$arrBrands[$product->brands_id]}}@endif
								</td>
								<td>@if(isset($arrCategories[$product->category_id])){{$arrCategories[$product->category_id]->categories_name}}@endif
								</td>
								<td>{{$product->reviews_count}}
								</td>
								<td>{{$product->questions_count}}
								</td>
								<td>
									@if($product->status == 1)
										Published
									@elseif($product->status == 2)
										Blocked
									@else
										Pending
									@endif
								<br><small>{{$product->updated_at->diffForHumans()}}</small>
								</td>
								<td>
								<a href='{{route("Manage Product","$product->product_services_id")}}' class="btn btn-default btn-sm">Edit</a><button class='btn-single-delete' data-id="{{$product->product_services_id}}"><i class="material-icons list-icon">delete</i></button>
								</td>
							</tr>
							@empty
							<tr>			
								No results
							</tr>
						@endforelse
					@endif
				</tbody>
			</table>
		</div>
		<!-- /.widget-body -->
	</div>
</div>

@endsection

@section('tailJs')
	<script>
		$( document ).ready(function() {
			enablePredefinedDateRangePickerQuestion();
		});
		
		$('.btn-del').on('click',function(e){
			let objItems = [] ;
			e.preventDefault();
			$.each($("[name='btSelectItem']:checked"), function(){
				objItems.push(this.value);
			});
			let data = JSON.stringify(objItems);
			return swal({
				title: 'Are you sure?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete'
			}).then(function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						method: "DELETE",
						url: '{{route("Delete Product")}}',
						data: {
							_token: $('meta[name="csrf-token"]').attr('content'),
							data: data,
						},
					}).done(function(data) {
						if(data == "done"){
							swal({
								title:'Delete Success',
								type: 'info'
							}).then(function(){
								window.location.href = "{{route('Products',$type)}}";
							});
						} else {
							swal({
								title:'Error Occur, try again later',
								type: 'warning'
							});
						}
					});
				}
			});
		});

		$('.btn-single-delete').on('click',function(e){
			let objItems = [] ;
			e.preventDefault();
			$this = $(this);
			let datId = $this.attr('data-id');
			if(datId> 0){			
				return swal({
					title: 'Are you sure?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete'
				}).then(function(isConfirm) {
					if (isConfirm) {
						$.ajax({
							method: "DELETE",
							url: '{{route("Delete Product")}}',
							data: {
								_token: $('meta[name="csrf-token"]').attr('content'),
								data: datId,
							},
						}).done(function(data) {
							if(data == "done"){
								swal({
									title:'Delete Success',
									type: 'info'
								}).then(function(){
									window.location.href = "{{route('Products',$type)}}";
								});
							} else {
								swal({
									title:'Error Occur, try again later',
									type: 'warning'
								});
							}
						});
					}
				});
			} else {
				return false;
			}
		});

		$('.sel-all').on('click',function(e){
			if($(this).is(':checked')){
				$("[name='btSelectItem']").prop('checked', true);
			} else {
				$("[name='btSelectItem']").prop('checked', false);
			}
		});

		function enablePredefinedDateRangePickerQuestion(){
			var $el = $('.predefinedRanges');
			if ( !$el.length ) return;
			var defaults = {
				locale: {
				format: 'YYYY-MM-DD',
				},
				startDate: moment().startOf('month'),
				endDate: moment().endOf('month'),
				opens: "left",
				ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				}
			};
			$el.each( function() {
				var start = moment().startOf('month');
				var end = moment();
				var $this = $(this);
				var options = $this.data('plugin-options');
				options = $.extend({}, defaults, options);
				function cb(start, end, first="") {
					$this.find('span').html(start.format(options.locale.format) + ' - ' + end.format(options.locale.format));
					var redirect_url = document.URL;
					redirect_url = updateQueryStringParameter(redirect_url,'start',start.format('YYYY-MM-DD'));
					redirect_url = updateQueryStringParameter(redirect_url,'end',end.format('YYYY-MM-DD'));
					if(first != true){
						window.location = redirect_url;
					}
				}

				if( this.unifato === undefined ){
				this.unifato = {};
				}
				$this.daterangepicker(options, cb);
				this.unifato.daterangepicker = $this.data('daterangepicker');
				var first = true;
				cb(start, end, first);
				first = false;
			});
		}

		function updateQueryStringParameter(uri, key, value) {
			var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
			var separator = uri.indexOf('?') !== -1 ? "&" : "?";
			if (uri.match(re)) {
				return uri.replace(re, '$1' + key + "=" + value + '$2');
			}else {
				return uri + separator + key + "=" + value;
			}
		}
	</script>
@endsection