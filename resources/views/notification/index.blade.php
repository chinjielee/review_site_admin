@extends( 'layouts.main' )
@section( 'headCss' )
<style>
.notification_details_wrapper small {
    opacity: 0.5;
    float: none;
}
.notification_details_wrapper p {
		display: inline-block;
}
.notification_details_wrapper small {
    opacity: 0.5;
    float: right;
}
</style>
@endsection
<!-- Additional JS -->
@section( 'headJs' )
	<!-- Javascript -->
@endsection
@section( 'content' )
<!-- Content -->
	<!-- Page Title Area -->
	<div class="row page-title clearfix">
		<div class="page-title-left">
			<h5 class="mr-0 mr-r-5">{{$pageInfo->page}}</h5>
		</div>
		<!-- /.page-title-left -->
	</div>
<!-- /.page-title -->
<div class="container clearfix">
    <div class="col-sm-12">
        <div class="list-group">
            @foreach($arrNotification as $notification)
                @if(isset($notification->data['action_url']))
                    <a href="{{$notification->data['action_url']}}" class="list-group-item list-group-item-action flex-column align-items-start notification_details_wrapper">
                @else
                    <a class="list-group-item list-group-item-action flex-column align-items-start notification_details_wrapper" onclick="return false;">
                @endif
                @if($notification->type == 'App\Notifications\NotificationNewProduct')
                    <p class="nomargin"><i class="i-rounded i-circled i-small icon-line2-speech notopmargin nobottommargin bg-blue"></i>{{$notification->data['body']}}</p>
                @else
                    <p class="nomargin"><i class="i-rounded i-circled i-small icon-line2-speech notopmargin nobottommargin bg-blue"></i>{{$notification->data['body']}}</p>
                @endif
                    <small><strong>{{$notification->created_at->diffForHumans()}}</strong></small>
                </a>
            @endforeach	
        </div>
    </div>
</div>
@endsection