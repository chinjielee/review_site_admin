@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
@section( 'headJs' )
@endsection
@section( 'content' )
<div class="row page-title clearfix">
	<div class="page-title-left">
		<h5 class="mr-0 mr-r-5">
			@if($pageView == "Edit")
				Edit User {{$userInfo->details->email}}
					@if($userInfo->details->user_status == 0)
					<span class="badge badge-pill badge-warning ml-2">Pending</span>
					@elseif($userInfo->details->user_status == 1)
					<span class="badge badge-pill badge-success ml-2">Active</span>
					@else
					<span class="badge badge-pill badge-danger ml-2">Banned</span>
					@endif
			@else
				Add New User
			@endif
		</h5>
	</div>
</div>
<div class="widget-list">
	<div class="row">
		<div class="col-md-8">
			@if(Session::get('errors')||count( $errors ) > 0)
				<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					<i class="icon-remove-sign"></i> {{ $error }} </br>
				@endforeach
				</div>
			@endif
			@if($flash = session('success'))
				<div class="alert alert-success" onclick="$(this).hide()">
					{{ $flash }}
				</div>
			@endif
			<form id="user-form" name="user-form" action="{{ route('Save User') }}" method="post">
				{{ csrf_field() }}
				@if($pageView != "Edit")
				<input type="hidden" name="type" value="add">
				<div class="row">
					<div class="form-group col-md-6">
						<label>First Name</label>
						<input type="text" id="register-firstname" name="firstname" value="{{old('firstname')}}" class="form-control {{ $errors->has('firstname') ? ' error' : '' }}" required>
					</div>
					<div class="form-group col-md-6">
						<label>Last Name</label>
						<input type="text" id="register-lastname" name="lastname" value="{{old('lastname')}}" class="form-control {{ $errors->has('lastname') ? ' error' : '' }}" required>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Email Address</label>
						<input type="email" class="form-control" id="email" value="{{old('email')}}" name="email"  required>
					</div>
					<div class="form-group col-md-6">
						<label>Password</label>
						<input type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" id="password" name="password" required>
						@if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Mobile</label>

						<div class="row">
							<div class="form-group col-md-4">
								<select id="register-form-mobile" name="mobile_code" class="form-control">
									<option {{ (old("mobile_code") == '60' ? "selected":"") }}>60</option>
									<option {{ (old("mobile_code") == '65' ? "selected":"") }}>65</option>
								</select>
							</div>
							<div class="form-group col-md-8">
								<input type="number" id="register-form-mobile" name="mobile" value="{{old('mobile')}}" class="form-control {{ $errors->has('mobile') ? ' error' : '' }}" />
							</div>
						</div>
					</div>
					<div class="form-group col-md-6">
						<label>Date of birth</label>
						<input type="text" value="{{old('dob')}}" class="form-control {{ $errors->has('dob') ? ' error' : '' }}" name="dob" placeholder="YYYY-MM-DD">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Gender</label>
						<select id="inputGender" class="form-control" name="gender">
							<option {{ (old("gender") == 'Female' ? "selected":"") }}>Female</option>
							<option {{ (old("gender") == 'Male' ? "selected":"") }}>Male</option>
						</select>
					</div>
					<div class="form-group col-md-6">
						<label>Country of residence</label>
						<select id="inputCountry" class="form-control" name="country">
							@foreach($countryList as $country)
								<option {{ (old("country") == "$country" ? "selected":"") }}>{{$country}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Language</label>
						<select id="inputLanguage" class="form-control" name="language">
							<option {{ (old("language") == 'English' ? "selected":"") }}>English</option>
							<option {{ (old("language") =='Chinese' ? "selected":"") }}>Chinese</option>
						</select>
					</div>
					<div class="form-group col-md-6">
						<label>User Status</label>
						<select id="inputStatus" class="form-control" name="status">
							@foreach($statusList as $status)
								<option {{ (old("status") == "$status" ? "selected":"") }}>{{$status}}</option>
							@endforeach
						</select>
					</div>
				</div>
				@else
				<input type="hidden" name="type" value="update">
				<input type="hidden" name="user" value="{{$userInfo->details->email}}">
				<div class="row">
					<div class="form-group col-md-6">
						<label>First Name</label>
						<input type="text" id="register-firstname" name="firstname" value="{{ $userInfo->details->first_name }}" class="form-control {{ $errors->has('firstname') ? ' error' : '' }}" required>
					</div>
					<div class="form-group col-md-6">
						<label>Last Name</label>
						<input type="text" id="register-lastname" name="lastname" value="{{$userInfo->details->last_name}}" class="form-control {{ $errors->has('lastname') ? ' error' : '' }}" required>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Mobile</label>
						<div class="row">
							<div class="form-group col-md-4">
								<select id="register-form-mobile" name="mobile_code" class="form-control">
									<option {{ ($userInfo->details->mobile_code == '60' ? "selected":"") }}>60</option>
									<option {{ ($userInfo->details->mobile_code == '65' ? "selected":"") }}>65</option>
								</select>
							</div>
							<div class="form-group col-md-8">
								<input type="number" id="register-form-mobile" name="mobile" value="{{$userInfo->details->mobile}}" class="form-control {{ $errors->has('mobile') ? ' error' : '' }}" />
							</div>
						</div>
					</div>
					<div class="form-group col-md-6">
						<label>Date of birth</label>
						<input type="text" value="{{ $userInfo->details->dob }}" class="form-control {{ $errors->has('dob') ? ' error' : '' }}" name="dob" placeholder="YYYY-MM-DD">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Gender</label>
						<select id="inputGender" class="form-control" name="gender">
							<option {{ ($userInfo->details->gender == 'Female' ? "selected":"") }}>Female</option>
							<option {{ ($userInfo->details->gender =='Male' ? "selected":"") }}>Male</option>
						</select>
					</div>
					<div class="form-group col-md-6">
						<label>Country of residence</label>
						<select id="inputCountry" class="form-control" name="country">
							@foreach($countryList as $country)
								<option {{ ($userInfo->details->country == "$country" ? "selected":"") }}>{{$country}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Language</label>
						<select id="inputLanguage" class="form-control" name="language">
							<option {{ ($userInfo->details->language == 'en' ? "selected":"") }}>English</option>
							<option {{ ($userInfo->details->language =='cn' ? "selected":"") }}>Chinese</option>
						</select>
					</div>
					<div class="form-group col-md-6">
						<label>User Status</label>
						<select id="inputStatus" class="form-control" name="status">
							<option {{ ($userInfo->details->user_status == "0" ? "selected":"") }}>Pending</option>
							<option {{ ($userInfo->details->user_status == "1" ? "selected":"") }}>Active</option>
							<option {{ ($userInfo->details->user_status == "2" ? "selected":"") }}>Banned</option>
						</select>
					</div>
				</div>
				@endif
				<div class="mb-5">
					<button type="submit" class="btn btn-color-scheme">Save Changes</button>
					<a href="{{ route('User')}}" class="btn btn-outline-color-scheme">Cancel</a>
				</div>
			</form>
		</div>

		@if($pageView == "Edit")
		<div class="col-md-4">
			<div class="widget-bg mb-2" style="position: relative">
				<div class="widget-body clearfix">
					<div class="widget-counter">
						<h6>Total Reviews</h6>
						<h3 class="h1"><a href="#"><span class="counter">{{$userInfo->details->review_count}}</span></a></h3><i class="material-icons list-icon">star</i>
					</div>
				</div>
			</div>
			<div class="widget-bg mb-2" style="position: relative">
				<div class="widget-body clearfix">
					<div class="widget-counter">
						<h6>Total Comments </h6>
						<h3 class="h1"><a href="#"><span class="counter">{{$userInfo->details->comment_count}}</span></a></h3><i class="material-icons list-icon">rate_review</i>
					</div>
				</div>
			</div>
			<div class="widget-bg mb-2" style="position: relative">
				<div class="widget-body clearfix">
					<div class="widget-counter">
						<h6>Total Questions</h6>
						<h3 class="h1"><a href="#"><span class="counter">{{$userInfo->details->question_count}}</span></a></h3><i class="material-icons list-icon">chat</i>
					</div>
				</div>
			</div>
		</div>
		@endif
	</div>
</div>
@endsection