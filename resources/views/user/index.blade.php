@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
@section( 'headJs' )
@endsection
@section( 'content' )
<div class="row page-title clearfix">
	<div class="page-title-left">
		<h5 class="mr-0 mr-r-5">All Users</h5>
	</div>
</div>
<div class="widget-list">
	<div class="row">
		<div class="col-md-12">
			@if(Session::get('errors')||count( $errors ) > 0)
				<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					<i class="icon-remove-sign"></i> {{ $error }} </br>
				@endforeach
				</div>
			@endif
			@if($flash = session('success'))
				<div class="alert alert-success" onclick="$(this).hide()">
					{{ $flash }}
				</div>
			@endif
			@if($flash = session('warning'))
				<div class="alert alert-warning" onclick="$(this).hide()">
					{{ $flash }}
				</div>
			@endif

			<a class="btn btn-default" href="{{ route('Export User')}}/xls"><i class="material-icons list-icon">get_app</i> Export .XLS</a>
			<a class="btn btn-default" href="{{ route('Export User')}}/xlsx"><i class="material-icons list-icon">get_app</i> Export .XLSX</a>
			<a class="btn btn-default" href="{{ route('Export User')}}/csv"><i class="material-icons list-icon">get_app</i> Export .CSV</a>
			<a class="btn btn-default" href="{{ route('Add User')}}"><i class="material-icons list-icon">add</i> Add New</a><!-- 
			<a class="btn btn-default" href="#"><i class="material-icons list-icon">delete</i> Delete</a> -->
		</div>
		<!-- <div class="col-md-5">
			<div class="input-group">
				<select class="form-control">
					<option>Filter Category</option>
					<option>Option 2</option>
					<option>Option 3</option>
					<option>Option 4</option>
					<option>Option 5</option>
				</select>
				<span class="input-group-btn"><a class="btn btn-default" href="#">Filter</a></span>
			</div>
		</div> -->
	</div>
	<div class="row mb-2">
		<div class="col-md-7">
			<p class="mt-3 mb-0">
				<a href="{{ route('User')}}">All ({{$user_list->totalCount}})</a> 
				<span class="mr-lr-10">|</span> 
				@foreach($user_list->categoryCount as $category)
					@if($category->user_status == 0)
						<a href="{{ route('User')}}/Pending">Pending ({{ $category->total }})</a>
						<span class="mr-lr-10">|</span>
					@elseif($category->user_status == 1)
						<a href="{{ route('User')}}/Active">Active ({{ $category->total }})</a>
						<span class="mr-lr-10">|</span>
					@else
						<a href="{{ route('User')}}/Banned">Banned ({{ $category->total }})</a>
					@endif
				@endforeach
			</p>
		</div>
		<div class="col-md-5 text-right">
			<div class="predefinedRanges mr-tb-10" style="cursor: pointer" data-plugin-options='{"opens": "right"}'>
				<span></span><i class="list-icon material-icons">expand_more</i>
            </div>
		</div>        
	</div>  
	<div class="widget-bg">
		<div class="widget-body clearfix">
			<table class="table table-striped table-responsive" data-toggle="datatables">
				<thead>
					<tr>
						<!-- <th class="bs-checkbox" data-field="state" tabindex="0">
							<div class="th-inner"><input name="btSelectAll" type="checkbox"></div>
							<div class="fht-cell"></div>
						</th> -->
						<th>Image</th>
						<th>Name/Email</th>
						<th>Mobile</th>
						<th>Role</th>
						<th class="text-center">Reviews</th>
						<th class="text-center">Comments</th>
						<th class="text-center">Questions</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($user_list->listing as $user)
					<tr>
						<!-- <td class="bs-checkbox">
							@if($user->user_status == 2)
							<input data-index="0" disabled name="btSelectItem" type="checkbox">
							@else
							<input data-index="0" name="btSelectItem" type="checkbox">
							@endif
						</td> -->
						<td>{{$user->first_name." ".$user->last_name}}<br><small>{{$user->email}}</small>
						</td>
						<td><img class="img-circle img-thumbnail" width="50" src="{{ url('/images/user/'.$user->avatar) }}">
						</td>
						<td>{{$user->mobile}}
						</td>
						<td>User<br>
							@if($user->user_status == 1)
							<span class="badge badge-pill badge-success">Active</span>
							@elseif($user->user_status == 2)
							<span class="badge badge-pill badge-danger">Banned</span>
							@else 
							<span class="badge badge-pill badge-warning">Pending</span>
							@endif
						</td>
						<td class="text-center">{{$user->review_count}}</td>
						<td class="text-center">{{$user->comment_count}}</td>
						<td class="text-center">{{$user->question_count}}</td>
						<td>
							<a href="{{route('Update User')}}/{{$user->email}}" class="btn btn-default btn-sm m-1">Edit</a>
							@if($user->user_status != 2)
							<form id="user-form" name="user-form" action="{{ route('Block User') }}" method="post">
								{{ csrf_field() }}
								<input type="hidden" name="user" value="{{$user->email}}">
								<button type="button" onclick="deleteConfirmation(this)" class="btn btn-default btn-sm m-1"><i class="material-icons list-icon">delete</i></button>
							</form>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
<script>
function deleteConfirmation(b){
	var button = $(b);
  	var form = button.parent('form').get(0);
	swal({
	    title: 'User Ban Confirmation',
	    text: 'Are you sure you want to ban this user?',
	    showCancelButton: true,
	    confirmButtonText: 'Yes, I am sure',
	    cancelButtonText: "Cancel"
	}).then(function(isConfirm) {
  		form.submit();
	});
}
</script>
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
@endsection