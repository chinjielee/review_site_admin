@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
@section( 'headJs' )
@endsection
@section( 'content' )
<div class="row page-title clearfix">
	<div class="page-title-left">
		<h5 class="mr-0 mr-r-5">Spam Filter</h5>
	</div>
</div>
<div class="widget-list">
	<div class="row">
		<div class="col-lg-12">
			<div class="button-box">
				<a id="select-all" data-multiselect-target="#public-methods" data-multiselect-method="select_all" data-event="click" class="btn btn-success mr-r-10 mr-b-10" href="#">Select All</a>
				<a id="deselect-all" data-multiselect-target="#public-methods" data-multiselect-method="deselect_all" data-event="click" class="btn btn-default mr-r-10 mr-b-10" href="#">Deselect All</a>
				<form id='add_text' class="form-inline">
					<input type="text" class="form-control mb-2 mr-sm-2" id="add_text_value" placeholder="Fxxx">
					<button type="submit" class="btn btn-blue mr-r-10 mr-b-10">Add</button>
				</form>
			</div>
			<form method="post" id='save_spam'>
				{{ csrf_field() }}
				<div class="mr-b-30">
					<select multiple="multiple" id="public-methods" data-toggle="multiselect" name="public-methods[]">
						@foreach($arraySpamList as $spamList)
							@if($spamList->status == 1)
								<option value="{{$spamList->text}}" selected>{{$spamList->text}}</option>
							@else
								<option value="{{$spamList->text}}">{{$spamList->text}}</option>
							@endif
						@endforeach
					</select>
				</div>
				<div class="button-box">
					<button type="submit" class="btn btn-blue mr-r-10 mr-b-10">Save Changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section( 'tailJs' )
	<script>
		$('form#add_text').submit(function(e) {
			e.preventDefault(); //Prevent the normal submission action
			$target = $("#public-methods");
			var newText = $('#add_text_value').val();
			$target.multiSelect( 'addOption', 
				{ 
					value: newText,
					text: newText, 
					index: 0, 
					nested: 'optgroup_label' 
				}
			);
			$("#public-methods").append('<option value="'+newText+'">'+newText+'</option>)');
			return false;
		});

		$('#save_spam').submit(function(e){
			e.preventDefault(); //Prevent the normal submission action
			// Get the data Active and Not Active
			var dataActive = $('select#public-methods').val();
			// var dataDisable = ;
			var allOption = $('select#public-methods > option').map(function() { return this.value; }).get();
			// Scan all non selected data
			var dataDisable = [];
			let size = allOption.length;
			for(var i =0 ; i < size ; i++) {
				if($.inArray(allOption[i], dataActive)!= -1) {
				} else {
					dataDisable.push(allOption[i]);
				}
			}
			return swal({
				title: 'Are you sure?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, save'
			}).then(function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						method: "POST",
						url: '{{route("Spam Filter Save")}}',
						data: {
							_token: $('meta[name="csrf-token"]').attr('content'),
							active: dataActive,
							disable: dataDisable
						},
					}).done(function(data) {
						if(data == "done"){
							swal({
								title:'Save Success',
								type: 'info'
							}).then(function(){
								window.location.href = "{{route('Spam Filter')}}";
							});
						} else {
							swal({
								title:'Error Occur, try again later',
								type: 'warning'
							});
						}
					});
				} else {
					return false;
				}
			});
		});
	</script>
@endsection