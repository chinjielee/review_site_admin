@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
@section( 'headJs' )
@endsection
@section( 'content' )
	<div class="row page-title clearfix">
		<div class="page-title-left">
			<h5 class="mr-0 mr-r-5">Change Password</h5>
		</div>
	</div>
<div class="widget-list">
	<div class="row">
		<div class="col-md-8">
			@if(Session::get('errors')||count( $errors ) > 0)
				<div class="alert alert-danger" onclick="$(this).hide()">
				@foreach ($errors->all() as $error)
					<i class="icon-remove-sign"></i> {{ $error }} </br>
				@endforeach
				</div>
			@endif
			@if($flash = session('success'))
				<div class="alert alert-success" onclick="$(this).hide()">
					{{ $flash }}
				</div>
			@endif
			<form method="post" action="{{ route('Password Update') }}">
				{{ csrf_field() }}
				<div class="row">
					<div class="form-group col-md-12">
						<label>Current Password</label>
						<input type="password" class="form-control {{ $errors->has('current_password') ? ' has-error' : '' }}" id="current_password" placeholder="" name="current_password" required>
						@if ($errors->has('current_password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </span>
                        @endif
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<label>New Password</label>
						<input type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" id="password" placeholder="" name="password" required>
						@if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<label>Confirm New Password</label>
						<input type="password" class="form-control {{ $errors->has('password_confirmation') ? ' has-error' : '' }}" id="password-confirm" placeholder="" name="password_confirmation" required>
						@if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
					</div>
				</div>
				<div class="mb-5">
					<button type="submit" class="btn btn-color-scheme">Save Changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection