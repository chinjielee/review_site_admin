@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
<!-- Additional JS -->
@section( 'headJs' )
	<!-- Javascript -->
@endsection
@section( 'content' )
	<!-- Content -->
	<!-- Page Title Area -->
	<div class="row page-title clearfix">
		<div class="page-title-left">
			<h5 class="mr-0 mr-r-5">Edit Profile</h5>
		</div>
		<!-- /.page-title-left -->
	</div>
<!-- /.page-title -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="widget-list">
	<div class="row">
		<div class="col-md-8">
				<div class="row">
					<div class="form-group col-md-12">
						@if(Session::get('errors')||count( $errors ) > 0)
							<div class="alert alert-danger" onclick="$(this).hide()">
							@foreach ($errors->all() as $error)
								<i class="icon-remove-sign"></i> {{ $error }} </br>
							@endforeach
							</div>
						@endif
						@if($flash = session('success'))
							<div class="alert alert-success" onclick="$(this).hide()">
								{{ $flash }}
							</div>
						@endif
						<img class="img-circle img-thumbnail mr-2" width="120" src="{{ url('/images/admin/'.Auth::user()->avatar) }}">
						<a onclick="$('#avatarFile').click();" class="btn btn-default">Upload/Add Photo</a>
						<form id="uploadAvatarForm" action="{{ route('My Profile Photo') }}" method="post" enctype="multipart/form-data">
				    	{{ csrf_field() }}
				        <div class="form-group">
				            <input type="file" onchange="photoSubmit.click()" class="hidden" accept=".gif, .png, .jpg, .pdf" name="avatar" id="avatarFile" aria-describedby="fileHelp">
				        </div>
				        <button id="photoSubmit" type="submit" class="btn btn-primary hidden">Submit</button>
				    </form>
					</div>
				</div>

			<form method="post" action="{{ route('Save Feature Image') }}" class="clearfix">
				{{ csrf_field() }}
				<div class="row">
					<div class="form-group col-md-6">
						<label>Display Name</label>
						<input class="form-control" value="{{Auth::user()->name}}" name="name" required placeholder="" type="text">
					</div>
					<div class="form-group col-md-6">
						<label>Email Address</label>
						<input class="form-control" value="{{Auth::user()->email}}" disabled placeholder="" type="text">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Mobile</label>
						<input class="form-control" value="{{Auth::user()->mobile}}" name="mobile" placeholder="012-3456789" type="mobile">
					</div>
				</div>
				<div class="mb-5">
					<button type="submit" class="btn btn-color-scheme">Save Changes</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection