@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
@section( 'headJs' )
@endsection
@section( 'content' )
<div class="row page-title clearfix">
	<div class="page-title-left">
		<h5 class="mr-0 mr-r-5">
			@if($pageView == "Edit")
				Edit Brand {{$brandInfo->details->brand_title}}
					@if($brandInfo->details->brand_status == 0)
					<span class="badge badge-pill badge-warning ml-2">Pending</span>
					@elseif($brandInfo->details->brand_status == 1)
					<span class="badge badge-pill badge-success ml-2">Active</span>
					@else
					<span class="badge badge-pill badge-danger ml-2">Banned</span>
					@endif
			@else
				Add New Brand
			@endif
		</h5>
	</div>
</div>
<div class="widget-list">
	<div class="row">
		<div class="col-md-8">
			@if(Session::get('errors')||count( $errors ) > 0)
				<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					<i class="icon-remove-sign"></i> {{ $error }} </br>
				@endforeach
				</div>
			@endif
			@if($flash = session('success'))
				<div class="alert alert-success" onclick="$(this).hide()">
					{{ $flash }}
				</div>
			@endif
			<form id="brand-form" name="brand-form" action="{{ route('Save Brands') }}" method="post">
				{{ csrf_field() }}
				@if($pageView != "Edit")
				<input type="hidden" name="type" value="add">
				<div class="form-group">
					<label>Title</label>
					<input type="text" id="brand_title" name="brand_title" value="{{old('brand_title')}}" class="form-control {{ $errors->has('brand_title') ? ' error' : '' }}" required>
					<small>Brand Title</small>
				</div>
				<div class="form-group">
					<label>Brand Status</label>
					<select id="inputStatus" class="form-control" name="status">
						@foreach($statusList as $status)
							<option {{ (old("status") == "$status" ? "selected":"") }}>{{$status}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Thumbnail <small>(Dimension: 180px X 135px)</small></label><br>
					<img class="mr-2"  id="display_image" name="display_image" width="80" src="{{ asset('/img/default.jpg')}}">
					<button type="button" class="btn btn-default" onclick="triggerUpload(this)" >Upload/Add Image</button>
					<input type="file" onchange="UploadPhoto('BrandFile')" class="hidden" accept=".gif, .png, .jpg, .pdf" name="avatar" id="BrandFile" name="BrandFile" aria-describedby="fileHelp">
					<div class="form-group">
						<input class="form-control" hidden readonly id="UploadedBrandFile" name="UploadedBrandFile" type="text" value="{{old('UploadedBrandFile')}}">
					</div>
				</div>
				@else
				<input type="hidden" name="type" value="update">
				<input type="hidden" name="brand" value="{{$brandInfo->details->brands_id}}">
				<div class="form-group">
					<label>Title</label>
					<input type="text" id="brand_title" name="brand_title" value="{{$brandInfo->details->brand_title}}" class="form-control {{ $errors->has('brand_title') ? ' error' : '' }}" required>
					<small>Brand Title</small>
				</div>
				<div class="form-group">
					<label>Brand Status</label>
					<select id="inputStatus" class="form-control" name="status">
						<option {{ ($brandInfo->details->brand_status == "0" ? "selected":"") }}>Pending</option>
						<option {{ ($brandInfo->details->brand_status == "1" ? "selected":"") }}>Active</option>
						<option {{ ($brandInfo->details->brand_status == "2" ? "selected":"") }}>Banned</option>
					</select>
				</div>
				<div class="form-group">
					<label>Thumbnail <small>(Dimension: 180px X 135px)</small></label><br>
					<img class="mr-2" id="display_image" name="display_image" width="80" src="{{url('/images/brand_image/')."/".$brandInfo->details->images}}">
					<button type="button" class="btn btn-default" onclick="triggerUpload(this)" >Upload/Add Image</button>
					<input type="file" onchange="UploadPhoto('BrandFile')" class="hidden" accept=".gif, .png, .jpg, .pdf" name="avatar" id="BrandFile" name="BrandFile" aria-describedby="fileHelp">
					<div class="form-group">
						<input class="form-control" hidden readonly id="UploadedBrandFile" name="UploadedBrandFile" placeholder="" type="text" value="{{$brandInfo->details->images}}">
					</div>
				</div>
				@endif
				<div class="mb-5">
					<button type="submit" class="btn btn-color-scheme">Save Changes</button>
					<a href="{{ route('Brands')}}" class="btn btn-outline-color-scheme">Cancel</a>
				</div>
			</form>
		</div>
	</div>
</div>	
@endsection
@section('tailJs')
<script type="text/javascript">

function triggerUpload(param){
	var UploadInput = param.nextSibling.nextSibling.id;
	$('#'+UploadInput).trigger("click");
}

function UploadPhoto(id){
    var image = $('#'+id)[0].files[0];
	var form = new FormData();
    form.append('id', id);
	form.append('brand_image', image);
	$.ajax({
	    url: '{{config("app.url")}}/brands/action/upload_brand_images',
	    data: form,
	    cache: false,
	    contentType: false,
	    processData: false,
	    type: 'POST',
	    beforeSend: function (xhr) {
            var token = '{{ csrf_token() }}';
            if(token){
				return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }                
        }, 
	    success:function(response) {
	        $('#Uploaded'+id).val(response);
	        $('#display_image').attr('src',"{{url('/images/brand_image/').'/'}}"+response);
	    },
	    error:function(response) {
	        console.log(response.responseText);
	    },
	});
}

</script>
@endsection