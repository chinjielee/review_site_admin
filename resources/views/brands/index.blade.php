@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
@section( 'headJs' )
@endsection
@section( 'content' )
<div class="row page-title clearfix">
	<div class="page-title-left">
		<h5 class="mr-0 mr-r-5">Brands</h5>
	</div>
</div>
<div class="widget-list">
	<div class="row">
		<div class="col-md-7">
			<a class="btn btn-default" href="{{route('Add Brands')}}"><i class="material-icons list-icon">add</i> Add New</a>
			<!-- <a class="btn btn-default" href="#"><i class="material-icons list-icon">delete</i> Delete</a> -->
		</div>
		<!-- <div class="col-md-5">
			<div class="input-group">
				<select class="form-control">
					<option>Filter Category</option>
					<option>Option 2</option>
					<option>Option 3</option>
					<option>Option 4</option>
					<option>Option 5</option>
				</select>
				<span class="input-group-btn"><a class="btn btn-default" href="#">Filter</a></span>
			</div>
		</div> -->
	</div>
	
	<div class="row mb-2">
		<div class="col-md-7">
		</div>
		<div class="col-md-5 text-right">
			<div id="predefinedRangesBrand" class="predefinedRangesBrand mr-tb-10" style="cursor: pointer" data-plugin-options='{"opens": "right"}'>
				<span></span>
				<i class="list-icon material-icons">expand_more</i>
            </div>
		</div>        
	</div>
	@if(Session::get('errors')||count( $errors ) > 0)
		<div class="alert alert-danger">
		@foreach ($errors->all() as $error)
			<i class="icon-remove-sign"></i> {{ $error }} </br>
		@endforeach
		</div>
	@endif
	@if($flash = session('success'))
		<div class="alert alert-success" onclick="$(this).hide()">
			{{ $flash }}
		</div>
	@endif
	<div class="widget-bg">
		<div class="widget-body clearfix">
			<table class="table table-striped table-responsive" data-toggle="datatables">
				<thead>
					<tr>
						<!-- <th class="bs-checkbox" data-field="state" tabindex="0">
							<div class="th-inner"><input name="btSelectAll" type="checkbox"></div>
							<div class="fht-cell"></div>
						</th> -->
						<th>Image</th>
						<th>Name</th>
						<th>Count</th>
						<th>Action</th>
					</tr>

				</thead>
				<tbody>
					@foreach($brand_list->listing as $brand)
					<tr>
						<!-- <td class="bs-checkbox"><input data-index="0" name="btSelectItem" type="checkbox"></td> -->
						<td><img class="mr-2" width="60" src="{{url('/images/brand_image/').'/'.$brand->images}}"></td>
						<td><a href="{{route('Update Brands')}}/{{$brand->brands_id}}">{{$brand->brand_title}}</a></td>
						<td>{{$brand->total}}</td>
						<td>
							<a href="{{route('Update Brands')}}/{{$brand->brands_id}}" class="btn btn-default btn-sm m-1">Edit</a>
							@if($brand->brand_status != 2)
							<form id="brand-form" name="brand-form" action="{{ route('Delete Brands') }}" method="post">
								{{ csrf_field() }}
								<input type="hidden" name="brand" value="{{$brand->brands_id}}">
								<button type="button" onclick="deleteConfirmation(this)" class="btn btn-default btn-sm m-1"><i class="material-icons list-icon">delete</i></button>
							</form>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
@section('tailJs')
<script>
	enablePredefinedDateRangePickerBrand();
	
	function enablePredefinedDateRangePickerBrand(){
		let input_start,input_end;
		if('{{$input_start}}' !== ''){
			input_start  = moment('{{$input_start}}','YYYY-MM-DD');
		} else {
			input_start = moment().startOf('month');
		}
		if('{{$input_end}}' !== ''){
			input_end = moment('{{$input_end}}','YYYY-MM-DD');			
		} else {
			input_end = moment().endOf('month');
		}
      var $el = $('.predefinedRangesBrand');
      if ( !$el.length ) return;
      var defaults = {
        locale: {
          format: 'YYYY-MM-DD',
        },
        startDate: input_start,
        endDate: input_end,
        opens: "left",
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      };
      $el.each( function() {
        var start = input_start;
        var end = input_end;
        var $this = $(this);
        var options = $this.data('plugin-options');
        options = $.extend({}, defaults, options);
        function cb(start, end, first="") {
        	$this.find('span').html(start.format(options.locale.format) + ' - ' + end.format(options.locale.format));
        	var redirect_url = document.URL;
		    redirect_url = updateQueryStringParameter(redirect_url,'start',start.format('YYYY-MM-DD'));
		    redirect_url = updateQueryStringParameter(redirect_url,'end',end.format('YYYY-MM-DD'));
		   	if(first != true){
			    window.location = redirect_url;
			}
        }

        if( this.unifato === undefined ){
          this.unifato = {};
        }
        $this.daterangepicker(options, cb);
        this.unifato.daterangepicker = $this.data('daterangepicker');
        var first = true;
        cb(start, end, first);
        first = false;
      });
    }

    function updateQueryStringParameter(uri, key, value) {
		var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
		var separator = uri.indexOf('?') !== -1 ? "&" : "?";
		if (uri.match(re)) {
			return uri.replace(re, '$1' + key + "=" + value + '$2');
		}else {
			return uri + separator + key + "=" + value;
		}
	}

    function deleteConfirmation(b){
		var button = $(b);
	  	var form = button.parent('form').get(0);
		swal({
		    title: 'Brand delete Confirmation',
		    text: 'Are you sure you want to delete this brand?',
		    showCancelButton: true,
		    confirmButtonText: 'Yes, I am sure',
		    cancelButtonText: "Cancel"
		}).then(function(isConfirm) {
	  		form.submit();
		});
	}
</script>
@endsection