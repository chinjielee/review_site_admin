@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
<!-- Additional JS -->
@section( 'headJs' )
	<!-- Javascript -->
@endsection
@section( 'content' )
	<!-- Content -->
	<!-- Page Title Area -->
	<div class="row page-title clearfix">
		<div class="page-title-left">
			<h5 class="mr-0 mr-r-5">Movies List</h5>
		</div>
		<!-- /.page-title-left -->
	</div>
<!-- /.page-title -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="widget-list">
	<div class="row">
		<div class="col-md-7">
			<a class="btn btn-default" href="#"><i class="material-icons list-icon">get_app</i> Export .CSV</a>
			<a class="btn btn-default" href="#"><i class="material-icons list-icon">delete</i> Delete</a>
		</div>
		<div class="col-md-5">
			<div class="input-group">
				<select class="form-control">
					<option>Filter Category</option>
					<option>Option 2</option>
					<option>Option 3</option>
					<option>Option 4</option>
					<option>Option 5</option>
				</select>
				<span class="input-group-btn"><a class="btn btn-default" href="#">Filter</a></span>
			</div>
		</div>
	</div>
	
	<div class="row mb-2">
	
		<div class="col-md-7">
			<p class="mt-3 mb-0">
				<a href="#">All (2,152)</a> <span class="mr-lr-10">|</span> <a href="#">Published (2,138)</a> <span class="mr-lr-10">|</span> <a href="#">Drafts (9)</a> <span class="mr-lr-10">|</span> <a href="#">Trash (364)</a>
			</p>
		</div>
	<div class="col-md-5 text-right">
	<div class="predefinedRanges mr-tb-10" style="cursor: pointer" data-plugin-options='{"opens": "right"}'><span></span>  <i class="list-icon material-icons">expand_more</i>
                                        </div>
	</div>        
	</div>  
	<div class="widget-bg">
		<div class="widget-body clearfix">
			<table class="table table-striped table-responsive" data-toggle="datatables">
				<thead>
					<tr>
						<th class="bs-checkbox" data-field="state" tabindex="0">
							<div class="th-inner"><input name="btSelectAll" type="checkbox">
							</div>
							<div class="fht-cell"></div>
						</th>
						<th>Image</th>
						<th>Name</th>
						<th>Brand</th>
						<th>Category</th>
						<th>Total Reviews</th>
						<th>Date</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="bs-checkbox"><input data-index="0" name="btSelectItem" type="checkbox">
						</td>
						<td><img class="mr-2" width="60" src="{{ asset('/img/default.jpg')}}">
						</td>
						<td><a href="#">Samsung Galaxy Note 8</a>
						</td>
						<td><a href="#">Samsung</a>
						</td>
						<td><a href="#">Electronics</a><br><a href="#">— Mobile Phones</a>
						</td>
						<td><a href="#"><i class="material-icons list-icon">star_border</i></a>
						</td>
						<td>Published<br><small>18 hours ago</small>
						</td>
						<td><button class="btn btn-default btn-sm">Edit</button><button class="btn btn-default btn-sm"><i class="material-icons list-icon">delete</i></button>
						</td>
					</tr>
					<tr>
						<td class="bs-checkbox"><input data-index="0" name="btSelectItem" type="checkbox">
						</td>
						<td><img class="mr-2" width="60" src="{{ asset('/img/default.jpg')}}">
						</td>
						<td><a href="#">Samsung Galaxy Note 8</a>
						</td>
						<td><a href="#">Samsung</a>
						</td>
						<td><a href="#">Electronics</a><br><a href="#">— Mobile Phones</a>
						</td>
						<td><a href="#"><i class="material-icons list-icon">grade</i></a>
						</td>
						<td>Published<br><small>18 hours ago</small>
						</td>
						<td><button class="btn btn-default btn-sm">Edit</button><button class="btn btn-default btn-sm"><i class="material-icons list-icon">delete</i></button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- /.widget-body -->
	</div>
</div>

@endsection