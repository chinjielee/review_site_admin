@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
@section( 'headJs' )
@endsection
@section( 'content' )
<div class="row page-title clearfix">
	<div class="page-title-left">
		<h5 class="mr-0 mr-r-5">All Admins</h5>
	</div>
</div>
<div class="widget-list">
	<div class="row">
		<div class="col-md-12">
			@if(Session::get('errors')||count( $errors ) > 0)
				<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					<i class="icon-remove-sign"></i> {{ $error }} </br>
				@endforeach
				</div>
			@endif
			@if($flash = session('success'))
				<div class="alert alert-success" onclick="$(this).hide()">
					{{ $flash }}
				</div>
			@endif
			@if($flash = session('warning'))
				<div class="alert alert-warning" onclick="$(this).hide()">
					{{ $flash }}
				</div>
			@endif
			<a class="btn btn-default" href="{{ route('Add Admin')}}"><i class="material-icons list-icon">add</i> Add New</a>
		</div>
	</div>
	<div class="row mb-2">
		<div class="col-md-7">
			<p class="mt-3 mb-0">
				<a href="{{ route('Admin')}}">All ({{$admin_list->totalCount}})</a> 
				<span class="mr-lr-10">|</span> 
				@foreach($admin_list->categoryCount as $category)
					@if($category->admin_status == 1)
						<a href="{{ route('Admin')}}/Active">Active ({{ $category->total }})</a>
					@else
						<a href="{{ route('Admin')}}/Deactivated">Deactivated ({{ $category->total }})</a>
						<span class="mr-lr-10">|</span>
					@endif
				@endforeach
			</p>
		</div>       
	</div>  
	<div class="widget-bg">
		<div class="widget-body clearfix">
			<table class="table table-striped table-responsive" data-toggle="datatables">
				<thead>
					<tr>
						<th>Name/Email</th>
						<th>Mobile</th>
						<th>Role</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($admin_list->listing as $admin)
					<tr>
						</td>
						<td><a href="#">{{$admin->name}}<br><small>{{$admin->email}}</small></a>
						</td>
						<td>{{$admin->mobile}}
						</td>
						<td>@if($admin->roles == 1)
							Super Admin
							@else 
							Admin
							@endif
							<br>
							@if($admin->admin_status == 1)
							<span class="badge badge-pill badge-success">Active</span>
							@else 
							<span class="badge badge-pill badge-danger">Deactived</span>
							@endif
						</td>
						<td>
							<a href="{{route('Update Admin')}}/{{$admin->email}}" class="btn btn-default btn-sm m-1">Edit</a>
							@if($admin->admin_status != 0)
							<form id="admin-form" name="admin-form" action="{{ route('Block Admin') }}" method="post">
								{{ csrf_field() }}
								<input type="hidden" name="admin" value="{{$admin->email}}">
								<button type="button" onclick="deleteConfirmation(this)" class="btn btn-default btn-sm m-1"><i class="material-icons list-icon">delete</i></button>
							</form>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
<script>
function deleteConfirmation(b){
	var button = $(b);
  	var form = button.parent('form').get(0);
	swal({
	    title: 'Admin Deactivate Confirmation',
	    text: 'Are you sure you want to deactivate this admin?',
	    showCancelButton: true,
	    confirmButtonText: 'Yes, I am sure',
	    cancelButtonText: "Cancel"
	}).then(function(isConfirm) {
  		form.submit();
	});
}
</script>
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
@endsection