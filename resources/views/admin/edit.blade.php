@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
@section( 'headJs' )
@endsection
@section( 'content' )
<div class="row page-title clearfix">
	<div class="page-title-left">
		<h5 class="mr-0 mr-r-5">
			@if($pageView == "Edit")
				Edit Admin {{$adminInfo->details->email}}
					@if($adminInfo->details->admin_status == "Active")
					<span class="badge badge-pill badge-success ml-2">Active</span>
					@else
					<span class="badge badge-pill badge-danger ml-2">Deactivated</span>
					@endif
			@else
				Add New Admin
			@endif
		</h5>
	</div>
</div>
<div class="widget-list">
	<div class="row">
		<div class="col-md-8">
			@if(Session::get('errors')||count( $errors ) > 0)
				<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					<i class="icon-remove-sign"></i> {{ $error }} </br>
				@endforeach
				</div>
			@endif
			@if($flash = session('success'))
				<div class="alert alert-success" onclick="$(this).hide()">
					{{ $flash }}
				</div>
			@endif
			<form id="admin-form" name="admin-form" action="{{ route('Save Admin') }}" method="post">
				{{ csrf_field() }}
				@if($pageView != "Edit")
				<input type="hidden" name="type" value="add">
				<div class="row">
					<div class="form-group col-md-6">
						<label>Display Name</label>
						<input type="text" id="register-name" name="name" value="{{old('name')}}" class="form-control {{ $errors->has('name') ? ' error' : '' }}" required>
					</div>
					<div class="form-group col-md-6">
						<label>Email Address</label>
						<input type="email" class="form-control" id="email" value="{{old('email')}}" name="email"  required>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Password</label>
						<input type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" id="password" name="password" required>
						@if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
					</div>
					<div class="form-group col-md-6">
						<label>Mobile</label>
						<input type="number" id="register-form-mobile" name="mobile" value="{{old('mobile')}}" class="form-control {{ $errors->has('mobile') ? ' error' : '' }}" />
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Admin Status</label>
						<select id="inputStatus" class="form-control" name="status">
							@foreach($statusList as $status)
								<option {{ (old("status") == "$status" ? "selected":"") }}>{{$status}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-md-6">
						<label>Admin Role</label>
						<select id="inputStatus" class="form-control" name="roles">
							@foreach($roleList as $roles)
								<option {{ (old("roles") == "$roles" ? "selected":"") }}>{{$roles}}</option>
							@endforeach
						</select>
					</div>
				</div>
				@else
				<input type="hidden" name="type" value="update">
				<input type="hidden" name="admin" value="{{$adminInfo->details->email}}">
				<div class="row">
					<div class="form-group col-md-6">
						<label>Display Name</label>
						<input type="text" id="register-name" name="name" value="{{ $adminInfo->details->name }}" class="form-control {{ $errors->has('name') ? ' error' : '' }}" required>
					</div>
					<div class="form-group col-md-6">
						<label>Mobile</label>
						<input type="number" id="register-form-mobile" name="mobile" value="{{$adminInfo->details->mobile}}" class="form-control {{ $errors->has('mobile') ? ' error' : '' }}" />
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>Admin Status</label>
						<select id="inputStatus" class="form-control" name="status">
							@foreach($statusList as $status)
								<option {{ ($adminInfo->details->admin_status == "$status" ? "selected":"") }}>{{$status}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-md-6">
						<label>Admin Role</label>
						<select id="inputRole" class="form-control" name="roles">
							@foreach($roleList as $roles)
								<option {{ ($adminInfo->details->roles == "$roles" ? "selected":"") }}>{{$roles}}</option>
							@endforeach
						</select>
					</div>
				</div>
				@endif
				<div class="mb-5">
					<button type="submit" class="btn btn-color-scheme">Save Changes</button>
					<a href="{{ route('Admin')}}" class="btn btn-outline-color-scheme">Cancel</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection