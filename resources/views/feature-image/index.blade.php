@extends('layouts.main')
@section('headCss')
@endsection
@section('headJs')
@endsection
@section('content')
	<div class="row page-title clearfix">
		<div class="page-title-left">
			<h5 class="mr-0 mr-r-5">Feature Images</h5>
		</div>
	</div>
<style>
@media (max-width: 575px) {
	.product_photos_wrapper .product_photos__wrapper {margin-bottom: 1.5625rem;}
	.product_photos_wrapper .product_photos__wrapper .form-group {margin: 0px;}
}
</style>
<div class="widget-list">
	<div class="row">
		<div class="col-lg-8">
			@if(Session::get('errors')||count( $errors ) > 0)
				<div class="alert alert-danger" onclick="$(this).hide()">
				@foreach ($errors->all() as $error)
					<i class="icon-remove-sign"></i> {{ $error }} </br>
				@endforeach
				</div>
			@endif
			@if($flash = session('success'))
				<div class="alert alert-success" onclick="$(this).hide()">
					{{ $flash }}
				</div>
			@endif
			<form method="post" action="{{ route('Save Feature Image') }}" class="clearfix" enctype="multipart/form-data">
				{{ csrf_field() }}
				@foreach($feature_image_details as $key => $feature_image_detail)
				<div class="card mb-3">
					<div class="card-header">
						<h6 class="m-0">Image {{$key+1}}</h6>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="form-group mb-0 col-sm-10">
								<input class="form-control" readonly id="UploadedAvatarFile{{$key+1}}" name="UploadedAvatarFile{{$key+1}}" placeholder="https://www.xxx.com/photo1.jpg" type="text" value="{{$feature_image_detail->image}}">
							</div>
							<div class="col-sm-2">
								<a onclick="triggerUpload(this)" class="btn btn-color-scheme btn-block" href="javascript: void(0);"><i class="material-icons list-icon">file_upload</i></a>
								<input type="file" onchange="UploadPhoto('AvatarFile{{$key+1}}')" class="hidden" accept=".gif, .png, .jpg, .pdf" name="avatar" id="AvatarFile{{$key+1}}" aria-describedby="fileHelp">
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12">
								<h6 class="mt-0">Large View</h6>
							</div>
							<div class="col-md-3">
								<div class="form-group m-0">
									<small>Max-Width (px)</small>
									<input class="form-control" id="large_width{{$key+1}}" name="large_width{{$key+1}}" value="{{$feature_image_detail->large_width}}" placeholder="180" type="number">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-0">
									<small>Top (%)</small>
									<input class="form-control" id="large_top{{$key+1}}" name="large_top{{$key+1}}" value="{{$feature_image_detail->large_top}}" placeholder="20" type="number">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-0">
									<small>Left (%)</small>
									<input class="form-control" id="large_left{{$key+1}}" name="large_left{{$key+1}}" value="{{$feature_image_detail->large_left}}" placeholder="20" type="number">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-0">
									<small>Right (%)</small>
									<input class="form-control" id="large_right{{$key+1}}" name="large_right{{$key+1}}" value="{{$feature_image_detail->large_right}}" placeholder="20" type="number">
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12">
								<h6 class="mt-0">Middle View</h6>
							</div>
							<div class="col-md-3">
								<div class="form-group m-0">
									<small>Max-Width (px)</small>
									<input class="form-control" id="middle_width{{$key+1}}" name="middle_width{{$key+1}}" value="{{$feature_image_detail->middle_width}}" placeholder="180" type="number">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-0">
									<small>Top (%)</small>
									<input class="form-control" id="middle_top{{$key+1}}" name="middle_top{{$key+1}}" value="{{$feature_image_detail->middle_top}}" placeholder="20" type="number">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-0">
									<small>Left (%)</small>
									<input class="form-control" id="middle_left{{$key+1}}" name="middle_left{{$key+1}}" value="{{$feature_image_detail->middle_left}}" placeholder="20" type="number">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group m-0">
									<small>Right (%)</small>
									<input class="form-control" id="middle_right{{$key+1}}" name="middle_right{{$key+1}}" value="{{$feature_image_detail->middle_right}}" placeholder="20" type="number">
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12">
								<h6 class="mt-0">URL Link</h6>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<input class="form-control" id="url_link{{$key+1}}" name="url_link{{$key+1}}" value="{{$feature_image_detail->url_link}}" placeholder="Add URL Link" type="text">
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				<div>
					<button type="submit" class="btn btn-color-scheme">Save Changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('tailJs')
<script type="text/javascript">

function triggerUpload(param){
	var UploadInput = param.nextSibling.nextSibling.id;
	$('#'+UploadInput).trigger("click");
}

function UploadPhoto(id){
    var image = $('#'+id)[0].files[0];
	var form = new FormData();
    form.append('id', id);
	form.append('feature_image', image);
	$.ajax({
	    url: '{{config("app.url")}}/feature-image/action/upload_feature_image',
	    data: form,
	    cache: false,
	    contentType: false,
	    processData: false,
	    type: 'POST',
	    beforeSend: function (xhr) {
            var token = '{{ csrf_token() }}';
            if(token){
				return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }                
        }, 
	    success:function(response) {
	        $('#Uploaded'+id).val(response);
	    },
	    error:function(response) {
	        alert(response.responseText);
	    },
	});
}

</script>
@endsection