@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
@section( 'headJs' )
@endsection
@section( 'content' )
<div class="row page-title clearfix">
	<div class="page-title-left">
		<h5 class="mr-0 mr-r-5">All Reviews</h5>
	</div>
</div>
<div class="widget-list">
	<div class="row mb-3">
		<div class="col-md-7">
			@if(Session::get('errors')||count( $errors ) > 0)
				<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					<i class="icon-remove-sign"></i> {{ $error }} </br>
				@endforeach
				</div>
			@endif
			@if($flash = session('success'))
				<div class="alert alert-success" onclick="$(this).hide()">
					{{ $flash }}
				</div>
			@endif
			@if($flash = session('warning'))
				<div class="alert alert-warning" onclick="$(this).hide()">
					{{ $flash }}
				</div>
			@endif
			<a class="btn btn-default" href="{{ route('Export Review')}}/csv"><i class="material-icons list-icon">get_app</i> Export .CSV</a>
			<!-- <a class="btn btn-default" href="#"><i class="material-icons list-icon">delete</i> Delete</a> -->
		</div>
	</div>
	<!-- <div class="row">
		<div class="col-md-4">
			<div class="form-group mb-0">
				<select class="selectpicker form-control" multiple="multiple">
					<option selected="selected">All Labels</option>
					<option>Option 2</option>
					<option>Option 3</option>
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group mb-0">
				<select class="selectpicker form-control" multiple="multiple">
					<option selected="selected">All Categories</option>
					<option>Option 2</option>
					<option>Option 3</option>
				</select>
			</div>
		</div>
		<div class="col-md-4">
		<div class="form-group mb-0">
			<select class="selectpicker form-control" multiple="multiple">
				<option selected="selected">All Brands</option>
				<option>Option 2</option>
				<option>Option 3</option>
			</select>
		</div>
		</div>
	</div> -->
	<div class="row mb-2">
		<div class="col-md-7">
			<p class="mt-3 mb-0">
				<a href="{{ route('Reviews')}}">All ({{$data_list->totalCount}})</a> 
				<span class="mr-lr-10">|</span>
				@foreach($data_list->categoryCount as $category)
					@if($category->status == 1)
						<a href="{{ route('Reviews')}}/Published">Published ({{ $category->total }})</a>
						<span class="mr-lr-10">|</span>
					@elseif($category->status == 2)
						<a href="{{ route('Reviews')}}/Cancelled">Cancelled ({{ $category->total }})</a>
						<span class="mr-lr-10">|</span>
					@else
						<a href="{{ route('Reviews')}}/Blocked">Blocked ({{ $category->total }})</a>
					@endif
				@endforeach
			</p>
		</div>
		<div class="col-md-5 text-right">
			<div id="predefinedRangesReview" class="predefinedRangesReview mr-tb-10" style="cursor: pointer" data-plugin-options='{"opens": "right"}'><span></span>  
				<i class="list-icon material-icons">expand_more</i>
            </div>
		</div>        
	</div>                       
	<div class="widget-bg">
		<div class="widget-body clearfix">
			<table class="table table-striped table-responsive" data-toggle="datatables">
				<thead>
					<tr>
						<!-- <th class="bs-checkbox" data-field="state" tabindex="0">
							<div class="th-inner"><input name="btSelectAll" type="checkbox">
							</div>
							<div class="fht-cell"></div>
						</th> -->
						<th>Review/Rating</th>
						<th>Image</th>
						<th>User Details</th>
						<th>Status</th>
						<th>Reported</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data_list->listing as $review)
					<tr>
						<!-- <td class="bs-checkbox">
							<input data-index="0" name="btSelectItem" type="checkbox">
						</td> -->
						<td>
						<a href="#">{{$review->product_service_title}}</a><br>
						<b>{{$review->review_title}}</b>
							<p>{{$review->comments}}</p><br>
							@for ($i = 0; $i < doubleval($review->rating); $i++)
								<i class="material-icons mr-1 fs-18 text-warning">thumb_up</i>
							@endfor
						<strong>{{number_format(doubleval($review->rating),1)}}</strong>
						</td>
						@if($review->review_image == 0)
						<td><img class="mr-2" width="60" src="{{url('/img').'/default_service_logo.png'}}">
						@else
						<td><img class="mr-2" width="60" src="{{url('/images/review/').'/'.$review->review_image}}">
						@endif
						</td>
						<td></a>{{$review->first_name." ".$review->last_name}}<br><small>{{$review->email}}</small>
						</td>
						<td>
							@if($review->status == 1)
								Published
							@elseif($review->status == 2)
								Cancelled
							@else
								Blocked
							@endif
						<!-- <span class="badge badge-pill badge-danger fs-10 my-auto">xxx text</span> -->
						</td>
						<td>
							@if($review->spam == 0)
							<span class="text-success"><i class="material-icons">warning</i> x0</span>
							@else
							<span class="text-danger"><i class="material-icons">warning</i> x{{$review->spam}}</span>
							@endif
						</td>
						<td>
							@if($review->status != 1)
							<form id="review-form" name="review-form" action="{{ route('Update Review') }}" method="post">
								{{ csrf_field() }}
								<input type="hidden" name="review" value="{{$review->product_reviews_id}}">
								<button type="submit" class="btn btn-default btn-sm m-1">Change to Published</button>
							</form>
							@elseif($review->status == 1)
							<form id="review-form" name="review-form" action="{{ route('Delete Review') }}" method="post">
								{{ csrf_field() }}
								<input type="hidden" name="review" value="{{$review->product_reviews_id}}">
								<button type="button" onclick="deleteConfirmation(this)" class="btn btn-default btn-sm m-1">Block Review</button>
							</form>
							@endif
							<!-- <button class="btn btn-default btn-sm m-1"><i class="material-icons list-icon">link</i></button> -->
							<!-- <button class="btn btn-default btn-sm m-1"><i class="material-icons list-icon">delete</i></button> -->
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
@section('tailJs')
<script>
enablePredefinedDateRangePickerReview();
function enablePredefinedDateRangePickerReview(){
  var $el = $('.predefinedRangesReview');
  if ( !$el.length ) return;
  var defaults = {
    locale: {
      format: 'YYYY-MM-DD',
    },
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month'),
    opens: "left",
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
  };
  $el.each( function() {
    var start = moment().startOf('month');
    var end = moment();
    var $this = $(this);
    var options = $this.data('plugin-options');
    options = $.extend({}, defaults, options);
    function cb(start, end, first="") {
    	$this.find('span').html(start.format(options.locale.format) + ' - ' + end.format(options.locale.format));
    	var redirect_url = document.URL;
	    redirect_url = updateQueryStringParameter(redirect_url,'start',start.format('YYYY-MM-DD'));
	    redirect_url = updateQueryStringParameter(redirect_url,'end',end.format('YYYY-MM-DD'));
	   	if(first != true){
		    window.location = redirect_url;
		}
    }

    if( this.unifato === undefined ){
      this.unifato = {};
    }
    $this.daterangepicker(options, cb);
    this.unifato.daterangepicker = $this.data('daterangepicker');
    var first = true;
    cb(start, end, first);
    first = false;
  });
}

function updateQueryStringParameter(uri, key, value) {
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}else {
		return uri + separator + key + "=" + value;
	}
}

function deleteConfirmation(b){
	var button = $(b);
  	var form = button.parent('form').get(0);
	swal({
	    title: 'Block Review Confirmation',
	    text: 'Are you sure you want to block this review?',
	    showCancelButton: true,
	    confirmButtonText: 'Yes, I am sure',
	    cancelButtonText: "Cancel"
	}).then(function(isConfirm) {
  		form.submit();
	});
}
</script>
@endsection