@extends('layouts.main-plain')
@section('content')
	<div id="wrapper" class="row wrapper">
        <div class="col-12 ml-sm-auto col-sm-10 col-md-8 col-lg-5 ml-md-auto login-center mx-auto p-0">
           <div class="card mt-5">
           <div class="card-body">
            <div class="text-center">
               <h1>
                <a href="#">
                    {{ config('app.name', 'WIKÅBÖ') }}
                </a>
				</h1>
            </div>
            <!-- /.navbar-header -->
            @if(Session::get('errors')||count( $errors ) > 0)
                <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <i class="icon-remove-sign"></i> {{ $error }} </br>
                @endforeach
                </div>
            @endif
            <form class="form-material" id="login-form" name="login-form" action="{{ route('login') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <input type="email" id="email" name="email" placeholder="johndoe@site.com" class="form-control form-control-line" value="{{ old('email') }}" required autofocus>
                    <label for="example-email">Email</label>
                </div>
                <div class="form-group">
                    <input type="password" placeholder="password" class="form-control form-control-line" id="password" name="password" value="" required>
                    <label>Password</label>
                </div>
                <div class="form-group">
                    <button class="btn btn-block btn-lg btn-color-scheme ripple" id="login-form-submit" name="login-form-submit" value="login">Login</button>
                </div>
                <div class="form-group no-gutters mb-0">
                    <div class="col-md-12 d-flex">
                        <div class="checkbox checkbox-info mr-auto">
                            <label class="d-flex">
                                <input id="remember" class="checkbox-style" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}> <span class="label-text">Remember me</span>
                            </label>
                        </div><a href="{{ route('password.request') }}" id="to-recover" class="my-auto pb-2 text-right"><i class="fa fa-lock mr-1"></i>Forgot Password?</a>
                    </div>
                </div>
            </form>
        </div>
			</div>
		</div>
    </div>
@endsection
