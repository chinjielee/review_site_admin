@extends('layouts.main-plain')
@section('content')
<div id="wrapper" class="row wrapper">
    <div class="col-12 ml-sm-auto col-sm-10 col-md-8 col-lg-5 ml-md-auto login-center mx-auto p-0">
       <div class="card mt-5">
            <div class="card-body">
                <div class="text-center">
                   <h1>
                    <a href="{{ route('Dashboard')}}">
                        {{ config('app.name', 'WIKÅBÖ') }}
                    </a>
                    </h1>
                </div>
                <!-- /.navbar-header -->
                @if(Session::get('errors')||count( $errors ) > 0)
                    <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <i class="icon-remove-sign"></i> {{ $error }} </br>
                    @endforeach
                    </div>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form class="form-material" method="POST" action="{{ route('password.request') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="email" id="email" name="email" placeholder="johndoe@site.com" class="form-control form-control-line" value="{{ old('email') }}" required autofocus>
                            <label for="example-email">Admin Email</label>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="password" id="password" name="password" class="form-control form-control-line" required autofocus>
                            <label for="example-password">Password</label>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control form-control-line" required autofocus>
                            <label for="example-password_confirmation">Confirm Password</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-lg btn-color-scheme ripple" id="reset-form-submit" name="reset-form-submit" value="reset">Reset Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
