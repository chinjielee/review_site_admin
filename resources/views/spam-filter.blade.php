@extends('layouts.main')
@section('headCss')
@endsection
<!-- Additional JS -->
@section('headJs')
<!-- Javascript -->
@endsection
@section('content')
<!-- Content -->
				<!-- Page Title Area -->
				<div class="row page-title clearfix">
					<div class="page-title-left">
						<h5 class="mr-0 mr-r-5">Dashboard</h5>
					</div>
					<!-- /.page-title-left -->
				</div>
				<!-- /.page-title -->
				<!-- =================================== -->
				<!-- Different data widgets ============ -->
				<!-- =================================== -->
				<div class="widget-list">
					<!-- Counters -->
					<div class="row">
						<div class="col-md-3 col-sm-6 widget-holder widget-full-height">
							<div class="widget-bg">
								<div class="widget-body clearfix">
									<div class="widget-counter">
										<h6>Total Reviews</h6>
										<h3 class="h1"><span class="counter">2748</span></h3><i class="material-icons list-icon">star</i>
									</div>
									<!-- /.widget-counter -->
								</div>
								<!-- /.widget-body -->
							</div>
							<!-- /.widget-bg -->
						</div>
						<div class="col-md-3 col-sm-6 widget-holder widget-full-height">
							<div class="widget-bg">
								<div class="widget-body clearfix">
									<div class="widget-counter">
										<h6>Total Comments </h6>
										<h3 class="h1"><span class="counter">625</span></h3><i class="material-icons list-icon">rate_review</i>
									</div>
									<!-- /.widget-counter -->
								</div>
								<!-- /.widget-body -->
							</div>
							<!-- /.widget-bg -->
						</div>
						<div class="col-md-3 col-sm-6 widget-holder widget-full-height">
							<div class="widget-bg">
								<div class="widget-body clearfix">
									<div class="widget-counter">
										<h6>Total Questions</h6>
										<h3 class="h1"><span class="counter">2748</span></h3><i class="material-icons list-icon">chat</i>
									</div>
									<!-- /.widget-counter -->
								</div>
								<!-- /.widget-body -->
							</div>
							<!-- /.widget-bg -->
						</div>
						<div class="col-md-3 col-sm-6 widget-holder widget-full-height">
							<div class="widget-bg">
								<div class="widget-body clearfix">
									<div class="widget-counter">
										<h6>Total Users</h6>
										<h3 class="h1"><span class="counter">625</span></h3><i class="material-icons list-icon">people</i>
									</div>
									<!-- /.widget-counter -->
								</div>
								<!-- /.widget-body -->
							</div>
							<!-- /.widget-bg -->
						</div>
					</div>
					<div class="row">
						<!-- Chart: Registration History -->
						<div class="col-md-6 widget-holder widget-full-height">
							<div class="widget-bg">
								<div class="widget-heading clearfix">
									<h5>New Sign Ups</h5>
									<ul class="widget-actions">
										<li class="dropdown">
											<div class="predefinedRanges float-right fs-13 fw-500" style="cursor: pointer"><i class="list-icon material-icons color-danger">fiber_manual_record</i> <span></span> <i class="list-icon material-icons">expand_more</i>
											</div>
										</li>
									</ul>
									<!-- /.widget-actions -->
								</div>
								<!-- /.widget-heading -->
								<div class="widget-body clearfix">
									<div class="row">
										<p class="col-sm-6 small">The registrations are measured from the time that the redesign was fully implemented and after the first-e-mailing.</p>
										<div class="col-sm-6 text-right">
											<h5 class="h1 fw-300 sub-heading-font-family color-danger mr-tb-0">66.05&#37;</h5><small><i class="material-icons list-icon color-danger">arrow_drop_up</i> more registrations</small>
										</div>
									</div>
									<!-- /.row -->
									<div id="extraAreaMorris" height="280px"></div>
								</div>
								<!-- /.widget-body -->
							</div>
							<!-- /.widget-bg -->
						</div>
						<!-- /.widget-holder -->
						<!-- Charts: Sales Statistics -->
						<div class="col-md-6 widget-holder widget-full-height">
							<div class="widget-bg">
								<div class="widget-heading clearfix">
									<h5>New Actions</h5>
									<ul class="widget-actions">
										<li class="dropdown">
											<div class="predefinedRanges float-right fs-13 fw-500" style="cursor: pointer"><i class="list-icon material-icons color-danger">fiber_manual_record</i> <span></span> <i class="list-icon material-icons">expand_more</i>
											</div>
										</li>
									</ul>
									<!-- /.widget-actions -->
								</div>
								<!-- /.widget-heading -->
								<div class="widget-body clearfix">
									<div class="row">
										<div class="col-4 mr-b-20 text-center">
											<h6 class="h5 mr-b-0 mr-t-10"><i class="list-icon material-icons mr-r-5 small">star</i> 1481</h6><small>reviews</small>
										</div>
										<div class="col-4 mr-b-20 text-center">
											<h6 class="h5 mr-b-0 mr-t-10"><i class="list-icon material-icons mr-r-5 small">rate_review</i> 5,678</h6><small>comments</small>
										</div>
										<div class="col-4 mr-b-20 text-center">
											<h6 class="h5 mr-b-0 mr-t-10"><i class="list-icon material-icons small">chat</i> 27,321</h6><small>questions</small>
										</div>
									</div>
									<!-- /.row -->
									<div id="productLineHomeMorris" style="height: 280px"></div>
								</div>
								<!-- /.widget-body -->
							</div>
							<!-- /.widget-bg -->
						</div>
						<!-- /.widget-holder -->
					</div>

				</div>
				<!-- /.widget-list -->
@endsection

@section('tailJs')
<!-- Javascript -->
@endsection