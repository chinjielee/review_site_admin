@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
<!-- Additional JS -->
@section( 'headJs' )
	<!-- Javascript -->
@endsection
@section( 'content' )
	<!-- Content -->
	<!-- Page Title Area -->
	<div class="row page-title clearfix">
		<div class="page-title-left">
			<h5 class="mr-0 mr-r-5">Edit Service Details</h5>
		</div>
		<!-- /.page-title-left -->
	</div>
<!-- /.page-title -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="widget-list">
	<div class="row">
		<div class="col-lg-8">
			<form>
				<div class="form-group">
					<input class="form-control" placeholder="Service Name" type="text">
				</div>
				<div class="form-group">
				<h6 class="mb-2">Service Details</h6>
				<textarea data-toggle="tinymce" data-plugin-options='{ "height": 400 }'></textarea>
				</div>
			</form>
		</div>
		<div class="col-lg-4">
			<div class="card mb-3">
				<div class="card-header">
					<h6 class="m-0">Brands</h6>
				</div>
				<div class="card-body" style="height: 180px;overflow-y: scroll;">
					<div class="checkbox">
						<label class="checkbox-checked">
							<input type="checkbox" checked="checked"> <span class="label-text">Samsung</span>
						</label>
					</div>
					<div class="checkbox">
						<label class="checkbox-checked">
							<input type="checkbox" checked="checked"> <span class="label-text">Samsung</span>
						</label>
					</div>
					<div class="checkbox">
						<label class="checkbox-checked">
							<input type="checkbox" checked="checked"> <span class="label-text">Samsung</span>
						</label>
					</div>
				</div>
				<div class="card-action"><a href="#" class="card-link fw-500">+ Add New Brand</a>
				</div>
			</div>
			<div class="card mb-3">
				<div class="card-header">
					<h6 class="m-0">Service Categories</h6>
				</div>
				<div class="card-body" style="height: 180px;overflow-y: scroll;">
					<div class="checkbox p-0">
						<label class="checkbox-checked">
							<input type="checkbox" checked="checked"> <span class="label-text">Electronics</span>
						</label>
					</div>
					<ul class="pl-3 m-0" style="list-style: none;">
						<li>
					<div class="checkbox p-0">
						<label class="checkbox-checked">
							<input type="checkbox" checked="checked"> <span class="label-text">Mobile Phones</span>
						</label>
					</div>
						</li>
						<li>
					<div class="checkbox p-0">
						<label class="checkbox-checked">
							<input type="checkbox" checked="checked"> <span class="label-text">Mobile Phones</span>
						</label>
					</div>
						</li>
						<li></li>
					</ul>
					<div class="checkbox">
						<label class="checkbox-checked">
							<input type="checkbox" checked="checked"> <span class="label-text">Appliances</span>
						</label>
					</div>
					<div class="checkbox">
						<label class="checkbox-checked">
							<input type="checkbox" checked="checked"> <span class="label-text">Automotive Accessories</span>
						</label>
					</div>
					<div class="checkbox">
						<label class="checkbox-checked">
							<input type="checkbox" checked="checked"> <span class="label-text">Cars</span>
						</label>
					</div>
				</div>
				<div class="card-action"><a href="#" class="card-link fw-500">+ Add New Category</a>
				</div>
			</div>
			<div class="card mb-3">
				<div class="card-header">
					<h6 class="m-0">Service Image</h6>
				</div>
				<div class="card-body">
					<img src="{{ asset('/img/default.jpg')}}" style="width: 100%;">
				</div>
				<div class="card-action"><a href="#" class="card-link fw-500">+ Set Service Image</a>
				</div>
			</div>
			<div class="text-right">
			<button class="btn btn-color-scheme">Save Changes</button> <button class="btn btn-outline-color-scheme">Delete</button>
			</div>
		</div>
	</div>
</div>

@endsection