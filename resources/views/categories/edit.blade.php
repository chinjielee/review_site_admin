@extends( 'layouts.main' )
@section( 'headCss' )
	<link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
@endsection
<!-- Additional JS -->
@section( 'headJs' )
	<!-- Javascript -->
@endsection
@section( 'content' )
	<!-- Content -->
	<!-- Page Title Area -->
	<div class="row page-title clearfix">
		<div class="page-title-left">
			<h5 class="mr-0 mr-r-5">Add New / Edit Category</h5>
		</div>
		<!-- /.page-title-left -->
	</div>
<!-- /.page-title -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="widget-list">
	<div class="row">
		<div class="col-md-8">
			@if(Session::get('errors')||count( $errors ) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
						<i class="icon-remove-sign"></i> {{ $error }} </br>
					@endforeach
				</div>
			@endif
			<form id='save_cat_form' action="{{route('Action Manage Categories')}}" method="POST">
				<input type="hidden" name="cid" value="@if(!empty($arrCategoryDetail)){{$arrCategoryDetail->categories_id}}@endif">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label>Name</label>
					<input class="form-control" name="name" placeholder="" type="text" @if(!empty($arrCategoryDetail)) value='{{$arrCategoryDetail->categories_name}}'@endif required>
					<small>The name is how it appears on your site.</small>
				</div>
				<div class="form-group">
					<label>Select label</label>
					<select class="form-control" name="parent_cat_root" id='label'>
						@foreach($arrLabel as $key => $value)
							@if(!empty($arrCategoryDetail))
								@if($key == $arrCategoryDetail->label_id)
									<option value="{{$key}}" selected>{{ucfirst($value->categories_name)}}</option>
								@else
									<option value="{{$key}}">{{ucfirst($value->categories_name)}}</option>
								@endif
							@else
								<option value="{{$key}}">{{ucfirst($value->categories_name)}}</option>
							@endif
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Parent category</label>
					<select class="form-control" name="parent_cat" id="category">
						<option>---</option>
					</select>
					<small>Assign a parent term to create a hierarchy. The term Jazz, for example, would be the parent of Bebop and Big Band.</small>
				</div>
				<div class="form-group">
					<label for="text"><small class="nott"></small>Category Rating Options </label>
					<input class="form-control" placeholder="Rating Description" name="rating_option[]" type="text" value="@if(!empty(old('rating_option_1')))old('rating_option_1')@else{{$rating_option_1}}@endif">
					<input class="form-control" placeholder="Rating Description" name="rating_option[]" type="text" value="@if(!empty(old('rating_option_2')))old('rating_option_2')@else{{$rating_option_2}}@endif">
					<input class="form-control" placeholder="Rating Description" name="rating_option[]" type="text" value="@if(!empty(old('rating_option_3')))old('rating_option_3')@else{{$rating_option_3}}@endif">
					<input class="form-control" placeholder="Rating Description" name="rating_option[]" type="text" value="@if(!empty(old('rating_option_4')))old('rating_option_4')@else{{$rating_option_4}}@endif">
					<input class="form-control" placeholder="Rating Description" name="rating_option[]" type="text" value="@if(!empty(old('rating_option_5')))old('rating_option_5')@else{{$rating_option_5}}@endif">
				</div>
			</form>
			<div class="form-group">
				<label for="text"><small class="nott"></small>Category Image <small>(Dimension: 350px X 425px)</small></label>
				<div id="image_dropzone" class="dropzone">
					<input type="file" name="cat_image" class="hidden">
				</div>
				<div id="preview" style="display: none;">
					<div class="dz-preview dz-file-preview">
						<div class="dz-image"><img data-dz-thumbnail /></div>
						<div class="dz-details">
						</div>
						<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
						<div class="dz-error-message"><span data-dz-errormessage></span></div>
						<div class="dz-success-mark">
							<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
								<!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
								<title></title>
								<desc></desc>
								<defs></defs>
								<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
									<path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
								</g>
							</svg>
			
						</div>
						<div class="dz-error-mark">
							<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
								<!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
								<title></title>
								<desc></desc>
								<defs></defs>
								<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
									<g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
										<path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
									</g>
								</g>
							</svg>
						</div>
					</div>
				</div>
			</div>
			<button class="btn btn-color-scheme btn-saves">Save Changes</button> <button data-id="@if(!empty($arrCategoryDetail)){{$arrCategoryDetail->categories_id}}@endif" class="btn btn-outline-color-scheme delButton">Delete</button>
			<button class="btn btn-outline-color-scheme" onclick='goBack();return false;'>Back</button>
		</div>
	</div>
</div>
<script>
		var delUrl = "{{route('Delete Categories Image')}}";
		var uploadUrl = "{{ route('Upload Categories Image') }}";
		@if(!empty($arrCategoryDetail->image))
			preloadImage =  '{{$imageUrl}}';
		@endif
	</script>
@endsection

@section('tailJs')
	<script src="{{ asset('js/dropzone.min.js') }}"></script>
	<script src="{{ asset('js/dropzone-cat-config.js') }}"></script>	
	<script>
		var productList = JSON.parse('{!!$strProductList!!}');
		var serviceList = JSON.parse('{!!$strServiceList!!}');
		var movieList = JSON.parse('{!!$strMovieList!!}');
		var categoriesDetail = JSON.parse('{!!$strCategoryDetail!!}');
		$( document ).ready(function(){
			bindCategoriesList(productList);
			//$("div#image_dropzone").dropzone({});
				// console.log(categoriesDetail);
			@if(!empty($arrCategoryDetail))
				// Set parent value
					$('#label').val(categoriesDetail.label_id).change();
					switch(categoriesDetail.label_id){
							case '1',1:
								bindCategoriesList(productList);
								break;
							case '2',2:
								bindCategoriesList(serviceList);
								break;
							case '3',3:
								bindCategoriesList(movieList);
								break;
							default:
								bindCategoriesList(productList);
								break;
						}
				//Set Option Value
					// $('')
			@endif
		});

		$('.btn-saves').on('click', function(e){
			e.preventDefault(); //Prevent the normal submission action
			$('#save_cat_form').submit();
		})

		$('.delButton').on('click',function(e){
			$this = $(this);
			var catId = [$this.data('id')];
			let strCatid = JSON.stringify(catId);
			e.preventDefault(); //Prevent the normal submission action
			return swal({
				title: 'Are you sure?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete'
			}).then(function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						method: "DELETE",
						url: '{{route("Delete Categories")}}',
						data: {
							_token: $('meta[name="csrf-token"]').attr('content'),
							data: strCatid,
						},
					}).done(function(data) {
						if(data == "done"){
							swal({
								title:'Delete Success',
								type: 'info'
							}).then(function(){
								window.location.href = "{{route('Categories')}}";
							});
						} else {
							swal({
								title:'Error Occur, try again later',
								type: 'warning'
							});
						}
					});
				} else {
					return false;
				}
			});
		});

		$("#label").on('change',function(){
			var $this = $(this);
			switch(this.value){
				case '1':
					bindCategoriesList(productList);
					$this.parents('.button').addClass('review-type-active');
					$('#review_header').html('@lang('review.Product Details')');
				break;
				case '2':
					bindCategoriesList(serviceList);
					$this.parents('.button').addClass('review-type-active');
					$('#review_header').html('@lang('review.Service Details')');
				break;
				case '3':
					bindCategoriesList(movieList);
					$this.parents('.button').addClass('review-type-active');
					$('#review_header').html('@lang('review.Movie Details')');
				break;
				default:
					bindCategoriesList([]);
					$this.parents('.button').addClass('review-type-active');
					$('#review_header').html("@lang('review.Product Details')");
			}
		});
		function bindCategoriesList(jsonList) {
			let bindCategoriesList = 0;
			@if(!empty($arrCategoryDetail))
				bindCategoriesList = '{{$arrCategoryDetail->parent_category_id}}';
			@endif
			// Reset
			$('#category').find('option')
				.remove()
				.end()
				.append('<option value="">--</option>')
			var i = 0;
			
			for(var i=0; i < Object.keys(jsonList).length;i++){
				console.log('Loop : ',jsonList[i].categories_id);
				console.log('parent : ',bindCategoriesList);
				if(jsonList[i].categories_id != 1 && jsonList[i].categories_id != 2 && jsonList[i].categories_id != 3){
					if( jsonList[i].categories_id == bindCategoriesList){
						$('#category').append('<option value="'+jsonList[i].categories_id+'" selected>'+jsonList[i].categories_name+'</option>');
					} else {
						$('#category').append('<option value="'+jsonList[i].categories_id+'">'+jsonList[i].categories_name+'</option>');
					}					
				}
			}
			$('#category').prop('disabled',false);
			$('.selectpicker').selectpicker('refresh');
		}
		function goBack() {
            window.history.back();
        }
	</script>
@endsection