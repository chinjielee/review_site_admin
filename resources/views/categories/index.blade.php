@extends( 'layouts.main' )
@section( 'headCss' )
@endsection
<!-- Additional JS -->
@section( 'headJs' )
	<!-- Javascript -->
@endsection
@section( 'content' )
	<!-- Content -->
	<!-- Page Title Area -->
	<div class="row page-title clearfix">
		<div class="page-title-left">
			<h5 class="mr-0 mr-r-5">Categories</h5>
		</div>
		<!-- /.page-title-left -->
	</div>
<!-- /.page-title -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="widget-list">
	<div class="row">
		<div class="col-md-7">
			<a class="btn btn-default" href="{{route('Manage Categories','')}}"><i class="material-icons list-icon">add</i> Add New</a>
			<a class="btn btn-default delButton" href="#"><i class="material-icons list-icon">delete</i> Delete</a>
		</div>
		<!-- <div class="col-md-5">
			<div class="input-group">
				<select class="form-control">
					<option>Filter Category</option>
					<option>Option 2</option>
					<option>Option 3</option>
					<option>Option 4</option>
					<option>Option 5</option>
				</select>
				<span class="input-group-btn"><a class="btn btn-default" href="#">Filter</a></span>
			</div>
		</div> -->
	</div>
	
	<div class="row mb-2">
		<div class="col-md-7">
			<p class="mt-3 mb-0">
				<a href="{{route('Categories')}}">All Labels</a> <span class="mr-lr-10">|</span> <a href="{{route('Categories Label',['label'=>'product'])}}">Products</a> <span class="mr-lr-10">|</span> <a href="{{route('Categories Label',['label'=>'service'])}}">Services</a> <span class="mr-lr-10">|</span> <a href="{{route('Categories Label',['label'=>'movie'])}}">Movies</a>
			</p>
		</div>
	<div class="col-md-5 text-right">
	<div class="predefinedRanges mr-tb-10" style="cursor: pointer" data-plugin-options='{"opens": "right"}'><span></span>  <i class="list-icon material-icons">expand_more</i>
	</div>
	</div>        
	</div>  
	<div class="widget-bg">
		<div class="widget-body clearfix">
		@if (Session::has('success'))
			<div class="alert alert-success">
				{!! Session::get('success') !!}
			</div>
		@endif
			<table class="table table-striped table-responsive" data-toggle="datatables">
				<thead>
					<tr>
						<th class="bs-checkbox" data-field="state" tabindex="0">
							<div class="th-inner"><input class="sel-all" name="btSelectAll" type="checkbox"></div>
							<div class="fht-cell"></div>
						</th>
						<th>Image</th>
						<th>Name</th>
						<th>Label</th>
						<th>Count</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($arrCategory as $categories)
					
					<tr>
						<td class="bs-checkbox"><input data-index="0" name="btSelectItem" value="{{$categories->categories_id}}" type="checkbox"></td>
						<td><img class="mr-2" width="60" src="@if(empty($categories->imageUrl)){{ asset('/img/default.jpg')}}@else{{$categories->imageUrl}}@endif"></td>
						<td><a href='{{route("Manage Categories","$categories->category_slug")}}'>{{$categories->categories_name}}</a></td>
						<td>{{ucfirst($categories->label)}}</td>
						<td>{{$categories->total}}</td>
						<td><a href='{{route("Manage Categories","$categories->category_slug")}}' class="btn btn-default btn-sm m-1">Edit</a>
						<button data-id="{{$categories->categories_id}}" class="btn btn-default btn-sm m-1 delButton"><i class="material-icons list-icon">delete</i></button></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<!-- /.widget-body -->
	</div>
</div>

@endsection
@section('tailJs')
	<script>
		$( document ).ready(function() {
			enablePredefinedDateRangePicker();
		});
		$('.delButton').on('click',function(e){
			let objItems = [] ;
			e.preventDefault();
			let singleData = $(this).data('id');

			if(typeof(singleData) == "undefined" || singleData == ""){
				$.each($("[name='btSelectItem']:checked"), function(){
					objItems.push(this.value);
				});
			} else {
				objItems.push(singleData);
			}
			
			if(objItems.length <= 0){
				swal({
					type: 'info',
					title: 'Oops...',
					text: 'Please choose an items to be delete.',
				});	
				return false;
			}
			let data = JSON.stringify(objItems);
			return swal({
				title: 'Are you sure?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete'
			}).then(function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						method: "DELETE",
						url: '{{route("Delete Categories")}}',
						data: {
							_token: $('meta[name="csrf-token"]').attr('content'),
							data: data,
						},
					}).done(function(data) {
						if(data == "done"){
							swal({
								title:'Delete Success',
								type: 'info'
							}).then(function(){
								window.location.href = "{{route('Categories')}}";
							});
						} else {
							swal({
								title:'Error Occur, try again later',
								type: 'warning'
							});
						}
					});
				}
			});
		});

		$('.sel-all').on('click',function(e){
			if($(this).is(':checked')){
				$("[name='btSelectItem']").prop('checked', true);
			} else {
				$("[name='btSelectItem']").prop('checked', false);
			}
		});


		function enablePredefinedDateRangePicker(){
			var $el = $('.predefinedRanges');
			if ( !$el.length ) return;
			var defaults = {
				locale: {
				format: 'YYYY-MM-DD',
				},
				startDate: moment().startOf('month'),
				endDate: moment().endOf('month'),
				opens: "left",
				ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				}
			};
			$el.each( function() {
				var start = moment().startOf('month');
				var end = moment();
				var $this = $(this);
				var options = $this.data('plugin-options');
				options = $.extend({}, defaults, options);
				function cb(start, end, first="") {
					$this.find('span').html(start.format(options.locale.format) + ' - ' + end.format(options.locale.format));
					var redirect_url = document.URL;
					redirect_url = updateQueryStringParameter(redirect_url,'start',start.format('YYYY-MM-DD'));
					redirect_url = updateQueryStringParameter(redirect_url,'end',end.format('YYYY-MM-DD'));
					if(first != true){
						window.location = redirect_url;
					}
				}

				if( this.unifato === undefined ){
				this.unifato = {};
				}
				$this.daterangepicker(options, cb);
				this.unifato.daterangepicker = $this.data('daterangepicker');
				var first = true;
				cb(start, end, first);
				first = false;
			});
		}

		function updateQueryStringParameter(uri, key, value) {
			var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
			var separator = uri.indexOf('?') !== -1 ? "&" : "?";
			if (uri.match(re)) {
				return uri.replace(re, '$1' + key + "=" + value + '$2');
			}else {
				return uri + separator + key + "=" + value;
			}
		}
	</script>
@endsection