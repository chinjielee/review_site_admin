<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'WIKÅBÖ') }}</title>
	<!-- Fonts -->
	<link rel="stylesheet" href="{{ asset('css/pace.css')}}">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
	<!-- Styles -->
	<link href="{{ asset('vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="{{ asset('vendors/weather-icons-master/weather-icons.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('vendors/weather-icons-master/weather-icons-wind.min.css') }}" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.min.css" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.4/footable.min.js"></script>
    <script src="{{ asset('js/node_modules/jquery-tabledit/jquery.tabledit.min.js')}}"></script>
	<link href="{{ asset('css/style.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/custom-style.css')}}" rel="stylesheet" type="text/css">
	<!-- Head Libs -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	@yield('headCss')
	<!-- Javascript -->
	@yield('headJs')
</head>
<body class="header-light sidebar-expand">
	<div id="wrapper" class="wrapper">
		<nav class="navbar">
			<div class="navbar-header">
				<a href="{{ route('Dashboard')}}" class="navbar-brand">
                    <img class="logo-expand" alt="" src="{{ asset('demo/logo-expand.png')}}">
                    <img class="logo-collapse" alt="" src="{{ asset('demo/logo-collapse.png') }}">
                </a>
			</div>
			<ul class="nav navbar-nav">
				<li class="sidebar-toggle"><a href="javascript:void(0)" class="ripple"><i class="material-icons list-icon">menu</i></a>
				</li>
			</ul>
			<div class="spacer"></div>
			<div class="btn-list d-none d-md-flex"><a href="{{ route('User')}}" class="btn btn-sm btn-color-scheme"><i class="material-icons list-icon fs-24">people</i> User List</a>
			</div>
			<ul class="nav navbar-nav d-none d-lg-block">
				<li class="dropdown"><a href="{{route('Notification List')}}" class="ripple"><i class="material-icons list-icon">notifications</i> <span class="badge badge-border bg-color-scheme">{{$share->total}}</span></a>
				</li>
			</ul>
		</nav>
		<div class="content-wrapper">
			<aside class="site-sidebar scrollbar-enabled clearfix">
				<!-- /.side-user -->
				<!-- Sidebar Menu -->
				<nav class="sidebar-nav">
					<ul class="nav in side-menu mt-0">
						<li><a href="{{ route('Dashboard')}}" class="ripple"><i class="list-icon material-icons">network_check</i> <span class="hide-menu 
							@if($pageInfo->page == 'Dashboard')
								color-color-scheme
							@endif">Dashboard</span></a>
						</li>
						<li><a href="{{ route('Categories')}}" class="ripple"><i class="list-icon material-icons">list</i> <span class="hide-menu 
							@if($pageInfo->page == 'Categories')
								color-color-scheme
							@endif">Categories</span></a>
						</li>
						<!-- <li><a href="{{ route('Feature Image')}}" class="ripple"><i class="list-icon material-icons">image</i> <span class="hide-menu 
							@if($pageInfo->page == 'Feature Image')
								color-color-scheme
							@endif">Feature Image</span></a>
						</li> -->
						<li><a href="{{ route('Brands')}}" class="ripple"><i class="list-icon material-icons">label</i> <span class="hide-menu
							@if($pageInfo->page == 'Brands')
								color-color-scheme
							@endif">Brands</span></a>
						</li>
						<li class="menu-item-has-children
						@if($pageInfo->page == 'Products' || $pageInfo->page == 'Services' || $pageInfo->page == 'Movies' )
							active
						@endif
							"><a href="javascript:void(0);" class="ripple"><span class=""><i class="list-icon material-icons">bookmark_border</i> <span class="hide-menu">All Labels</span></span></a>
							<ul class="list-unstyled sub-menu">
								<li><a href="{{ route('Products',1)}}">Products List</a>
								</li>
								<li><a href="{{ route('Products',2)}}">Services List</a>
								</li>
								<li><a href="{{ route('Products',3)}}">Movie List</a>
								</li>
							</ul>
						</li>
						<li class="menu-item-has-children
						@if($pageInfo->page == 'All Reviews' || $pageInfo->page == 'All Comments' || $pageInfo->page == 'All Questions' )
							active
						@endif"><a href="javascript:void(0);" class="ripple"><i class="list-icon material-icons">rate_review</i> <span class="hide-menu">All Actions</span></a>
							<ul class="list-unstyled sub-menu">
								<li><a href="{{ route('Reviews')}}">All Reviews</a>
								</li>
								<li><a href="{{ route('Questions')}}">All Questions</a>
								</li>
								<li><a href="{{ route('Comments')}}">All Comments</a>
								</li>
							</ul>
						</li>
						<li class="menu-item-has-children
						@if($pageInfo->page == 'User' || $pageInfo->page == 'Update User')
							active
						@endif"><a href="javascript:void(0);" class="ripple"><i class="list-icon material-icons">people</i> <span class="hide-menu">User List</span></a>
							<ul class="list-unstyled sub-menu">
								<li><a href="{{ route('User')}}">All Users</a>
								</li>
								<li><a href="{{ route('Add User')}}">Add New User</a>
								</li>
							</ul>
						</li>
						<li class="menu-item-has-children
						@if($pageInfo->page == 'Admin' || $pageInfo->page == 'Update Admin')
							active
						@endif"><a href="javascript:void(0);" class="ripple"><i class="list-icon material-icons">person</i> <span class="hide-menu">Admin List</span></a>
							<ul class="list-unstyled sub-menu">
								<li><a href="{{ route('Admin')}}">All Admins</a>
								</li>
								<li><a href="{{ route('Add Admin')}}">Add New Admin</a>
								</li>
							</ul>
						</li>
						<li><a href="{{ route('Spam Filter')}}" class="ripple"><i class="list-icon material-icons">speaker_notes_off</i> <span class="hide-menu
							@if($pageInfo->page == 'Spam Filter')
								color-color-scheme
							@endif">Spam Filter</span></a>
						</li>
						<li class="menu-item-has-children
						@if($pageInfo->page == 'Profile' || $pageInfo->page == 'Change Password')
							active
						@endif"
						><a href="javascript:void(0);" class="ripple"><i class="list-icon material-icons">face</i> <span class="hide-menu">My Profile</span></a>
							<ul class="list-unstyled sub-menu">
								<li><a href="{{ route('Profile')}}">Edit Profile</a>
								</li>
								<li><a href="{{ route('Change Password')}}">Change Password</a>
								</li>
							</ul>
						</li>
						<li><a href="{{ route('logout')}}" ><i class="list-icon material-icons">settings_power</i> <span class="hide-menu">Log Out</span></a>
						</li>
					</ul>
				</nav>
			</aside>
		</div>
		<main class="main-wrapper clearfix">
			@yield('content')
		</main>
		<div class="chat-panel" hidden>
				<div class="card">
					<div class="card-header">
						<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
						<button type="button" class="minimize" aria-label="Minimize"><span aria-hidden="true"><i class="material-icons">expand_more</i></span>
                    </button> <span class="user-name">John Doe</span>
					</div>
					<!-- /.card-header -->
					<div class="card-block custom-scroll">
						<div class="messages custom-scroll-content scrollbar-enabled">
							<div class="current-user-message">
								<img class="user-image" width="30" height="30" src="{{ asset('demo/users/user1.jpg')}}" alt="">
								<div class="message">
									<p>Lorem ipsum dolor sit amet?</p><small>10:00 am</small>
								</div>
								<!-- /.message -->
							</div>
							<!-- /.current-user-message -->
							<div class="other-user-message">
								<img class="user-image" width="30" height="30" src="{{ asset('demo/users/user2.jpg')}}" alt="">
								<div class="message">
									<p>Etiam rhoncus. Maecenas tempus, tellus eget condi mentum rhoncus</p><small>10:00 am</small>
								</div>
								<!-- /.message -->
							</div>
							<!-- /.other-user-message -->
							<div class="current-user-message">
								<img class="user-image" width="30" height="30" src="{{ asset('demo/users/user1.jpg')}}" alt="">
								<div class="message">
									<img src="{{ asset('demo/chat-message.jpg')}}" alt=""> <small>10:00 am</small>
								</div>
								<!-- .,message -->
							</div>
							<!-- /.current-user-message -->
							<div class="current-user-message">
								<img class="user-image" width="30" height="30" src="{{ asset('demo/users/user1.jpg')}}" alt="">
								<div class="message">
									<p>Maecenas nec odio et ante tincidunt tempus.</p><small>10:00 am</small>
								</div>
								<!-- .,message -->
							</div>
							<!-- /.current-user-message -->
							<div class="other-user-message">
								<img class="user-image" width="30" height="30" src="{{ asset('demo/users/user2.jpg')}}" alt="">
								<div class="message">
									<p>Donec sodales :)</p><small>10:00 am</small>
								</div>
								<!-- /.message -->
							</div>
							<!-- /.other-user-message -->
						</div>
						<!-- /.messages -->
						<form action="javascript:void(0)" method="post">
							<textarea name="message" style="resize: none" placeholder="Type message and hit enter"></textarea>
							<ul class="list-unstyled list-inline chat-extra-buttons">
								<li class="list-inline-item"><a href="javascript:void(0)"><i class="material-icons">insert_emoticon</i></a>
								</li>
								<li class="list-inline-item"><a href="javascript:void(0)"><i class="material-icons">attach_file</i></a>
								</li>
							</ul>
							<button class="btn btn-color-scheme btn-circle submit-btn" type="submit"><i class="material-icons">send</i>
                        </button>
						</form>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
		</div>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
	<script src="{{ asset('js/bootstrap.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
	<script src="{{ asset('vendors/charts/utils.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>
	<script src="{{ asset('vendors/charts/excanvas.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/mithril/1.1.1/mithril.js"></script>
	<script src="{{ asset('vendors/theme-widgets/widgets.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/clndr/1.4.7/clndr.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.4/tinymce.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.4/themes/inlite/theme.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.4/jquery.tinymce.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.4/footable.min.js"></script>
    <script src="{{ asset('js/node_modules/jquery-tabledit/jquery.tabledit.min.js')}}"></script>
	<script src="{{ asset('js/theme.js')}}"></script>
	<script src="{{ asset('js/custom.js')}}"></script>
	@yield('tailJs')
</body>
</html>