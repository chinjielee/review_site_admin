@extends('layouts.main')
@section('headCss')
@endsection
@section('headJs')
@endsection
@section('content')

@if($flash = session('warning'))
	<div class="alert alert-warning" onclick="$(this).hide()">
		{{ $flash }}
	</div>
@endif
<div class="row page-title clearfix">
	<div class="page-title-left">
		<h5 class="mr-0 mr-r-5">Dashboard</h5>
	</div>
</div>
<div class="widget-list">
	<div class="row">
		<div class="col-md-3 col-sm-6 widget-holder widget-full-height">
			<div class="widget-bg">
				<div class="widget-body clearfix">
					<div class="widget-counter">
						<h6>Total Reviews</h6>
						<h3 class="h1"><a href="{{ route('Reviews')}}"><span class="counter">{{$review_count}}</span></a></h3><i class="material-icons list-icon">star</i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 widget-holder widget-full-height">
			<div class="widget-bg">
				<div class="widget-body clearfix">
					<div class="widget-counter">
						<h6>Total Comments </h6>
						<h3 class="h1"><a href="{{ route('Comments')}}"><span class="counter">{{$comment_count}}</span></a></h3><i class="material-icons list-icon">rate_review</i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 widget-holder widget-full-height">
			<div class="widget-bg">
				<div class="widget-body clearfix">
					<div class="widget-counter">
						<h6>Total Questions</h6>
						<h3 class="h1"><a href="{{ route('Questions')}}"><span class="counter">{{$question_count}}</span></a></h3><i class="material-icons list-icon">chat</i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 widget-holder widget-full-height">
			<div class="widget-bg">
				<div class="widget-body clearfix">
					<div class="widget-counter">
						<h6>Total Users</h6>
						<h3 class="h1"><a href="{{ route('User')}}"><span class="counter">{{$user_count}}</span></a></h3><i class="material-icons list-icon">people</i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 widget-holder widget-full-height">
			<div class="widget-bg">
				<div class="widget-heading clearfix">
					<h5>New Sign Ups</h5>
					<ul class="widget-actions">
						<li class="dropdown">
							<div id="predefinedRangesDashboard" class="predefinedRangesDashboard float-right fs-13 fw-500" style="cursor: pointer">
								<i class="list-icon material-icons color-warning">fiber_manual_record</i> 
								<span></span> 
								<i class="list-icon material-icons">expand_more</i>
							</div>
						</li>
					</ul>
				</div>
				<div class="widget-body clearfix">
					<!-- <div class="row">
						<p class="col-sm-6 small">The registrations are measured from the time that the redesign was fully implemented and after the first-e-mailing.</p>
						<div class="col-sm-6 text-right">
							<h5 class="h1 fw-300 sub-heading-font-family color-danger mr-tb-0">66.05&#37;</h5><small><i class="material-icons list-icon color-danger">arrow_drop_up</i> more registrations</small>
						</div>
					</div> -->
					<div id="extraAreaMorrisDashboard" height="280px"></div>
				</div>
			</div>
		</div>
		<div class="col-md-6 widget-holder widget-full-height">
			<div class="widget-bg">
				<div class="widget-heading clearfix">
					<h5>New Actions</h5>
					<ul class="widget-actions">
						<li class="dropdown">
							<div id="predefinedRangesDashboard2" class="predefinedRangesDashboard2 float-right fs-13 fw-500" style="cursor: pointer"><i class="list-icon material-icons color-warning">fiber_manual_record</i> <span></span> <i class="list-icon material-icons">expand_more</i>
							</div>
						</li>
					</ul>
				</div>
				<div class="widget-body clearfix">
					<div class="row">
						<div class="col-4 mr-b-20 text-center">
							<h6 class="h5 mr-b-0 mr-t-10"><i class="list-icon material-icons mr-r-5 small">star</i> <text id="reviews">0</text></h6><small>Reviews</small>
						</div>
						<div class="col-4 mr-b-20 text-center">
							<h6 class="h5 mr-b-0 mr-t-10"><i class="list-icon material-icons mr-r-5 small">rate_review</i> <text id="questions">0</text></h6><small>Questions</small>
						</div>
						<div class="col-4 mr-b-20 text-center">
							<h6 class="h5 mr-b-0 mr-t-10"><i class="list-icon material-icons small">chat</i> <text id="comments">0</text></h6><small>Comments</small>
						</div>
					</div>
					<div id="productLineHomeMorrisDashboard" style="height: 280px"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('tailJs')

<script>
	enablePredefinedDateRangePickerDashboard();
	function enablePredefinedDateRangePickerDashboard(){
      var $el = $('.predefinedRangesDashboard');
      var $el2 = $('.predefinedRangesDashboard2');
      if ( !$el.length ) return;
      if ( !$el2.length ) return;
      var defaults = {
        locale: {
          format: 'YYYY-MM-DD',
        },
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month'),
        opens: "left",
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      };
      $el.each( function() {
        var start = moment().startOf('month');
        var end = moment();
        var $this = $(this);
        var options = $this.data('plugin-options');
        options = $.extend({}, defaults, options);
        function cb(start, end) {
        	$this.find('span').html(start.format(options.locale.format) + ' - ' + end.format(options.locale.format));
        	extraAreaMorrisChartDashboard(start.format('YYYY-MM-DD 00:00:00'),end.format('YYYY-MM-DD 23:59:59'));
        }

        if( this.unifato === undefined ){
          this.unifato = {};
        }
        $this.daterangepicker(options, cb);
        this.unifato.daterangepicker = $this.data('daterangepicker');
        cb(start, end);
      });
      $el2.each( function() {
        var start = moment().startOf('month');
        var end = moment();
        var $this = $(this);
        var options = $this.data('plugin-options');
        options = $.extend({}, defaults, options);
        function cb(start, end) {
        	$this.find('span').html(start.format(options.locale.format) + ' - ' + end.format(options.locale.format));
        	productLineHomeMorrisChartDashboard(start.format('YYYY-MM-DD  00:00:00'),end.format('YYYY-MM-DD 23:59:59'));
        }

        if( this.unifato === undefined ){
          this.unifato = {};
        }
        $this.daterangepicker(options, cb);
        this.unifato.daterangepicker = $this.data('daterangepicker');
        cb(start, end);
      });
    }

	function extraAreaMorrisChartDashboard(start,end){
		$('#extraAreaMorrisDashboard').html('');
  		var ctx = document.getElementById("extraAreaMorrisDashboard");
      	if ( ctx === null ) return;
      	var chart = Morris.Area({
	        element: 'extraAreaMorrisDashboard',
	        data: generateData(start,end),
	        lineColors: ['#49C1D0'],
	        xkey: 'period',
	        ykeys: ['user'],
	        labels: ['User'],
	        lineWidth: 0,
	        fillOpacity: 1.0,
	        pointSize: 0,
	        pointFillColors: ['#fff'],
	        pointStrokeColors: ["#86d6e0"],
	        behaveLikeLine: false,
	        smooth: true,
	        gridLineColor: '#e0e0e0',
	        resize: false,
	        hideHover: 'auto'
	    });

      	$(document).on('SIDEBAR_CHANGED_WIDTH', chart.resizeHandler );
      	function generateData(start,end) {
	        var data = [];
	        $.ajax({
			    url: '{{config("app.url")}}/signupdata',
			    data: { 
			    	start: start, 
				    end: end
			  	},
			    async: false,
			    type: 'GET',
			    success:function(response) {
			    	if (response === undefined || response.length == 0) {
			    		return data = [{'period':(2019)+'','user':0},{'period':(2020)+'','user':0},{'period':(2021)+'','user':0},{'period':(2022)+'','user':0},{'period':(2023)+'','user':0}];
			    	}else{
				    	return data = response;
				    }
			    },
			    error:function(response) {
			        console.log(response.responseText);
			    },
			});
	        return data;
      	}
    }

    function productLineHomeMorrisChartDashboard(start,end){
    	$('#productLineHomeMorrisDashboard').html('');
      	var ctx = document.getElementById("productLineHomeMorrisDashboard");
      	if ( ctx === null ) return;
      	var chart = Morris.Area({
	        element: 'productLineHomeMorrisDashboard',
	        data: generateData(start,end),
	        xkey: 'period',
	        ykeys: ['reviews', 'questions', 'comments'],
	        labels: ['Reviews', 'Questions', 'Comments'],
	        ymax: 'auto',
	        ymin: 'auto',
	        pointSize: 3,
	        fillOpacity: 0,
	        pointStrokeColors:['#00bfc7', '#fb9678', '#9675ce'],
	        behaveLikeLine: true,
	        gridLineColor: '#e0e0e0',
	        lineWidth: 1,
	        hideHover: 'auto',
	        lineColors: ['#00bfc7', '#fb9678', '#9675ce'],
	        resize: true
      	});

      	$(document).on('SIDEBAR_CHANGED_WIDTH', chart.resizeHandler);
      	function generateData(start,end) {
        	var data = [];
	        $.ajax({
			    url: '{{config("app.url")}}/actiondata',
			    data: { 
			    	start: start, 
				    end: end
			  	},
			    async: false,
			    type: 'GET',
			    success:function(response) {
			    	if (response['full_list'] === undefined || response['full_list'].length == 0) {
			    		$('#reviews').html("0");
			    		$('#questions').html("0");
			    		$('#comments').html("0");
			    		return data = [
			    		{'period':(2019)+'','reviews':0,'questions':0,'comments':0},
			    		{'period':(2020)+'','reviews':0,'questions':0,'comments':0},
			    		{'period':(2021)+'','reviews':0,'questions':0,'comments':0},
			    		{'period':(2022)+'','reviews':0,'questions':0,'comments':0},
			    		{'period':(2023)+'','reviews':0,'questions':0,'comments':0}];
			    	}else{
				    	$('#reviews').html(response['reviews']);
			    		$('#questions').html(response['questions']);
			    		$('#comments').html(response['comments']);
			    		return data = response['full_list'];
				    }
			    },
			    error:function(response) {
			        console.log(response.responseText);
			    },
			});
        return data;
      }
    }
</script>
@endsection