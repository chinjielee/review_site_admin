<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Brands extends Model
{
    protected $fillable = [
        'brand_title','brand_info', 'images', 'brand_status'
    ];
    protected $primaryKey = 'brands_id';
    protected $table = 'brands';

    public function getCountAllBrands(){
        return self::count();
    }

    public function getCountByDate($start ,$end){
        return DB::table('brands')->select(DB::raw('DATE(created_at) as period'), DB::raw('count(*) as brands'))->where([['created_at', '>=', $start],['created_at', '<=', $end]])->groupBy('period')->get();
    }

    public function getAllBrands($keyword){
        return self::get();
    }
    
    public function getAllBrandsNameKeyById(){
        return self::all()->pluck('brand_title','brands_id');
    }
}
