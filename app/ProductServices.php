<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Categories;
use DB;
class ProductServices extends Model
{

    protected $fillable = [
        'product_service_title','product_slug' , 'product_service_desc', 'brands_id','category_id','label_id' , 'product_service_attributes','image','product_rating', 'status', 'created_at', 'updated_at'
    ];
    protected $primaryKey = 'product_services_id';
    protected $table = 'product_services';

    const LIMIT_FIRST_LOAD = 1000;    
    const IMAGE_PATH = 'images/product';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_DRAFT = 3;

    public function questions()
    {
        return $this->hasMany('App\ProductQuestion','product_services_id','product_services_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\ProductReview','product_services_id','product_services_id');
    }

    public function getProductListByLabel($type, $status, $start = "", $end = "") {
        if(($start != "") && ($end != "")){
            if($status != 0){
                $result = self::where('label_id', $type)->where([['status', $status],['created_at', '>=', $start],['created_at', '<=', $end]]);
            } else {
                $result = self::where('label_id', $type)->where([['created_at', '>=', $start],['created_at', '<=', $end]]);
            }
        } else {
            if($status != 0){
                $result = self::where('label_id', $type)->where('status', $status);
            } else {
                $result = self::where('label_id', $type);
            }
        }
        $result->withCount([
            'questions',
            'questions AS questions'
        ])->withCount([
            'reviews',
            'reviews AS reviews'
        ]);
        return $result->get();
    }

    public function getProductListAll($start="" , $end="") {
        if(($start != "") && ($end != "")){
            return self::withCount([
                'questions',
                'questions AS questions'
            ])->withCount([
                'reviews',
                'reviews AS reviews'
            ])->where([['created_at', '>=', $start],['created_at', '<=', $end]])->get();
        } else {
            return self::withCount([
                'questions',
                'questions AS questions'
            ])->withCount([
                'reviews',
                'reviews AS reviews'
            ])->take(self::LIMIT_FIRST_LOAD)->get();
        }
    }

    public function getProductDetailsById($productId) {
        return self::where('product_services_id', $productId)->first();
    }

    /**
    * Save and move to saved images from temp storage.
    *
    * @param Request $request
    */
    public function prodImageSave($adminId, $pId)
    {
        if(empty($adminId) || empty($pId)) {
             return false;
        } else {
            $image_path = public_path(self::IMAGE_PATH);
            $displayProductId = base64_encode($pId);
            $des_image_path = $image_path."/".$displayProductId."/";
            $temp_image_path = $image_path."/temp/".$adminId;
            $temp_main_image_path = $image_path."/temp/".$adminId."/main";
            $strImage = '';
            $strMainImage = '';
            if (file_exists($temp_image_path)) {
                foreach (scandir($temp_image_path) as $file) {
                    if ('.' === $file) continue;
                    if ('..' === $file) continue;
                    if(!is_dir($temp_image_path."/".$file)){
                        $strImage .= ",".$file;
                        if(file_exists($des_image_path)) {
                            if(!file_exists($des_image_path.$file)){
                                rename($temp_image_path."/".$file,$des_image_path.$file);
                            }
                        } else {
                            mkdir($des_image_path, 0777, true);
                            rename($temp_image_path."/".$file,$des_image_path.$file);
                        }
                    }
                }
            }
            if (file_exists($temp_main_image_path)) {
                foreach (scandir($temp_main_image_path) as $file) {
                    if ('.' === $file) continue;
                    if ('..' === $file) continue;
                    $strMainImage = $file;
                    if(file_exists($des_image_path)) {
                        rename($temp_main_image_path."/".$file,$des_image_path.$file);
                    } else {
                        mkdir($des_image_path, 0777, true);
                        rename($temp_main_image_path."/".$file,$des_image_path.$file);
                    }
                }
            }

            if(!empty($strImage) || !empty($strMainImage)){
                $arrImagesFinal = array();
                // Arrange main image as the 1st image
                if(!empty($strMainImage)) {
                    array_push($arrImagesFinal,$strMainImage);
                }
                if(!empty($strImage)){
                    $strImage = substr($strImage,1);
                    $arrImages = explode(',',$strImage);
                    foreach($arrImages as $img){
                        array_push($arrImagesFinal,$img);
                    }
                }
                // Update Product Review Image
                $strImagesFinal = implode(',',$arrImagesFinal);
                return $this->_updateImagePath($strImagesFinal, $pId);    
            } else {
                return true;
            }

         }
    }

    private function _clearProductImage($displayPid){
        $path = public_path(self::IMAGE_PATH.$displayPid.'/');
        $this->_rrmdir($path);
    }


    private function _rrmdir($dir) {
        if (is_dir($dir)) {
          $objects = scandir($dir); 
          foreach ($objects as $object) { 
            if ($object != "." && $object != "..") {
              if (is_dir($dir."/".$object))
                $this->_rrmdir($dir."/".$object);
              else
                unlink($dir."/".$object);
            } 
          }
          rmdir($dir); 
        } 
    }

    public function countByBrandIdDate($start="",$end=""){
        if(empty($start) || empty($end)) {
            return collect(DB::select('SELECT brands.*,count(1) as total FROM brands LEFT JOIN product_services ON brands.brands_id = product_services.brands_id GROUP BY brands.brands_id'))->keyBy('brands_id')->toArray();
        } else {
            return collect(
                    DB::select("SELECT brands.*,count(1) as total FROM brands LEFT JOIN product_services ON brands.brands_id = product_services.brands_id WHERE brands.created_at >= '$start' AND brands.created_at < '$end' GROUP BY brands.brands_id")
            )->keyBy('brands_id')->toArray();
        }
    }

    public function columnMapping($arrInput){
        $returnInput = array();
        $objCat = new Categories();
        if(!empty($arrInput['psid'])){
            $returnInput['product_services_id'] = $arrInput['psid'];
        }
        $returnInput['product_service_title'] = isset($arrInput['product_name'])?$arrInput['product_name'] :"";
        $returnInput['product_service_desc'] = isset($arrInput['product_desc'])?$arrInput['product_desc'] :"";
        $returnInput['product_service_attributes'] = isset($arrInput['detail'])?$arrInput['detail'] :"";
        $returnInput['product_slug'] = isset($arrInput['product_name'])?strtolower(str_replace(" ", "-", $arrInput['product_name'])) :"";
        $returnInput['category_id'] = isset($arrInput['category'])?$arrInput['category']:0;
        $returnInput['brands_id'] = isset($arrInput['brand'])?$arrInput['brand']:0;
        $objLabel = $objCat->getCategoriesLabel($arrInput['category']);
        if(!empty($objLabel->label_2)){
            $label_id = $objLabel->label_id_2;
        } else {
            $label_id = $objLabel->label_id_1;
        }
        $returnInput['label_id'] = $label_id;
        $returnInput['status'] = self::STATUS_ACTIVE;
        $returnInput['created_at'] = Carbon::now();
        $returnInput['updated_at'] = Carbon::now();
        return $returnInput;
    }

    public function updCreateProduct($arrInput) {
        if(isset($arrInput['product_services_id'])){
            return self::updateOrCreate(['product_services_id' => $arrInput['product_services_id']], $arrInput)->product_services_id;
        } else {
            return self::updateOrCreate($arrInput)->product_services_id;
        }
    }

    public function del($pid) {
        return self::whereIn('product_services_id', $pid)
                    ->update(['status'=>self::STATUS_INACTIVE]);
    }

    private function _updateImagePath($strImage, $cId) {
        $catRow = self::find($cId);
        $catRow->images = $strImage;
        return $catRow->save();
    }


}
