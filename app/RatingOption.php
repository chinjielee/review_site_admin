<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RatingOption extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'type','option_json','updated_at','created_at'
    ];
    protected $primaryKey = 'rating_option_id';
    protected $table = 'rating_option';
    protected $timestamp = true;

    public function insert($input) {
        self::create($input);
        
    }

    public function _processInputToBeInsert($input = array()) {
        $return_input = array();
        if(isset($input['category_id']) && !empty($input['category_id'])) {
            $return_input['category_id'] = $input['category_id'];
        } else {
            return false;
        }

        if(isset($input['rating_option']) && !empty($input['rating_option'])) {
            if(is_array($input['rating_option'])) {
                $return_input['option_json'] = json_encode((object)$input['rating_option']);
            } else {
                $return_input['option_json'] = $input['rating_option'];
            }
        }
        return $return_input;
    }

    public function getRatingOption($category_id = 0 ){
        $arrRatingOption = self::where('category_id',$category_id)->first();
        if(!empty($arrRatingOption)){
            return $arrRatingOption;
        } else {
            return false;
        }
        
    }

    public function processDisplayRatingOption($strJsonRatingOption) {
        $arrRatingOption = json_decode($strJsonRatingOption,true);
        return $arrRatingOption;
    }


    public function insertOrUpdate($arrInput = array()) {
        if(isset($arrInput['category_id'])){
            return self::updateOrCreate(['category_id' => $arrInput['category_id']], $arrInput)->category_id;
        } else {
            return self::updateOrCreate($arrInput)->category_id;
        }
    }

}
