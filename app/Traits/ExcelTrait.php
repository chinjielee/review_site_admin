<?php 
namespace App\Traits;
// use App\Http\Requests;
// use Illuminate\Http\Request;
// use Input;
// use DB;
// use Session;
use Excel;
trait ExcelTrait
{

    // public function importExport()
    // {
    //     return view('importExport');
    // }
    public function downloadExcel($data,$filename = 'export',$type = 'xls')
    {
        if(!in_array($type, array('xls','csv','xlsx'))){
            $type = 'xls';
        }
        return Excel::create($filename, function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    // public function importExcel(Request $request)
    // {
    //     if($request->hasFile('import_file')){
    //         Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
    //             foreach ($reader->toArray() as $key => $row) {
    //                 $data['title'] = $row['title'];
    //                 $data['description'] = $row['description'];

    //                 if(!empty($data)) {
    //                     DB::table('post')->insert($data);
    //                 }
    //             }
    //         });
    //     }

    //     Session::put('success', 'Youe file successfully import in database!!!');

    //     return back();
    // }
}