<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DiscussStat;

class runDailyDiscussLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "discusslog:stat";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running the daily stat for the top discussed product';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $objDiscussStat = new DiscussStat();
        $result= $objDiscussStat->updateInsertTopDiscussProduct();
    }
}
