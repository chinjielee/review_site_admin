<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class SpamFilter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text','rate', 'type','status','updated_at','created_at'
    ];
    protected $primaryKey = 'spam_filter_id';
    protected $table = 'spam_filter';
    protected $timestamp = true;

    public function reset(){
        self::truncate();
    }

    public function bulkInsert($arrayInsertInput) {
        return self::insert($arrayInsertInput);
    }

}
