<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProductReview extends Model
{
    const STATUS_PUBLISHED = 1;
    const STATUS_CANCELLED = 2;
    const STATUS_BLOCKED = 3;
    const INVOICE_IMAGE_PATH = 'images/invoice';
    const REVIEW_IMAGE_PATH = 'images/review';
    protected $table = 'product_reviews';
    protected $primaryKey = 'product_reviews_id';
    protected $fillable = array(
        'product_services_id',
        'temp_product_services_id',
        'review_title',
        'comments',
        'key_highlight',
        'review_image',
        'up_vote',
        'down_vote',
        'rating',
        'anonymous',
        'purchased_from',
        'purchased_date',
        'invoice_image',
        'created_at',
        'updated_at'
    );
    public $timestamps = true;

    public function getTotalCount(){
        return self::count();
    }

    public function getCountByDate($start ,$end){
        return DB::table('product_reviews')->select(DB::raw('DATE(created_at) as period'), DB::raw('count(product_reviews_id) as reviews'))->where([['created_at', '>=', $start],['created_at', '<=', $end]])->groupBy('period')->get();
    }

    public function getFullList($status="",$start="",$end=""){
        if(($start != "") && ($end != "")){
            if($status != ""){
                return DB::table('product_reviews')->select('product_reviews.*','users.first_name','users.last_name','users.email',DB::raw('COUNT(review_report_fraud.product_review_id) as spam'),'product_services.product_service_title','product_services.product_slug')->leftJoin('review_report_fraud','product_reviews.product_reviews_id','review_report_fraud.product_review_id')->leftJoin('product_services','product_reviews.product_services_id','product_services.product_services_id')->join('users','product_reviews.user_id','users.user_id')->where([['product_reviews.created_at', '>=', $start],['product_reviews.created_at', '<=', $end],['product_reviews.status',$status]])->groupBy('product_reviews_id')->get();
            }else{
                return DB::table('product_reviews')->select('product_reviews.*','users.first_name','users.last_name','users.email',DB::raw('COUNT(review_report_fraud.product_review_id) as spam'),'product_services.product_service_title','product_services.product_slug')->leftJoin('review_report_fraud','product_reviews.product_reviews_id','review_report_fraud.product_review_id')->leftJoin('product_services','product_reviews.product_services_id','product_services.product_services_id')->join('users','product_reviews.user_id','users.user_id')->where([['product_reviews.created_at', '>=', $start],['product_reviews.created_at', '<=', $end]])->groupBy('product_reviews_id')->get();
            }
        }else if($status != ""){
            return DB::table('product_reviews')->select('product_reviews.*','users.first_name','users.last_name','users.email',DB::raw('COUNT(review_report_fraud.product_review_id) as spam'),'product_services.product_service_title','product_services.product_slug')->leftJoin('review_report_fraud','product_reviews.product_reviews_id','review_report_fraud.product_review_id')->leftJoin('product_services','product_reviews.product_services_id','product_services.product_services_id')->join('users','product_reviews.user_id','users.user_id')->where('product_reviews.status', $status)->groupBy('product_reviews_id')->get();
        }else{
            return DB::table('product_reviews')->select('product_reviews.*','users.first_name','users.last_name','users.email',DB::raw('COUNT(review_report_fraud.product_review_id) as spam'),'product_services.product_service_title','product_services.product_slug')->leftJoin('review_report_fraud','product_reviews.product_reviews_id','review_report_fraud.product_review_id')->leftJoin('product_services','product_reviews.product_services_id','product_services.product_services_id')->join('users','product_reviews.user_id','users.user_id')->groupBy('product_reviews_id')->get();
        }
    }

    public function getProcessRatingDetailByPendingServicesId($ppsid){
        $productDetail = self::where('temp_product_services_id',$ppsid)->first();
        if(!empty($productDetail)){
            $ratingDetail = $productDetail->rating;
            if(!empty($ratingDetail)) {
                return $this->_getNewProcessedProductRating($ratingDetail);
            } else {
                return "{}";
            }
        } else {
            return "{}";
        }
    }

    public function getCountByStatus(){
        return DB::table('product_reviews')->select('status', DB::raw('count(`product_reviews_id`) as total'))->groupBy('status')->get();
    }

    public function updateApprovedProductId($tempId, $productServiceId) {
        $review =  self::where('temp_product_services_id', $tempId)->first();
        // Not all submit review
        if(!empty($review)){
            $review->product_services_id = $productServiceId;
            $review->save();
        }
    }

    private function _getNewProcessedProductRating($objJsonRating) {
        $objRating = json_decode($objJsonRating);
        // ReCount
        if(!isset($objRating->count)) {
            $objRating->count = 1;
        } else {
            $oriCount = $objRating->count;
        }        
        return json_encode($objRating);
    }
}
