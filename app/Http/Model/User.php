<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DB;

class User extends Model
{
    use Notifiable;
    protected $fillable = [
        'first_name','last_name', 'email', 'password','mobile_code','mobile', 'gender', 'dob', 'country','email_token','language','user_status','created_by'
    ];
    protected $primaryKey = 'user_id';
    protected $table = 'users';

    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getCountAllUser(){
        return self::count();
    }

    public function getCountByDate($start ,$end){
        return DB::table('users')->select(DB::raw('DATE(created_at) as period'), DB::raw('count(*) as user'))->where([['created_at', '>=', $start],['created_at', '<=', $end]])->groupBy('period')->get();
    }

    public function getAllUser($keyword){
        return self::get();
    }

    public function getFullNameAttribute($value)
    {
        return $this->attributes['first_name']." ".$this->attributes['last_name'];
    }
}
