<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\ExcelTrait;
use App\ProductServices;
use App\Brands;
use App\Categories;
use App\ProductReview;
use Auth;
use File;
use App\PendingProductService;

class LabelsController extends Controller
{
    use ExcelTrait;    
    const PRODUCT_LABEL_ID = 1;
    const SERVICE_LABEL_ID = 2;
    const MOVIE_LABEL_ID = 3;

    const PRODUCT_IMAGE_PATH = 'images/product';

    public function __construct()
    {
        $this->prod_image = public_path(self::PRODUCT_IMAGE_PATH);
    }

    protected function prodInsertValidator(array $data)
    {
        $messages = [
            'product_name.required' => 'Please give this product a name.',
            'category.required' => 'Please select a category for the product',
            'category.min' => 'Please select a category for the product'
        ];
        $validator = Validator::make($data, [
             'product_name'=> 'required|string|max:255',
             'category'=>'required|numeric|min:1'
        ],$messages);
        return $validator;
    }

	public function products(Request $request, $type = 0, $status = 0) {

        $pageInfo = (object)array();
        switch($type){
            case 1:
                $pageInfo->page = "Products";
                break;
            case 2:
                $pageInfo->page = "Services";
                break;
            case 3:
                $pageInfo->page = "Movies";
                break;
        }
        $objProductService = new ProductServices();
        $objBrands = new Brands();
        $objCategories = new Categories();
        $start = $end = "";
        if(($request->input('start') !== NULL) && ($request->input('end') !== NULL)){
            $start = $request->input('start')." 00:00:00";
            $end = $request->input('end')." 23:59:59";
        }
        if($type == 0 ){
            $arrProductList = $objProductService ->getProductListAll($start,$end);
        } else {
            if($status == $objProductService::STATUS_DRAFT ){
                // Get the Pending Product list 
                $objPendingProductService = new PendingProductService();
                $arrProductList = $objPendingProductService->getPendingProductDetails($start,$end, $type, $status);
            } else {
                $arrProductList = $objProductService ->getProductListByLabel($type, $status , $start,$end);
            }
        }
        foreach($arrProductList as $product){
            $product->display_id = base64_encode($product->product_services_id);
            $product->image = explode(',',$product->images)[0];
        }
        $arrBrands = $objBrands->getAllBrandsNameKeyById();
        $arrCategories = $objCategories->getCategoryList()->keyBy('categories_id');
        return view('products-list.index',compact('pageInfo','arrProductList','arrBrands','arrCategories','type','status'));
    }

    public function manageProduct(Request $request, $product_services_id = 0 ) {
        $pageInfo = (object)array();
        $pageInfo->page = "Edit Products";
        $objProductService = new ProductServices();
        $admin = Auth::user();
        if(!empty($admin->admin_id)){
            $adminId = $admin->admin_id;
        } else {
            return Response::json([
                'message' => 'Only admin can access to this page.'
            ], 400);
        }
        $productDetail = array();
        // Create new product
        if(!empty($product_services_id)){
            $this->clearTempProductImage($adminId);
            $productDetail = $objProductService->getProductDetailsById($product_services_id);
            $productDetail->jsonAttribute = json_decode($productDetail->product_service_attributes,true);
        } else {
            $product_services_id = 0;
        }
        $objBrands = new Brands();
        $objCategories = new Categories();
        $arrBrands = $objBrands->getAllBrandsNameKeyById();
        $arrCategories = $objCategories->getCategoryList()->keyBy('categories_id');
        $arrCategories = $objCategories->preProcessCategoriesForSelect($arrCategories);
        return view('products-list.edit',compact('pageInfo','arrBrands','arrCategories','productDetail','product_services_id'));
    }

    public function manageProductPending($pending_product_service_id){
        $pageInfo = (object)array();
        $pageInfo->page = "Edit Products";
        $objPendingProductService = new PendingProductService();
        $admin = Auth::user();
        if(!empty($admin->admin_id)){
            $adminId = $admin->admin_id;
        } else {
            return Response::json([
                'message' => 'Only admin can access to this page.'
            ], 400);
        }
        $productDetail = array();
        // Create new product
        if(!empty($pending_product_service_id)) {
            $this->clearTempProductImage($adminId);
            $productDetail = $objPendingProductService->getPendingProductDetailsById($pending_product_service_id);
            if(!empty($productDetail)){
                if(!empty($productDetail->product_attributes)){
                    $productDetail->jsonAttribute = json_decode($productDetail->product_attributes,true);
                }
            }
        } else {
            $pending_product_service_id = 0;
        }
        $objBrands = new Brands();
        $objCategories = new Categories();
        $arrBrands = $objBrands->getAllBrandsNameKeyById();
        $arrCategories = $objCategories->getCategoryList()->keyBy('categories_id');
        $arrCategories = $objCategories->preProcessCategoriesForSelect($arrCategories);
        return view('products-list.pending-edit',compact('pageInfo','arrBrands','arrCategories','productDetail','pending_product_service_id'));
    }
    
    public function delProd(Request $request) {
        $objProductService = new ProductServices();
        $arrInput = $request->input();
        if(!empty($arrInput['psid'])){
            $objProductService->del($arrInput['psid']);
            return redirect()->route('Products',1)->with('success', 'Product created successfully');   ;
        } else {
            return redirect()->back()->withInput()->withErrors(['Product Not Exist.']);
        }
    }
    public function mngProd(Request $request) {
        $validator = $this->prodInsertValidator($request->all());
        $validator->validate();
        $admin = Auth::user();
        $objProductService = new ProductServices();
        $objProductReview = new ProductReview();
        $arrInput = $request->input();
        if(!isset($arrInput['url'])){
            $arrInput['url'] = '';
        }

        $arrInput['detail'] = $this->processProductDetail($arrInput['key'],$arrInput['val'], $arrInput['url']);
        $arrMappedInput = $objProductService->columnMapping($arrInput);
        // Generating Rating Json 
        if(!empty($arrInput['ppsid'])){
            $arrMappedInput['product_rating'] = $objProductReview->getProcessRatingDetailByPendingServicesId($arrInput['ppsid']);
        }
        $objCategories = new Categories();
        $objParentLabel = $objCategories->getCategoriesLabel($arrMappedInput['category_id']);
        $parentId = 1;
        if(!empty($objParentLabel)){
            if(!empty($objParentLabel->label_id_2)){
                $parentId = $objParentLabel->label_id_2;
            } else {
                if(!empty($objParentLabel->label_id_1)){
                    $parentId = $objParentLabel->label_id_1;
                } else {
                    $parentId = $objParentLabel->categories_id;
                }
            }
        }
        
        $inserted_id = $objProductService->updCreateProduct($arrMappedInput);
        $blnInserted = $objProductService->prodImageSave($admin->admin_id, $inserted_id); 
        if($blnInserted) {
            // Add from pending
            if(!empty($arrInput['ppsid'])){
                //update pending product to sucess
                $objPending = new PendingProductService();
                $objPending->updatePendingProductToPublished($arrInput['ppsid']);
                // Insert into Review
                $objProductReview->updateApprovedProductId($arrInput['ppsid'],$inserted_id);
            }

            return redirect()->route('Products',$parentId)->with('success', 'Product created successfully');   ;
        } else {
            return redirect()->back()->withInput()->withErrors(['Somethings went wrong.']);
        }
    }

    public function rejectPendingProduct(Request $request){
        $arrRequest = $request->all();
        $ppsid = $arrRequest['data'];
        $objPendingProduct = new PendingProductService();
        $blnDel = $objPendingProduct->updatePendingProductToDenied($ppsid);
        if($blnDel){
            return 'done';
        } else {
            return 'fail';
        }
    }

    public function delete_product(Request $request) {
        $arrRequest = $request->all();
        $pid = json_decode($arrRequest['data']);
        $objProduct = new ProductServices();
        if(is_array($pid)){
            $blnDel = $objProduct->del($pid);
        } else {
            $blnDel = $objProduct->del([$pid]);
        }
        
        if($blnDel){
            return 'done';
        } else {
            return 'fail';
        }
    }

    /**
    * Saving images uploaded through XHR Request.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function prodImageStore(Request $request) {
        // AdminUser & productId
        $admin = Auth::user();
        if(!empty($admin->admin_id)){
            $adminId = $admin->admin_id;
        } else {
            return Response::json([
                'message' => 'Image upload Failed, Missing user details'
            ], 400);
        }
        $upload_path = $this->prod_image."/temp/".$adminId."";

        $photos = $request->file('file');
        
        if (!is_array($photos)) {
            $photos = [$photos];
        }
        if (!is_dir($upload_path)) {
            mkdir($upload_path, 0777, true);
        }
        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];
            //$name = sha1(date('YmdHis') . str_random(30));
            $name = $request->type."_".pathinfo($photo->getClientOriginalName())['filename'];
            $name = str_replace([' ',',','.'],'',$name);
            $save_name = microtime().$name . '.' . $photo->getClientOriginalExtension();
            $photo->move($upload_path, $save_name);
        }
        $uploaded_url = url('/temp/'.$adminId."/".$save_name);

        return Response::json([
            'url' => $uploaded_url,
            'save_file' => $save_name,
            'message' => 'Image upload Successfully'
        ], 200);
    }

    /**
    * Saving images uploaded through XHR Request.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function prodMainImageStore(Request $request) {
        // AdminUser & productId
        $admin = Auth::user();
        if(!empty($admin->admin_id)){
            $adminId = $admin->admin_id;
        } else {
            return Response::json([
                'message' => 'Image upload Failed, Missing user details'
            ], 400);
        }
        $this->clearTempProductImage($adminId, true);
        $upload_path = $this->prod_image."/temp/".$adminId."/main";

        $photos = $request->file('file');
        
        if (!is_array($photos)) {
            $photos = [$photos];
        }
        if (!is_dir($upload_path)) {
            mkdir($upload_path, 0777, true);
        }
        
        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];
            //$name = sha1(date('YmdHis') . str_random(30));
            $name = $request->type."_".pathinfo($photo->getClientOriginalName())['filename'];
            $name = str_replace([' ',',','.'],'',$name);
            $save_name = microtime().$name . '.' . $photo->getClientOriginalExtension();
            $photo->move($upload_path, $save_name);
        }
        
        $uploaded_url = url('/temp/'.$adminId."/main/".$save_name);

        return Response::json([
            'url' => $uploaded_url,
            'save_file' => $save_name,
            'message' => 'Image upload Successfully'
        ], 200);
    }

    /**
    * Remove the images from the storage.
    *
    * @param Request $request
    */
   public function imageDestroy(Request $request)
   {
        $filename = $request->id;
        $admin = Auth::user();
        if(!empty($admin->admin_id)){
            $adminId = $admin->admin_id;
            $upload_path = $this->prod_image."/temp/".$adminId."/_".$filename;
            if (file_exists($upload_path)) {
                unlink($upload_path);
            }
            return Response::json(['message' => 'File successfully delete'], 200);
        } else {
            return Response::json([
                'message' => 'Image upload Failed, Missing user details'
            ], 400);
        }
   }

   /**
    * Remove the images from the storage.
    *
    * @param Request $request
    */
    public function imageMainDestroy(Request $request)
    {
         $filename = $request->id;
         $admin = Auth::user();
         if(!empty($admin->admin_id)){
             $adminId = $admin->admin_id;
             $upload_path = $this->prod_image."/temp/".$adminId."/main/_".$filename;
             if (file_exists($upload_path)) {
                 unlink($upload_path);
             }
             return Response::json(['message' => 'File successfully delete'], 200);
         } else {
             return Response::json([
                 'message' => 'Image upload Failed, Missing user details'
             ], 400);
         }
    }

    public function export_product(Request $request, $type){
        $objProductService = new ProductServices();
        $file_type = $request->segment(4);
        $arrProductList = $objProductService ->getProductListByLabel($type)->toArray();
        $file_name = "Export Product";
        $this->downloadExcel($arrProductList,$file_name,$file_type);
    }

    private function processProductDetail($key,$val, $url = ""){
        $key = json_decode($key,true);
        $val = json_decode($val,true);
        $arrDetail = array();
        foreach($key as $k => $v){
            if($v !== "-"){
                $arrDetail[$v] = $val[$k];
            }
        }
        if(!empty($url)){
            $arrDetail['url'] = $url;
        }
        return json_encode($arrDetail);
    }

    private function clearTempProductImage($adminId, $main = false) {
        if($main){
            $path = public_path('images/product/temp/'.$adminId."/main/");
        } else {
            $path = public_path('images/product/temp/'.$adminId."/");
        }
        $this->_rrmdir($path);
    }

    private function copyToTempProductImage(){
        $srcfile='C:\File\Whatever\Path\Joe.txt';
        $dstfile='G:\Shared\Reports\Joe.txt';

        if($main){
            $despath = public_path('images/product/temp/'.$adminId."/main/");
        } else {
            $despath = public_path('images/product/temp/'.$adminId."/");
        }
        copy($srcfile, $despath);
    }

    private function _rrmdir($dir) {
        if (is_dir($dir)) {
          $objects = scandir($dir); 
          foreach ($objects as $object) { 
            if ($object != "." && $object != "..") {
              if (is_dir($dir."/".$object))
                $this->_rrmdir($dir."/".$object);
              else
                unlink($dir."/".$object);
            } 
          }
          rmdir($dir); 
        } 
    }
}