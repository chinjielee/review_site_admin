<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\SpamFilter;
use Carbon\Carbon;

class SpamController extends Controller
{
	public function spam_filter(){
        $pageInfo = (object)array();
        $pageInfo->page = "Spam Filter";
        $objSpamList = new SpamFilter();
        $arraySpamList = $objSpamList->get();
        return view('spam-filter.index',compact('pageInfo','arraySpamList'));
    }

    public function saveSpamFilter(Request $request) {
        $arrRequest = $request->all();
        $arrInsertArray = array();
        if(!empty($arrRequest['active'])){
            foreach($arrRequest['active'] as $active) {
                $arrActive = array();
                $arrActive['text'] = $active;
                $arrActive['rate'] = 0;
                $arrActive['type'] = 0;
                $arrActive['status'] = 1;
                $arrActive['created_at'] = Carbon::now();
                $arrActive['updated_at'] = Carbon::now();
                array_push($arrInsertArray, $arrActive);
            }
        }
        if(!empty($arrRequest['disable'])){
            foreach($arrRequest['disable'] as $disable) {
                $arrDisable = array();
                $arrDisable['text'] = $disable;
                $arrDisable['rate'] = 0;
                $arrDisable['type'] = 0;
                $arrDisable['status'] = 0;
                $arrDisable['created_at'] = Carbon::now();
                $arrDisable['updated_at'] = Carbon::now();
                array_push($arrInsertArray, $arrDisable);
            }    
        }
        
        $objSpamFilter = new SpamFilter();
        $objSpamFilter->reset();
        $result = $objSpamFilter->bulkInsert($arrInsertArray);
        if($result){
            return "done";
        } else {
            return "fail";
        }
        
    }
}