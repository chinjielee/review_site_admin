<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\ProductReview;
use App\ProductQuestion;
use App\ProductComment;
use App\Traits\ExcelTrait;

class ActionController extends Controller
{
    use ExcelTrait;    
	public function reviews(Request $request){
        $pageInfo = (object)array();
        $data_list = (object)array();
        $pageInfo->page = "All Reviews";
        $objProductReview = new ProductReview();
        $start = $end = "";
        if(($request->input('start') !== NULL) && ($request->input('end') !== NULL)){
            $start = $request->input('start')." 00:00:00";
            $end = $request->input('end')." 23:59:59";
        }
        switch($request->segment(3)){
            case "Published" : $status = '1'; break;
            case "Cancelled" : $status = '2'; break;
            case "Blocked" : $status = '3'; break;
            default : $status = ''; break; 
        }
        $data_list->totalCount = $objProductReview->getTotalCount();
        $data_list->listing = $objProductReview->getFullList($status,$start,$end);
        foreach($data_list->listing as $list){
            if(!empty($list->review_image)){
                $list->review_image = base64_encode($list->product_reviews_id)."/review/".$list->review_image;
            }
            
        }
        $data_list->categoryCount = $objProductReview->getCountByStatus();
        return view('reviews.index',compact('pageInfo','data_list'));
    }

    public function questions(Request $request){
        $pageInfo = (object)array();
        $data_list = (object)array();
        $pageInfo->page = "All Questions";
        $objProductQuestion = new ProductQuestion();
        $start = $end = "";
        if(($request->input('start') !== NULL) && ($request->input('end') !== NULL)){
            $start = $request->input('start')." 00:00:00";
            $end = $request->input('end')." 23:59:59";
        }
        switch($request->segment(3)){
            case "Published" : $status = '1'; break;
            case "Cancelled" : $status = '2'; break;
            case "Blocked" : $status = '3'; break;
            default : $status = ''; break; 
        }
        $data_list->totalCount = $objProductQuestion->getTotalCount();
        $data_list->listing = $objProductQuestion->getFullList($status,$start,$end);
        $data_list->categoryCount = $objProductQuestion->getCountByStatus();
        return view('questions.index',compact('pageInfo','data_list'));
    }

    public function comments(Request $request){
        $pageInfo = (object)array();
        $data_list = (object)array();
        $pageInfo->page = "All Comments";
        $objProductComment = new ProductComment();
        $start = $end = "";
        if(($request->input('start') !== NULL) && ($request->input('end') !== NULL)){
            $start = $request->input('start')." 00:00:00";
            $end = $request->input('end')." 23:59:59";
        }
        switch($request->segment(3)){
            case "Published" : $status = '1'; break;
            case "Cancelled" : $status = '2'; break;
            case "Blocked" : $status = '3'; break;
            default : $status = ''; break; 
        }
        $data_list->totalCount = $objProductComment->getTotalCount();
        $data_list->listing = $objProductComment->getFullList($status,$start,$end);
        $data_list->categoryCount = $objProductComment->getCountByStatus();
        return view('comments.index',compact('pageInfo','data_list'));
    }

    public function update_review(Request $request){
        $review = ProductReview::where('product_reviews_id', $request['review'])->first();
        if($review === NULL){
            session()->flash('warning', 'Review was not found.');
            return redirect()->back();
        }
        $review->status = '1';
        $review->save();
        session()->flash('success', 'Review was published.');
        return redirect()->back();
    }

    public function update_question(Request $request){
        $question = ProductQuestion::where('product_questions_id', $request['question'])->first();
        if($question === NULL){
            session()->flash('warning', 'Question was not found.');
            return redirect()->back();
        }
        $question->status = '1';
        $question->save();
        session()->flash('success', 'Question was published.');
        return redirect()->back();
    }

    public function update_comment(Request $request){
        $comment = ProductComment::where('product_answers_id', $request['comment'])->first();
        if($comment === NULL){
            session()->flash('warning', 'Comment was not found.');
            return redirect()->back();
        }
        $comment->status = '1';
        $comment->save();
        session()->flash('success', 'Comment was published.');
        return redirect()->back();
    }

    public function delete_review(Request $request){
        $review = ProductReview::where('product_reviews_id', $request['review'])->first();
        if($review === NULL){
            session()->flash('warning', 'Review was not found.');
            return redirect()->back();
        }
        $review->status = '3';
        $review->save();
        session()->flash('success', 'Review was blocked.');
        return redirect()->back();
    }

    public function delete_question(Request $request){
        $question = ProductQuestion::where('product_questions_id', $request['question'])->first();
        if($question === NULL){
            session()->flash('warning', 'Question was not found.');
            return redirect()->back();
        }
        $question->status = '3';
        $question->save();
        session()->flash('success', 'Question was blocked.');
        return redirect()->back();
    }

    public function delete_comment(Request $request){
        $comment = ProductComment::where('product_answers_id', $request['comment'])->first();
        if($comment === NULL){
            session()->flash('warning', 'Comment was not found.');
            return redirect()->back();
        }
        $comment->status = '3';
        $comment->save();
        session()->flash('success', 'Comment was blocked.');
        return redirect()->back();
    }

    public function export_review(Request $request){
        $file_type = $request->segment(4);
        $data = ProductReview::get()->toArray();
        $file_name = "Export Review";
        $this->downloadExcel($data,$file_name,$file_type);
    }

    public function export_question(Request $request){
        $file_type = $request->segment(4);
        $data = ProductQuestion::get()->toArray();
        $file_name = "Export Question";
        $this->downloadExcel($data,$file_name,$file_type);
    }

    public function export_comment(Request $request){
        $file_type = $request->segment(4);
        $data = ProductReview::get()->toArray();
        $file_name = "Export Comment";
        $this->downloadExcel($data,$file_name,$file_type);
    }
}