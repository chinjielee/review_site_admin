<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Categories;
use App\RatingOption;
use Auth;
use File;

class CategoriesController extends Controller
{
    const CAT_IMAGE_PATH = 'images/categories';

    public function __construct()
    {
        $this->cat_image = public_path(self::CAT_IMAGE_PATH);
    }

    protected function cateInsertValidator(array $data)
    {
        $messages = [
            'name.required' => 'Please give this category a name.',
            'rating_option.required' => 'Please specify total of 5 rating categories',
        ];
        $validator = Validator::make($data, [
             'name'=> 'required|string|max:255',
             'rating_option.*' => 'required',
        ],$messages);
        return $validator;
    }

    public function saveCategories(Request $request){
        $validator = $this->cateInsertValidator($request->all());
        $validator->validate();
        $admin = Auth::user();
        $objCategories = new Categories();
        $objRatingOption = new RatingOption();
        $arrInput = $request->input();
    
        $arrMappedInput = $objCategories->columnMapping($arrInput);
        $inserted_id = $objCategories->updCreateCategory($arrMappedInput);

        $blnInserted = $objCategories->categoriesImageSave($admin->admin_id, $inserted_id); 
        if(!empty($arrInput['rating_option'])){
            $returnInput['rating_option'] = implode(',',$arrInput['rating_option']);
            $arrInput['category_id'] = $inserted_id;
            $arrRatingOptionInput = $objRatingOption->_processInputToBeInsert($arrInput);
            if($arrRatingOptionInput){
                $category_id = $objRatingOption->insertOrUpdate($arrRatingOptionInput);    
            }
        }
        if($blnInserted) {
            return redirect()->route('Categories')->with('success', 'Category created successfully');   ;
        } else {
            return redirect()->back()->withInput()->withErrors(['Somethings went wrong.']);
        }
    }

	public function categories(Request $request){
        $pageInfo = (object)array();
        $pageInfo->page = "Categories";
        // Get the Categories List & count
        $objCategories = new Categories();
        $start = $end = "";
        if(($request->input('start') !== NULL) && ($request->input('end') !== NULL)){
            $start = $request->input('start')." 00:00:00";
            $end = $request->input('end')." 23:59:59";
        }
        $arrCategory = $objCategories->getCategoryListWithCount($start, $end);
        foreach($arrCategory as $cat){
            $categoryDisplay = $objCategories->getCategoriesLabel($cat->categories_id);
            $cat->imageUrl = "";
            if(!empty($cat->image)){
                $displayCategoriesId = base64_encode($cat->categories_id);
                $imageUrl = url(self::CAT_IMAGE_PATH."/".$displayCategoriesId."/".$cat->image);
                $cat->imageUrl = $imageUrl;
            }
            
            if(empty($categoryDisplay->label_2)){
                $cat->label = $categoryDisplay->label_1;
            } else {
                $cat->label = $categoryDisplay->label_2;
            }
        }
        return view('categories.index',compact('pageInfo','arrCategory'));
    }

    public function categoriesByLabel($type) {
        $pageInfo = (object)array();
        $pageInfo->page = "Categories";
        $objCategories = new Categories();
        $arrCategory = $objCategories->getCategoryGroupByLabel(strtolower($type));
        foreach($arrCategory as $cat) {
            $categoryDisplay = $objCategories->getCategoriesLabel($cat->categories_id);
            $cat->imageUrl = "";
            if(!empty($cat->image)){
                $displayCategoriesId = base64_encode($cat->categories_id);
                $imageUrl = url(self::CAT_IMAGE_PATH."/".$displayCategoriesId."/".$cat->image);
                $cat->imageUrl = $imageUrl;
            }
            
            if(!isset($categoryDisplay->label_2) || empty($categoryDisplay->label_2)){
                $cat->label = !empty($categoryDisplay->label_1) ? $categoryDisplay->label_1 : "" ;
            } else {
                $cat->label = $categoryDisplay->label_2;
            }
        }
        return view('categories.index',compact('pageInfo','arrCategory'));
    }

    public function mngCategories(Request $request, $category_slug = ""){
        $pageInfo = (object)array();
        $pageInfo->page = "Manage Categories";
        $objCategories = new Categories();
        $arrCategoryDetail = array();
        $imageUrl = "";
        if(!empty($category_slug)) {
            $arrCategoryDetail = $objCategories->getMultiLayerCategoryBySlug($category_slug);
            if(!empty($arrCategoryDetail)){
                $displayCategoriesId = base64_encode($arrCategoryDetail->categories_id);
                $directory = public_path(self::CAT_IMAGE_PATH."/".$displayCategoriesId."/".$arrCategoryDetail->image);
                if (File::exists($directory)) {
                    $imageUrl = url(self::CAT_IMAGE_PATH."/".$displayCategoriesId."/".$arrCategoryDetail->image);
                }
                if(!empty($arrCategoryDetail->parent_cat_1)){
                    if(!empty($arrCategoryDetail->parent_cat_2)){
                        $arrCategoryDetail->label_id = $arrCategoryDetail->parent_cat_2_id;
                    } else {
                        $arrCategoryDetail->label_id = $arrCategoryDetail->parent_cat_1_id;
                    }
                } else {
                    $arrCategoryDetail->label_id = $arrCategoryDetail->parent_category_id;
                }
            }
        }
        $arrCategoryList = array();
        $arrCategoryList = $objCategories->getCategoryTopExplorelist();
        $arrLabel = $objCategories->getLabelList();
        $strProductList = json_encode($arrCategoryList['product']);
        $strServiceList = json_encode($arrCategoryList['service']);
        $strMovieList = json_encode($arrCategoryList['movie']);
        $strCategoryDetail = json_encode($arrCategoryDetail);
        $objRatingOption = new RatingOption();
        if(!empty($arrCategoryDetail)){
            $objCategoryRateOption = $objRatingOption->getRatingOption($arrCategoryDetail->categories_id);
        } else {
            $objCategoryRateOption = $objRatingOption->getRatingOption();
        }
        $rating_option_1 = $rating_option_2 = $rating_option_3 = $rating_option_4 = $rating_option_5 = '';
        if($objCategoryRateOption){
            $arrRatingOption = $objRatingOption->processDisplayRatingOption($objCategoryRateOption->option_json);
            $optionCount = 1;
            foreach($arrRatingOption as $option) {
                $name = 'rating_option_'.$optionCount;
                $$name = $option;
                $optionCount++;
            }
        }
        return view('categories.edit',compact('pageInfo','arrCategoryDetail','arrCategoryList','strProductList','strServiceList','strMovieList','imageUrl','strCategoryDetail','arrLabel','rating_option_1','rating_option_2','rating_option_3','rating_option_4','rating_option_5'));
    }

    /**
    * Saving images uploaded through XHR Request.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function catImageStore(Request $request) {
        // AdminUser & productId
        $admin = Auth::user();
        if(!empty($admin->admin_id)){
            $adminId = $admin->admin_id;
        } else {
            return Response::json([
                'message' => 'Image upload Failed, Missing user details'
            ], 400);
        }
        $this->clearTempCategoriesImage($adminId);
        $upload_path = $this->cat_image."/temp/".$adminId."";

        $photos = $request->file('file');
        
        if (!is_array($photos)) {
            $photos = [$photos];
        }
        if (!is_dir($upload_path)) {
            mkdir($upload_path, 0777, true);
        }
        
        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];
            //$name = sha1(date('YmdHis') . str_random(30));
            $name = $request->type."_".pathinfo($photo->getClientOriginalName())['filename'];
            $name = str_replace([' ',',','.'],'',$name);
            $save_name = $name . '.' . $photo->getClientOriginalExtension();
            $photo->move($upload_path, $save_name);
        }
        
        $uploaded_url = url('/temp/'.$adminId."/".$save_name);

        return Response::json([
            'url' => $uploaded_url,
            'save_file' => $save_name,
            'message' => 'Image upload Successfully'
        ], 200);
    }

    public function catImageSave(){

    }

    /**
    * Remove the images from the storage.
    *
    * @param Request $request
    */
   public function imageDestroy(Request $request)
   {
        $filename = $request->id;
        $admin = Auth::user();
        if(!empty($admin->admin_id)){
            $adminId = $admin->admin_id;
            $upload_path = $this->cat_image."/temp/".$adminId."/_".$filename;
            if (file_exists($upload_path)) {
                unlink($upload_path);
            }
            return Response::json(['message' => 'File successfully delete'], 200);
        } else {
            return Response::json([
                'message' => 'Image upload Failed, Missing user details'
            ], 400);
        }
   }

   public function delCategories(Request $request){
       
        $arrRequest = $request->all();
        $cid = json_decode($arrRequest['data']);
        $objCategories = new Categories();
        $blnDel = $objCategories->del($cid);
        if($blnDel){
            return 'done';
        } else {
            return 'fail';
        }
    }

    public function clearTempCategoriesImage($adminId){
        $path = public_path('images/categories/temp/'.$adminId."/");
        $this->_rrmdir($path);
    }

    private function _rrmdir($dir) {
        if (is_dir($dir)) {
          $objects = scandir($dir); 
          foreach ($objects as $object) { 
            if ($object != "." && $object != "..") {
              if (is_dir($dir."/".$object))
                $this->_rrmdir($dir."/".$object);
              else
                unlink($dir."/".$object);
            } 
          }
          rmdir($dir); 
        } 
    }
}