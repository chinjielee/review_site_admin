<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\ProductReview;
use App\ProductQuestion;
use App\ProductComment;
use App\Admin;
use App\Http\Model\User;
use DB;
use Auth;
use Hash;

class AdminController extends Controller
{
    const COUNTRY = array('Malaysia','Singapore');
    const ADMIN_STATUS = array('Active','Deactivated');
    const ADMIN_ROLE = array('Admin','Super Admin');
	public function dashboard(){
        $pageInfo = (object)array();
        $pageInfo->page = "Dashboard";
        $objProductReview = new ProductReview();
        $objProductQuestion = new ProductQuestion();
        $objProductComment = new ProductComment();
        $objUser = new User();
        $review_count = $objProductReview->getTotalCount();
        $comment_count = $objProductComment->getTotalCount();
        $question_count = $objProductQuestion->getTotalCount();
        $user_count = $objUser->getCountAllUser();
        return view('dashboard',compact('pageInfo','comment_count','question_count','review_count','user_count'));
    }

    public function signupdata(Request $request){
        $objUser = new User();
        $start = $request->input('start');
        $end = $request->input('end');
        $user_count = $objUser->getCountByDate($start,$end);
        return $user_count;
    }

    public function actiondata(Request $request){
        $objProductReview = new ProductReview();
        $objProductQuestion = new ProductQuestion();
        $objProductComment = new ProductComment();
        $start = $request->input('start');
        $end = $request->input('end');
        $product_review_count = $objProductReview->getCountByDate($start,$end);
        $product_question_count = $objProductQuestion->getCountByDate($start,$end);
        $product_comment_count = $objProductComment->getCountByDate($start,$end);
        $result = array();
        $result['full_list'] = array();
        $date_list = array();
        //Get Review List and Combine With Question and Comment
        foreach ($product_review_count as $review){
            array_push($date_list, $review->period);
            $test = (object)array();
            $test->period = $review->period;
            $test->reviews = $review->reviews;
            $test->questions = 0;
            $test->comments = 0;
            foreach($product_question_count as $question){
                if($test->period == $question->period){
                    $test->questions = $question->questions;
                    break;
                }
                if($test->period < $question->period){
                    break;
                }
            }
            foreach($product_comment_count as $comment){
                if($test->period == $comment->period){
                    $test->comments = $comment->comments;
                    break;
                }
                if($test->period < $comment->period){
                    break;
                }
            }
            array_push($result['full_list'],$test);
        }

        //Get Remaining Question List Match With Comment
        foreach ($product_question_count as $question){
            if(!in_array($question->period, $date_list)){
                array_push($date_list, $question->period);
                $test = (object)array();
                $test->period = $question->period;
                $test->reviews = 0;
                $test->questions = $question->questions;
                $test->comments = 0;
                foreach($product_comment_count as $comment){
                    if($test->period == $comment->period){
                        $test->comments = $comment->comments;
                        break;
                    }
                    if($test->period < $comment->period){
                        break;
                    }
                }
                array_push($result['full_list'],$test);
            }
        }

        //Get Remaining Comment List
        foreach ($product_comment_count as $comment){
            if(!in_array($comment->period, $date_list)){
                array_push($date_list, $comment->period);
                $test = (object)array();
                $test->period = $comment->period;
                $test->reviews = 0;
                $test->questions = 0;
                $test->comments = $comment->comments;
                array_push($result['full_list'],$test);
            }
        }
        $result["reviews"] = $result["questions"] = $result["comments"] = 0;
        foreach($result['full_list'] as $list){
            $result["reviews"] += $list->reviews;
            $result["questions"] += $list->questions;
            $result["comments"] += $list->comments;
        }
        return $result;
    }

    public function profile(){
        $pageInfo = (object)array();
        $pageInfo->page = "Profile";
        return view('profile.index',compact('pageInfo'));
    }

    public function change_password(){
        $pageInfo = (object)array();
        $pageInfo->page = "Change Password";
        return view('profile.changepass',compact('pageInfo'));
    }

    public function update_avatar(Request $request){
        $this->validate($request,[
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $admin = Auth::user();
        $avatarName = $admin->admin_id.'_avatar'.time().'.'.$request->avatar->getClientOriginalExtension();
        $request->avatar->move(public_path('images/admin'), $avatarName);
        $admin->avatar = $avatarName;
        $admin->save();
        return back()->with('success','You have successfully upload image.');
    }

    public function update_profile(Request $request){
        $rules = [
            'name'     => 'required|string|max:255',
            'mobile'        => 'required|string|min:9'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            view('profile.index',compact('pageInfo'));
            return redirect()->route('Profile')->withErrors($validator)->withInput();
        }else{
            $admin = Auth::user();
            $admin->name = $request->input('name');
            $admin->mobile = $request->input('mobile');
            $admin->save();
            session()->flash('success', 'Your profile was updated.');
            return redirect()->route('Profile');
        }
    }

    public function update_password(Request $request){
        if(!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            return redirect()->back()->withErrors("Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current_password'), $request->get('password')) == 0){
            return redirect()->back()->withErrors("New Password cannot be same as your current password. Please choose a different password.");
        }

        $rules = [
            'current_password'  => 'required',
            'password'          => 'required|string|min:4|confirmed',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            view('profile.index',compact('pageInfo'));
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $admin = Auth::user();
            $admin->password = bcrypt($request->input('password'));
            $admin->save();
            session()->flash('success', 'Your password was updated.');
            return redirect()->back();
        }
    }

    public function admin(Request $request){
        $status = $request->segment(2);
        switch($status){
            case "Active" : $status = '1'; break;
            case "Deactivated" : $status = '0'; break;
            default : $status = ''; break; 
        }
        $pageInfo = (object)array();
        $pageInfo->page = "Admin";
        $admin_list = (object)array();
        if($status != ''){
            $admin_list->listing = DB::table('admins')->where('admin_status', $status)->get();
        }else{
            $admin_list->listing = DB::table('admins')->groupBy('admin_id')->get();

        }
        $admin_list->totalCount = DB::table('admins')->count();
        $admin_list->categoryCount = DB::table('admins')->select('admin_status', DB::raw('count(`admin_id`) as total'))->groupBy('admin_status')->get();
        return view('admin.index',compact('pageInfo','admin_list'));
    }

    public function add_admin(){
        $pageInfo = (object)array();
        $pageInfo->page = "Add Admin";
        $pageView = "Add";
        $statusList = self::ADMIN_STATUS;
        $roleList = self::ADMIN_ROLE;
        $countryList = self::COUNTRY;
        return view('admin.edit',compact('pageInfo','pageView','countryList','statusList','roleList'));
    }

    public function update_admin(Request $request){
        $pageInfo = (object)array();
        $adminInfo = (object)array();
        $adminInfo->email = $request->segment(4); 
        $statusList = self::ADMIN_STATUS;
        $roleList = self::ADMIN_ROLE;
        $countryList = self::COUNTRY;
        if($adminInfo->email !== NULL){
            $pageInfo->page = "Update Admin";
            $pageView = "Edit";
            $adminInfo->details = DB::table('admins')->where('email', $adminInfo->email)->first();
        }else{
            $pageInfo->page = "Add Admin";
            $pageView = "Add";
        }

        switch ($adminInfo->details->admin_status){
            case "0" : $adminInfo->details->admin_status = "Deactivated"; break;
            case "1" : $adminInfo->details->admin_status = "Active"; break;
            default : break;
        }
        switch ($adminInfo->details->roles){
            case "0" : $adminInfo->details->roles = "Admin"; break;
            case "1" : $adminInfo->details->roles = "Super Admin"; break;
            default : break;
        }
        return view('admin.edit',compact('pageInfo','pageView','countryList','statusList','adminInfo','roleList'));
    }

    public function block_admin(Request $request){
        $admin = Admin::where('email', $request['admin'])->first();
        if($admin === NULL){
            session()->flash('warning', 'Admin '.$request['admin'].' was not found.');
            return redirect()->route('Admin');
        }
        $admin->admin_status = '0';
        $admin->save();
        session()->flash('success', 'Admin '.$request['admin'].' is deactive.');
        return redirect()->route('Admin');
    }

    public function save_admin(Request $request){
        if($request['type']=="add"){
            $rules = [
                'name'     => 'required|string|max:255',
                'email'         => 'required|unique:admins',
                'mobile'        => 'required|string|min:9',
            ];
        }else{
            $rules = [
                'name'     => 'required|string|max:255',
                'mobile'        => 'required|string|min:9',
            ];
        }
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            if($request['type']=="add"){
                switch ($request['status']){
                    case "Deactivated" : $status = "0"; break;
                    case "Active" : $status = "1"; break;
                    default : $status = "1"; break;
                }
                switch ($request['roles']){
                    case "Admin" : $role = "0"; break;
                    case "Super Admin" : $role = "1"; break;
                    default : $role = "0"; break;
                }
                $newAdmin = Admin::create([
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'password' => bcrypt($request['password']),
                    'mobile' => $request['mobile'],
                    'admin_status' => $status,
                    'roles' => $role,
                ]);
                $newAdmin->save();
                session()->flash('success', 'New admin '.$request['email'].' was created.');
            }else{
                switch ($request['status']){
                    case "Deactivated" : $status = "0"; break;
                    case "Active" : $status = "1"; break;
                    default : $status = "1"; break;
                }

                switch ($request['roles']){
                    case "Admin" : $role = "0"; break;
                    case "Super Admin" : $role = "1"; break;
                    default : $role = "0"; break;
                }
                $admin = Admin::where('email', $request['admin'])->first();
                if($admin === NULL){
                    session()->flash('warning', 'Admin '.$request['admin'].' was not found.');
                    return redirect()->back();
                }
                $admin->name = $request['name'];
                $admin->mobile = $request['mobile'];
                $admin->admin_status = $status;
                $admin->roles = $role;
                $admin->save();
                session()->flash('success', 'Admin '.$request['email'].' is updated.');
            }
            return redirect()->route('Admin');
        }
    }
}