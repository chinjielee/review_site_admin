<?php
namespace App\Http\Controllers;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Model\User;
use Carbon\Carbon;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\ExcelTrait;

class UserController extends Controller
{
    use ExcelTrait;    
    const COUNTRY = array('Malaysia','Singapore');
    const USER_STATUS = array('Pending','Active','Banned');
	public function user(Request $request){
        $status = $request->segment(2);
        switch($status){
            case "Pending" : $status = '0'; break;
            case "Active" : $status = '1'; break;
            case "Banned" : $status = '2'; break;
            default : $status = ''; break; 
        }
        $pageInfo = (object)array();
        $pageInfo->page = "User";
        // $objUser = new User();
        $user_list = (object)array();
        if($status != ''){
            $user_list->listing = DB::table('users')
            ->leftJoin('product_reviews', 'users.user_id', '=', 'product_reviews.user_id')
            ->leftJoin('product_answers', 'users.user_id', '=', 'product_answers.user_id')
            ->leftJoin('product_questions', 'users.user_id', '=', 'product_questions.user_id')
            ->select('users.*', DB::raw("count(distinct product_reviews.product_reviews_id) as review_count"), DB::raw("count(distinct product_answers.product_answers_id) as comment_count"),DB::raw("count(distinct product_questions.product_questions_id) as question_count"))->where('user_status', $status)->groupBy('user_id')->get();
        }else{
            $user_list->listing = DB::table('users')
            ->leftJoin('product_reviews', 'users.user_id', '=', 'product_reviews.user_id')
            ->leftJoin('product_answers', 'users.user_id', '=', 'product_answers.user_id')
            ->leftJoin('product_questions', 'users.user_id', '=', 'product_questions.user_id')
            ->select('users.*', DB::raw("count(distinct product_reviews.product_reviews_id) as review_count"), DB::raw("count(distinct product_answers.product_answers_id) as comment_count"),DB::raw("count(distinct product_questions.product_questions_id) as question_count"))->groupBy('user_id')->get();

        }
        $user_list->totalCount = DB::table('users')->count();
        $user_list->categoryCount = DB::table('users')->select('user_status', DB::raw('count(`user_id`) as total'))->groupBy('user_status')->get();
        return view('user.index',compact('pageInfo','user_list'));
    }

    public function add_user(){
        $pageInfo = (object)array();
        $pageInfo->page = "Add User";
        $pageView = "Add";
        $statusList = self::USER_STATUS;
        $countryList = self::COUNTRY;
        return view('user.edit',compact('pageInfo','pageView','countryList','statusList'));
    }

    public function update_user(Request $request){
        $pageInfo = (object)array();
        $userInfo = (object)array();
        $userInfo->email = $request->segment(4); 
        $statusList = self::USER_STATUS;
        $countryList = self::COUNTRY;
        if($userInfo->email !== NULL){
            $pageInfo->page = "Update User";
            $pageView = "Edit";
            $userInfo->details = DB::table('users')
            ->leftJoin('product_reviews', 'users.user_id', '=', 'product_reviews.user_id')
            ->leftJoin('product_answers', 'users.user_id', '=', 'product_answers.user_id')
            ->leftJoin('product_questions', 'users.user_id', '=', 'product_questions.user_id')
            ->select('users.*', DB::raw("count(distinct product_reviews.product_reviews_id) as review_count"), DB::raw("count(distinct product_answers.product_answers_id) as comment_count"),DB::raw("count(distinct product_questions.product_questions_id) as question_count"))->where('email', $userInfo->email)->groupBy('user_id')->first();
        }else{
            $pageInfo->page = "Add User";
            $pageView = "Add";
        }
        return view('user.edit',compact('pageInfo','pageView','countryList','statusList','userInfo'));
    }

    public function block_user(Request $request){
        $user = User::where('email', $request['user'])->first();
        if($user === NULL){
            session()->flash('warning', 'User '.$request['user'].' was not found.');
            return redirect()->route('User');
        }
        $user->user_status = '2';
        $user->save();
        session()->flash('success', 'User '.$request['user'].' was banned.');
        return redirect()->route('User');
    }

    public function save_user(Request $request){
        if($request['type']=="add"){
            $rules = [
                'firstname'     => 'required|string|max:255',
                'lastname'      => 'required|string|max:255',
                'email'         => 'required|unique:users',
                'gender'        => 'required|in:Male,Female',
                'mobile_code'   => 'required',
                'mobile'        => 'required|string|min:7',
                'language'      => 'required|in:English,Chinese',
                'country'       => 'required|in:'.implode(',', self::COUNTRY)
            ];
        }else{
            $rules = [
                'firstname'     => 'required|string|max:255',
                'lastname'      => 'required|string|max:255',
                'gender'        => 'required|in:Male,Female',
                'mobile_code'   => 'required',
                'mobile'        => 'required|string|min:7',
                'language'      => 'required|in:English,Chinese',
                'country'       => 'required|in:'.implode(',', self::COUNTRY)
            ];
        }
        $messages = [
            'dob.date'      => 'Date of Birth is not a valid date (YYYY-MM-DD).',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            // view('profile.my-profile',compact('pageInfo'));
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            if($request['type']=="add"){
                $admin_id = Auth::user()->admin_id;
                $dob = "";
                if(!empty($request['dob'])){
                    $dob = Carbon::parse($request['dob'])->format('Y-m-d');
                }
                switch ($request['language']){
                    case "English" : $language = "en"; break;
                    case "Chinese" : $language = "cn"; break;
                    default : $language = "en"; break;
                }
                switch ($request['status']){
                    case "Pending" : $status = "0"; break;
                    case "Active" : $status = "1"; break;
                    case "Banned" : $status = "2"; break;
                    default : $status = "1"; break;
                }
                $newUser = User::create([
                    'first_name' => $request['firstname'],
                    'last_name' => $request['lastname'],
                    'email' => $request['email'],
                    'password' => bcrypt($request['password']),
                    'mobile_code' => $request['mobile_code'],
                    'mobile' => $request['mobile'],
                    'gender' => $request['gender'],
                    'country' => $request['country'],
                    'dob' => $dob,
                    'language' => $language,
                    'user_status' => $status,
                    'created_by' => $admin_id,
                ]);
                $newUser->email_token = base64_encode($newUser->user_id."-".$request['email']);
                $newUser->save();
                session()->flash('success', 'New user '.$request['email'].' was created.');
            }else{
                $dob = "";
                if(!empty($request['dob'])){
                    $dob = Carbon::parse($request['dob'])->format('Y-m-d');
                }
                switch ($request['language']){
                    case "English" : $language = "en"; break;
                    case "Chinese" : $language = "cn"; break;
                    default : $language = "en"; break;
                }
                switch ($request['status']){
                    case "Pending" : $status = "0"; break;
                    case "Active" : $status = "1"; break;
                    case "Banned" : $status = "2"; break;
                    default : $status = "1"; break;
                }
                $user = User::where('email', $request['user'])->first();
                if($user === NULL){
                    session()->flash('warning', 'User '.$request['user'].' was not found.');
                    return redirect()->back();
                }
                $user->first_name = $request['firstname'];
                $user->last_name = $request['lastname'];
                $user->mobile_code = $request['mobile_code'];
                $user->mobile = $request['mobile'];
                $user->gender = $request['gender'];
                $user->country = $request['country'];
                $user->dob = $dob;
                $user->language = $language;
                $user->user_status = $status;
                $user->save();
                session()->flash('success', 'User '.$request['email'].' is updated.');
            }
            return redirect()->route('User');
        }
    }

    public function export_user(Request $request){
        $file_type = $request->segment(4);
        $data = User::get()->toArray();
        $file_name = "Export User";
        $this->downloadExcel($data,$file_name,$file_type);
    }
}