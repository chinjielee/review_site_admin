<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;
use App\FeatureImage;

class FeatureImageController extends Controller
{
    public function feature_image(){
        $pageInfo = (object)array();
        $pageInfo->page = "Feature Image";
        $feature_image_details = json_decode(DB::table('feature_image')->first()->details);
        return view('feature-image.index',compact('pageInfo','feature_image_details'));
    }

    public function upload_feature_image(Request $request){
        $this->validate($request,[
            'feature_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $avatarName = date('Y-m-d h:i:s').'.'.$request->feature_image->getClientOriginalExtension();
        $request->feature_image->move(public_path('images/feature_image'), $avatarName);
        return public_path('images/feature_image')."/".$avatarName;
    }

    public function save_feature_image(Request $request){
        $rules = [
            'UploadedAvatarFile1'    => 'required|string|max:200',
            'large_width1'           => 'required|numeric',
            'large_top1'             => 'required|numeric',
            'large_left1'            => 'required|numeric',
            'large_right1'           => 'required|numeric',
            'large_width1'           => 'required|numeric',
            'middle_width1'          => 'required|numeric',
            'middle_top1'            => 'required|numeric',
            'middle_left1'           => 'required|numeric',
            'middle_right1'          => 'required|numeric',
            'url_link1'              => 'required|string|max:200',
            'UploadedAvatarFile2'    => 'required|string|max:200',
            'large_width2'           => 'required|numeric',
            'large_top2'             => 'required|numeric',
            'large_left2'            => 'required|numeric',
            'large_right2'           => 'required|numeric',
            'large_width2'           => 'required|numeric',
            'middle_width2'          => 'required|numeric',
            'middle_top2'            => 'required|numeric',
            'middle_left2'           => 'required|numeric',
            'middle_right2'          => 'required|numeric',
            'url_link2'              => 'required|string|max:200',
            'UploadedAvatarFile3'    => 'required|string|max:200',
            'large_width3'           => 'required|numeric',
            'large_top3'             => 'required|numeric',
            'large_left3'            => 'required|numeric',
            'large_right3'           => 'required|numeric',
            'large_width3'           => 'required|numeric',
            'middle_width3'          => 'required|numeric',
            'middle_top3'            => 'required|numeric',
            'middle_left3'           => 'required|numeric',
            'middle_right3'          => 'required|numeric',
            'url_link3'              => 'required|string|max:200',
            'UploadedAvatarFile4'    => 'required|string|max:200',
            'large_width4'           => 'required|numeric',
            'large_top4'             => 'required|numeric',
            'large_left4'            => 'required|numeric',
            'large_right4'           => 'required|numeric',
            'large_width4'           => 'required|numeric',
            'middle_width4'          => 'required|numeric',
            'middle_top4'            => 'required|numeric',
            'middle_left4'           => 'required|numeric',
            'middle_right4'          => 'required|numeric',
            'url_link4'              => 'required|string|max:200',
            'UploadedAvatarFile5'    => 'required|string|max:200',
            'large_width5'           => 'required|numeric',
            'large_top5'             => 'required|numeric',
            'large_left5'            => 'required|numeric',
            'large_right5'           => 'required|numeric',
            'large_width5'           => 'required|numeric',
            'middle_width5'          => 'required|numeric',
            'middle_top5'            => 'required|numeric',
            'middle_left5'           => 'required|numeric',
            'middle_right5'          => 'required|numeric',
            'url_link5'              => 'required|string|max:200',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            view('feature-image.index',compact('pageInfo'));
            return redirect()->route('Feature Image')->withErrors($validator)->withInput();
        }else{
            $feature_image = FeatureImage::where('feature_image_id', '1')->first();
            $feature_image_input = array(
                (object) array(
                    "image" => ltrim(strrchr($request->UploadedAvatarFile1,'/'),'/'),
                    "large_width" => $request->large_width1,
                    "large_top" => $request->large_top1,
                    "large_left" => $request->large_left1,
                    "large_right" => $request->large_right1,
                    "middle_width" => $request->middle_width1,
                    "middle_top" => $request->middle_top1,
                    "middle_left" => $request->middle_left1,
                    "middle_right" => $request->middle_right1,
                    "url_link" => $request->url_link1
                ),
                (object) array(
                    "image" => ltrim(strrchr($request->UploadedAvatarFile2,'/'),'/'),
                    "large_width" => $request->large_width2,
                    "large_top" => $request->large_top2,
                    "large_left" => $request->large_left2,
                    "large_right" => $request->large_right2,
                    "middle_width" => $request->middle_width2,
                    "middle_top" => $request->middle_top2,
                    "middle_left" => $request->middle_left2,
                    "middle_right" => $request->middle_right2,
                    "url_link" => $request->url_link2
                ),
                (object) array(
                    "image" => ltrim(strrchr($request->UploadedAvatarFile3,'/'),'/'),
                    "large_width" => $request->large_width3,
                    "large_top" => $request->large_top3,
                    "large_left" => $request->large_left3,
                    "large_right" => $request->large_right3,
                    "middle_width" => $request->middle_width3,
                    "middle_top" => $request->middle_top3,
                    "middle_left" => $request->middle_left3,
                    "middle_right" => $request->middle_right3,
                    "url_link" => $request->url_link3
                ),
                (object) array(
                    "image" => ltrim(strrchr($request->UploadedAvatarFile4,'/'),'/'),
                    "large_width" => $request->large_width4,
                    "large_top" => $request->large_top4,
                    "large_left" => $request->large_left4,
                    "large_right" => $request->large_right4,
                    "middle_width" => $request->middle_width4,
                    "middle_top" => $request->middle_top4,
                    "middle_left" => $request->middle_left4,
                    "middle_right" => $request->middle_right4,
                    "url_link" => $request->url_link4
                ),
                (object) array(
                    "image" => ltrim(strrchr($request->UploadedAvatarFile5,'/'),'/'),
                    "large_width" => $request->large_width5,
                    "large_top" => $request->large_top5,
                    "large_left" => $request->large_left5,
                    "large_right" => $request->large_right5,
                    "middle_width" => $request->middle_width5,
                    "middle_top" => $request->middle_top5,
                    "middle_left" => $request->middle_left5,
                    "middle_right" => $request->middle_right5,
                    "url_link" => $request->url_link5
                )
            );
            $feature_image->details = json_encode($feature_image_input);
            $feature_image->save();
            session()->flash('success', 'Feature image was updated.');
            return redirect()->route('Feature Image');
        }
    }
}