<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\NotificationReadAll;
use App\Admin;

class NotificationController extends Controller
{
    const HARD_CODE_USER_ID = 10;
    const NOTIFICATION_PER_PAGE = 50;
    //
    public function listNotification(Request $request){
        $pageInfo = (object)array();
        $pageInfo->page = "Notification";
        
        $objAdmin = new Admin();
        $user = $objAdmin->getAdminUser();
        if(!empty($user->unreadNotifications()->get())) {
            $user->unreadNotifications()->get()->each(function ($n) {
                $n->markAsRead();
            });
            event(new NotificationReadAll($user->id));
        }

        $arrNotification = $user->notifications()->paginate(self::NOTIFICATION_PER_PAGE);
        //$arrNotification = array();
        
        return view('notification.index',compact('pageInfo','arrNotification'));
    }

}
