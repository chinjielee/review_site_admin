<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Brands;
use App\ProductServices;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\ExcelTrait;

class BrandsController extends Controller
{
    use ExcelTrait;
    const BRAND_STATUS = array('Pending','Active','Deleted');
	public function brands(Request $request){
        $status = $request->segment(2);
        $pageInfo = (object)array();
        $pageInfo->page = "Brands";
        $brand_id = 1;
        $brand_list = (object)array();
        $objProduct = new ProductServices();
        $input_start = '';
        $input_end = '';
        if(($request->input('start') !== NULL) && ($request->input('end') !== NULL)){
            $input_start = $request->input('start');
            $input_end = $request->input('end');
            $start = $request->input('start')." 00:00:00";
            $end = $request->input('end')." 23:59:59";
            $brand_list->listing = $objProduct->countByBrandIdDate($start,$end);
            // $brand_list->listing = DB::table('brands')->where([['created_at', '>=', $start],['created_at', '<=', $end]])->get();
        }else{
            $brand_list->listing = $objProduct->countByBrandIdDate();
        }
        return view('brands.index',compact('pageInfo','brand_list','brand_id','input_start','input_end'));
    }

    public function upload_brand_images(Request $request){
        $this->validate($request,[
            'brand_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $avatarName = date('Y-m-d_h:i:s').'.'.$request->brand_image->getClientOriginalExtension();
        $request->brand_image->move(public_path('images/brand_image'), $avatarName);
        return $avatarName;
    }

    public function add_brands(){
        $pageInfo = (object)array();
        $pageInfo->page = "Add Brands";
        $pageView = "Add";
        $statusList = self::BRAND_STATUS;
        return view('brands.edit',compact('pageInfo','pageView','statusList'));
    }

    public function update_brands(Request $request){
        $pageInfo = (object)array();
        $brandInfo = (object)array();
        $brandInfo->id = $request->segment(4);
        $statusList = self::BRAND_STATUS;
        if($brandInfo->id !== NULL){
            $pageInfo->page = "Update Brands";
            $pageView = "Edit";
            $brandInfo->details = DB::table('brands')->select('brands.*')->where('brands_id', $brandInfo->id)->first();
        }else{
            $pageInfo->page = "Add Brands";
            $pageView = "Add";
        }
        $statusList = self::BRAND_STATUS;
        return view('brands.edit',compact('pageInfo','pageView','statusList','brandInfo'));
    }


    public function delete_brands(Request $request){
        $brand = Brands::where('brands_id', $request['brand'])->first();
        if($brand === NULL){
            session()->flash('warning', 'Brand was not found.');
            return redirect()->route('Brands');
        }
        $brand->brand_status = '2';
        $brand->save();
        session()->flash('success', 'Brand '.$brand->brand_title.' was deleted.');
        return redirect()->route('Brands');
    }

    public function save_brands(Request $request){
        $rules = [
            'brand_title'   => 'required|string|max:255',
            'status'        => 'required',
            'UploadedBrandFile'     => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            if($request['type']=="add"){
                switch ($request['status']){
                    case "Pending" : $status = "0"; break;
                    case "Active" : $status = "1"; break;
                    case "Banned" : $status = "2"; break;
                    default : $status = "1"; break;
                }
                $newBrand = Brands::create([
                    'brand_title' => $request['brand_title'],
                    'brand_status' => $status,
                    'images' => $request['UploadedBrandFile']
                ]);
                $newBrand->save();
                session()->flash('success', 'New brand '.$request['brand_title'].' was created.');
            }else{
                switch ($request['status']){
                    case "Pending" : $status = "0"; break;
                    case "Active" : $status = "1"; break;
                    case "Banned" : $status = "2"; break;
                    default : $status = "1"; break;
                }
                $brand = Brands::where('brands_id', $request['brand'])->first();
                if($brand === NULL){
                    session()->flash('warning', 'Brand was not found.');
                    return redirect()->back();
                }
                $brand->brand_title = $request['brand_title'];
                $brand->brand_status = $status;
                $brand->images = $request['UploadedBrandFile'];
                $brand->save();
                session()->flash('success', 'Brand '.$request['brand_title'].' is updated.');
            }
            return redirect()->route('Brands');
        }
    }

    public function export_brands(Request $request){
        $file_type = $request->segment(4);
        $data = Brands::get()->toArray();
        $file_name = "Export Brands";
        $this->downloadExcel($data,$file_name,$file_type);
    }
}