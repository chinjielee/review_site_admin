<?php

namespace App\Http\Middleware;

use Closure;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->roles != 1){
            session()->flash('warning', 'Only Super Admin Can View The Previous Page.');
            return redirect('/home');
        }
        return $next($request);
    }
}
