<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class DiscussLog extends Model
{
    //
    protected $table = 'discuss_log';
    protected $primaryKey = 'discuss_log_id';
    protected $timestamp = true;
    protected $fillable = [
        'product_service_id',
        'product_service_title',
        'categories_id',
        'day_month_year',
        'created_at',
        'updated_at'
    ];
    
}
