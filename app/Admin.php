<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Http\Model\User;

class Admin extends Authenticatable
{
    use Notifiable;
    const HARDCODE_USER_ID = 10;
    protected $guard = 'user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function getAdminUser(){
        $user = User::find(Admin::HARDCODE_USER_ID);
        return $user;
    }
    protected $fillable = [
        'name','email', 'password','mobile','admin_status','roles','avatar'
    ];
    protected $primaryKey = 'admin_id';
    protected $table = 'admins';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
