<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categories;

class PendingProductService extends Model
{
    const STATUS_PENDING = 1;
    const STATUS_APPROVE = 2;
    const STATUS_CANCEL = 3;
    const STATUS_REJECT = 4;
    //
    protected $table = 'pending_product_service';
    protected $primaryKey = 'pending_product_service_id';
    public $timestamps = true;
    protected $fillable = array(
        'product_name',
        'category_id	',
        'category_name',
        'product_attributes',
        'created_at',
        'updated_at'
    );

    public function create($request){
        return self::insertGetId($request);
    }

    public function getProductDetailsBySlugWithFullCategoryByTempId($id){
        return self::select('pending_product_service.*', 'c.*', 'c1.categories_id as parent_cat_1_id','c2.categories_id as parent_cat_2_id','c1.category_slug as parent_cat_1', 'c2.category_slug as parent_cat_2')
            ->leftjoin('categories as c', 'pending_product_service.category_id', '=', 'c.categories_id')
            ->leftjoin('categories as c1','c.parent_category_id', '=', 'c1.categories_id')
            ->leftjoin('categories as c2', 'c1.parent_category_id', '=', 'c2.categories_id')
            ->where('pending_product_service_id', '=', $id)
            ->first();
    }

    public function getPendingProductDetails($start = '', $end = '',$type) {
        $objCategories = new Categories();
        switch($type){
            case'1':
                $type_name = 'product';
                break;
            case'2':
                $type_name = 'service';
                break;
            case'3':
                $type_name = 'movie';
                break;
        }
        $arrCategoriesList = $objCategories->getCategoryGroupByLabel($type_name)->toArray();
        $arrCategoryId = array_column($arrCategoriesList,'categories_id');
        
        if(($start != "") && ($end != "")) {
            return self::where([['status',self::STATUS_PENDING],['created_at', '>=', $start],['created_at', '<=', $end]])
                    ->whereIn('category_id',$arrCategoryId)
                    ->get();
        } else {
            return self::where('status',self::STATUS_PENDING)->whereIn('category_id',$arrCategoryId)->get();
        }
    }

    public function getPendingProductDetailsById($pending_id){
        return self::where('pending_product_service_id',$pending_id)->first();
    }

    public function del($pid) {
        return self::whereIn('pending_product_service_id', $pid)
                    ->update(['status'],self::STATUS_INACTIVE);
    }

    public function updatePendingProductToPublished($ppsid) {
        $pendingProduct = self::find($ppsid);
        $pendingProduct->status = self::STATUS_APPROVE;
        return $pendingProduct->save();
    }

    public function updatePendingProductToDenied($ppsid) {
        $pendingProduct = self::find($ppsid);
        $pendingProduct->status = self::STATUS_REJECT;
        return $pendingProduct->save();
    }
}