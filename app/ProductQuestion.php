<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProductQuestion extends Model
{
    const STATUS_PUBLISHED = 1;
    const STATUS_CANCELLED = 2;
    const STATUS_BLOCKED = 3;
    protected $table = 'product_questions';
    protected $primaryKey = 'product_questions_id';
    protected $fillable = array(
        'product_services_id',
        'user_id',
        'question_description',
        'anonymous',
        'created_at',
        'updated_at'
    );
    public $timestamps = true;

    public function getTotalCount(){
        return self::count();
    }

    public function getCountByDate($start ,$end){
        return DB::table('product_questions')->select(DB::raw('DATE(created_at) as period'), DB::raw('count(product_questions_id) as questions'))->where([['created_at', '>=', $start],['created_at', '<=', $end]])->groupBy('period')->get();
    }

    public function getFullList($status="",$start="",$end=""){
        if(($start != "") && ($end != "")){
            if($status != ""){
                return DB::table('product_questions')->select('product_questions.*','users.first_name','users.last_name','users.email','product_services.product_service_title','product_services.product_slug')->leftJoin('product_services','product_questions.product_services_id','product_services.product_services_id')->join('users','product_questions.user_id','users.user_id')->where([['product_questions.created_at', '>=', $start],['product_questions.created_at', '<=', $end],['product_questions.status',$status]])->get();
            }else{
                return DB::table('product_questions')->select('product_questions.*','users.first_name','users.last_name','users.email','product_services.product_service_title','product_services.product_slug')->leftJoin('product_services','product_questions.product_services_id','product_services.product_services_id')->join('users','product_questions.user_id','users.user_id')->where([['product_questions.created_at', '>=', $start],['product_questions.created_at', '<=', $end]])->get();
            }
        }else if($status != ""){
            return DB::table('product_questions')->select('product_questions.*','users.first_name','users.last_name','users.email','product_services.product_service_title','product_services.product_slug')->leftJoin('product_services','product_questions.product_services_id','product_services.product_services_id')->join('users','product_questions.user_id','users.user_id')->where('product_questions.status', $status)->get();
        }else{
            return DB::table('product_questions')->select('product_questions.*','users.first_name','users.last_name','users.email','product_services.product_service_title','product_services.product_slug')->leftJoin('product_services','product_questions.product_services_id','product_services.product_services_id')->join('users','product_questions.user_id','users.user_id')->get();
        }
    }

    public function getCountByStatus(){
        return DB::table('product_questions')->select('status', DB::raw('count(`product_questions_id`) as total'))->groupBy('status')->get();
    }
}
