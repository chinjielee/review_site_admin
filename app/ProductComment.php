<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProductComment extends Model
{
    const STATUS_PUBLISHED = 1;
    const STATUS_CANCELLED = 2;
    const STATUS_BLOCKED = 3;
    protected $table = 'product_answers';
    protected $primaryKey = 'product_answers_id';
    protected $fillable = array(
        'product_questions_id',
        'user_id',
        'answer',
        'anonymous',
        'status',
        'created_at',
        'updated_at'
    );
    public $timestamps = true;

    public function getTotalCount(){
        return self::count();
    }

    public function getCountByDate($start ,$end){
        return DB::table('product_answers')->select(DB::raw('DATE(created_at) as period'), DB::raw('count(product_answers_id) as comments'))->where([['created_at', '>=', $start],['created_at', '<=', $end]])->groupBy('period')->get();
    }

    public function getFullList($status="",$start="",$end=""){
        if(($start != "") && ($end != "")){
            if($status != ""){
                return DB::table('product_answers')->select('product_answers.*','users.first_name','users.last_name','users.email','product_services.product_service_title','product_services.product_slug','product_questions.question_description')->leftJoin('product_questions','product_answers.product_questions_id','product_questions.product_questions_id')->leftJoin('product_services','product_questions.product_services_id','product_services.product_services_id')->join('users','product_answers.user_id','users.user_id')->where([['product_answers.created_at', '>=', $start],['product_answers.created_at', '<=', $end],['product_answers.status',$status]])->get();
            }else{
                return DB::table('product_answers')->select('product_answers.*','users.first_name','users.last_name','users.email','product_services.product_service_title','product_services.product_slug','product_questions.question_description')->leftJoin('product_questions','product_answers.product_questions_id','product_questions.product_questions_id')->leftJoin('product_services','product_questions.product_services_id','product_services.product_services_id')->join('users','product_answers.user_id','users.user_id')->where([['product_answers.created_at', '>=', $start],['product_answers.created_at', '<=', $end]])->get();
            }
        }else if($status != ""){
            return DB::table('product_answers')->select('product_answers.*','users.first_name','users.last_name','users.email','product_services.product_service_title','product_services.product_slug','product_questions.question_description')->leftJoin('product_questions','product_answers.product_questions_id','product_questions.product_questions_id')->leftJoin('product_services','product_questions.product_services_id','product_services.product_services_id')->join('users','product_answers.user_id','users.user_id')->where('product_answers.status', $status)->get();
        }else{
            return DB::table('product_answers')->select('product_answers.*','users.first_name','users.last_name','users.email','product_services.product_service_title','product_services.product_slug','product_questions.question_description')->leftJoin('product_questions','product_answers.product_questions_id','product_questions.product_questions_id')->leftJoin('product_services','product_questions.product_services_id','product_services.product_services_id')->join('users','product_answers.user_id','users.user_id')->get();
        }
    }

    public function getCountByStatus(){
        return DB::table('product_answers')->select('status', DB::raw('count(`product_answers_id`) as total'))->groupBy('status')->get();
    }
}
