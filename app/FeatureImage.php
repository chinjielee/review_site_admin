<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureImage extends Model
{
    protected $fillable = [
        'details', 'created_at', 'updated_at'
    ];
    protected $primaryKey = 'feature_image_id';
    protected $table = 'feature_image';
}
