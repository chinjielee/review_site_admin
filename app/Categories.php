<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Categories extends Model
{
    //
    const MAX_LIST_PER_LABEL = 100;
    const PRODUCT_ID = 1;
    const SERVICE_ID = 2;
    const MOVIE_ID = 3;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    const IMAGE_PATH = 'images/categories';
    protected $fillable = [
        'categories_description','categories_name','category_slug','level','parent_category_id', 'created_at', 'updated_at','status'
    ];
    protected $primaryKey = 'categories_id';
    protected $table = 'categories';

    private $jsonLabelList = "";

    public function getCategoryListWithCount($start="", $end="") {
        if(($start != "") && ($end != "")){
            return self::selectRaw('categories.*,count(product_services.product_services_id) as total')
                ->leftjoin('product_services', 'product_services.category_id', '=', 'categories.categories_id')
                ->where('categories.level','>','0')
                ->where('categories.status','1')
                ->where([['categories.created_at', '>=', $start],['categories.created_at', '<=', $end]])
                ->groupBy('categories.categories_id')
                ->orderBy('categories.categories_name','ASC')
                ->get();

        } else {
            return self::selectRaw('categories.*,count(product_services.product_services_id) as total')
            ->leftjoin('product_services', 'product_services.category_id', '=', 'categories.categories_id')
            ->where('categories.level','>','0')
            ->where('categories.status','1')
            ->groupBy('categories.categories_id')
            ->orderBy('categories.categories_name','ASC')
            ->get();
        }
    }

    public function getCategoryList() {
        return self::where('status','1')
                ->get();
    }


    public function getCategoriesLabel($category_id) {
        if(empty($this->jsonLabelList)){
            $result = self::selectRaw('categories.categories_id,c1.category_slug as label_1, c2.category_slug as label_2, c1.categories_id as label_id_1, c2.categories_id as label_id_2')
            ->leftjoin('categories as c1', 'categories.parent_category_id', '=', 'c1.categories_id')
            ->leftjoin('categories as c2', 'c1.parent_category_id', '=', 'c2.categories_id')
            ->where('categories.status','1')
            ->get()->keyBy('categories_id')->toJson();
            $this->jsonLabelList = json_decode($result);
            $category_id = $category_id."";
            if(isset($this->jsonLabelList->$category_id)){
                return $this->jsonLabelList->$category_id;
            } else {
                return 0;
            }
        } else {
            if(isset($this->jsonLabelList->$category_id)){
                return $this->jsonLabelList->$category_id;
            } else {
                return 0;
            }
            
        }
    }

    public function getCategoryTopExplorelist()
    {
        $arrCategoryList = array();
        $arrCategoryList['product'] = self::where('parent_category_id', '1')->where('level','1')->limit(self::MAX_LIST_PER_LABEL)->get();
        $arrCategoryList['service'] = self::where('parent_category_id', '2')->where('level','1')->limit(self::MAX_LIST_PER_LABEL)->get();
        $arrCategoryList['movie'] = self::where('parent_category_id', '3')->where('level','1')->limit(self::MAX_LIST_PER_LABEL)->get();
        return $arrCategoryList;
    }

    public function getLabelList() {
        return self::select('categories_id','categories_name')->where('level','0')->where('status','1')->get()->keyBy('categories_id');
    }

    public function getCategoryGroupByLabel($label = "all")
    {
        $arrCategoryList = array();
        switch($label) {
            case 'all':
                // SELECT * FROM categories WHERE categories_id = 1 OR parent_category_id =1 OR parent_category_id IN (SELECT categories_id FROM categories WHERE parent_category_id = 1) ORDER BY `category_slug` ASC
                $arrCategoryList['product'] = self::where('status',self::STATUS_ACTIVE)
                    ->where(function($query){
                        $query->where('categories_id',self::PRODUCT_ID)
                        ->orWhere('parent_category_id', self::PRODUCT_ID)
                        ->orWhereIn('parent_category_id',function($query)
                        {
                            $query->select('categories_id')
                                ->from('categories')
                                ->where('parent_category_id',self::PRODUCT_ID);
                        });
                    })
                    ->get();    
                $arrCategoryList['service'] = self::where('status',self::STATUS_ACTIVE)
                    ->where(function($query){
                        $query->where('categories_id',self::SERVICE_ID)
                        ->orWhere('parent_category_id', self::SERVICE_ID)
                        ->orWhereIn('parent_category_id',function($query)
                        {
                            $query->select('categories_id')
                                ->from('categories')
                                ->where('parent_category_id',self::SERVICE_ID);
                        });
                    })
                    ->get();
                $arrCategoryList['movie'] = self::where('status',self::STATUS_ACTIVE)
                    ->where(function($query){
                        $query->where('categories_id',self::MOVIE_ID)
                        ->orWhere('parent_category_id', self::MOVIE_ID)
                        ->orWhereIn('parent_category_id',function($query)
                        {
                            $query->select('categories_id')
                                ->from('categories')
                                ->where('parent_category_id',self::MOVIE_ID);
                        });
                    })
                    ->get();
                break;
            case 'product':
                $arrCategoryList = self::where('status',self::STATUS_ACTIVE)
                    ->where(function($query){
                        $query->where('categories_id',self::PRODUCT_ID)
                        ->orWhere('parent_category_id', self::PRODUCT_ID)
                        ->orWhereIn('parent_category_id',function($query)
                        {
                            $query->select('categories_id')
                                ->from('categories')
                                ->where('parent_category_id',self::PRODUCT_ID);
                        });
                    })
                    ->get();
                break;
            case 'service':
                $arrCategoryList = self::where('status',self::STATUS_ACTIVE)
                    ->where(function($query){
                        $query->where('categories_id',self::SERVICE_ID)
                        ->orWhere('parent_category_id', self::SERVICE_ID)
                        ->orWhereIn('parent_category_id',function($query)
                        {
                            $query->select('categories_id')
                                ->from('categories')
                                ->where('parent_category_id',self::SERVICE_ID);
                        });
                    })
                    ->get();
                break;
            case 'movie':
                $arrCategoryList = self::where('status',self::STATUS_ACTIVE)
                    ->where(function($query){
                        $query->where('categories_id',self::MOVIE_ID)
                        ->orWhere('parent_category_id', self::MOVIE_ID)
                        ->orWhereIn('parent_category_id',function($query)
                        {
                            $query->select('categories_id')
                                ->from('categories')
                                ->where('parent_category_id',self::MOVIE_ID);
                        });
                    })
                    ->get();
                break;
            default:
                 // SELECT * FROM categories WHERE categories_id = 1 OR parent_category_id =1 OR parent_category_id IN (SELECT categories_id FROM categories WHERE parent_category_id = 1) ORDER BY `category_slug` ASC
                 $arrCategoryList['product'] = self::where('status',self::STATUS_ACTIVE)
                    ->where(function($query){
                        $query->where('categories_id',self::PRODUCT_ID)
                        ->where(function($query){
                            $query->orWhere('parent_category_id', self::PRODUCT_ID)
                            ->orWhereIn('parent_category_id',function($query)
                            {
                                $query->select('categories_id')
                                    ->from('categories')
                                    ->where('parent_category_id',self::PRODUCT_ID);
                            });
                        });
                    })
                    ->get();    
                $arrCategoryList['service'] = self::where('status',self::STATUS_ACTIVE)
                    ->where(function($query){
                        $query->where('categories_id',self::SERVICE_ID)
                        ->where('status',self::STATUS_ACTIVE)
                        ->orWhere('parent_category_id', self::SERVICE_ID)
                        ->where(function($query){
                            $query->orWhereIn('parent_category_id',function($query)
                            {
                                $query->select('categories_id')
                                    ->from('categories')
                                    ->where('parent_category_id',self::SERVICE_ID);
                            });
                        });
                    })
                    ->get();
                $arrCategoryList['movie'] = self::where('status',self::STATUS_ACTIVE)
                    ->where(function($query){
                        $query->where('categories_id',self::MOVIE_ID)
                        ->where('status',self::STATUS_ACTIVE)
                        ->where(function($query){
                            $query->orWhere('parent_category_id', self::MOVIE_ID)
                                ->orWhereIn('parent_category_id',function($query)
                                {
                                    $query->select('categories_id')
                                        ->from('categories')
                                        ->where('parent_category_id',self::MOVIE_ID);
                                });
                        });
                    })
                    ->get();
                break;
        }
        return $arrCategoryList;   
    }

    public function getMultiLayerCategoryBySlug($slug){
        return self::select('categories.*', 'c2.category_slug as parent_cat_1', 'c3.category_slug as parent_cat_2','c1.parent_category_id as parent_cat_1_id','c2.parent_category_id as parent_cat_2_id')
        ->leftjoin('categories as c1', 'categories.parent_category_id', '=', 'c1.categories_id')
        ->leftjoin('categories as c2', 'c1.parent_category_id', '=', 'c2.categories_id')
        ->leftjoin('categories as c3', 'c2.parent_category_id', '=', 'c3.categories_id')
        ->where('categories.category_slug',$slug)->where('categories.status','1')
        ->get()->first();
    }

    public function getCategoriesDetailBySlug($slug){
        return self::where('category_slug',$slug)->where('status','1')->first();
    }
    
    public function del($cid){
        return self::whereIn('categories_id', $cid)
                    ->update(['status'=>self::STATUS_DEACTIVE]);
    }

    /**
    * Save and move to saved images from temp storage.
    *
    * @param Request $request
    */
    public function categoriesImageSave($adminId, $cId)
    {
        if(empty($adminId) || empty($cId)) {
             return false;
        } else {
            $image_path = public_path(self::IMAGE_PATH);
            $displayCategoriesId = base64_encode($cId);
            $des_image_path = $image_path."/".$displayCategoriesId."/";
            $temp_image_path = $image_path."/temp/".$adminId;
            $strImage = '';
            $strReviewImage = '';
            if (file_exists($temp_image_path)) {
                foreach (scandir($temp_image_path) as $file) {
                    if ('.' === $file) continue;
                    if ('..' === $file) continue;
                    $strImage .= ",".$file;
                    $this->_clearCategoriesImage($displayCategoriesId);
                    if(file_exists($des_image_path)) {
                        rename($temp_image_path."/".$file,$des_image_path.$file);
                    } else {
                        mkdir($des_image_path, 0777, true);
                        rename($temp_image_path."/".$file,$des_image_path.$file);
                    }
                }
            }

            // Update Product Review Image
            $strImage = substr($strImage,1);
            return $this->_updateImagePath($strImage, $cId);
         }
    }

    public function columnMapping($arrInput){
        $returnInput = array();
        if(!empty($arrInput['cid'])){
            $returnInput['categories_id'] = $arrInput['cid'];
        }
        $returnInput['categories_description'] = isset($arrInput['name'])?$arrInput['name'] :"";
        $returnInput['categories_name'] = isset($arrInput['name'])?$arrInput['name'] :"";
        $returnInput['category_slug'] = isset($arrInput['name'])?strtolower(str_replace(" ", "-", $arrInput['name'])."-".$arrInput['parent_cat_root']) :"";
        $returnInput['category_slug'] = preg_replace('/[^A-Za-z0-9\-]/', '', $returnInput['category_slug']); 
        // Logic for parent_category_id , level 
        if(!empty($arrInput['parent_cat'])){
            $returnInput['parent_category_id'] = $arrInput['parent_cat'];
            $returnInput['level'] = 2;
        } elseif (!empty($arrInput['parent_cat_root'])) {
            $returnInput['parent_category_id'] = $arrInput['parent_cat_root'];
            $returnInput['level'] = 1;
        } else {
            $returnInput['parent_category_id'] = 0 ;
            $returnInput['level'] = 0;
        }
        $returnInput['status'] = self::STATUS_ACTIVE;
        $returnInput['created_at'] = Carbon::now();
        $returnInput['updated_at'] = Carbon::now();
        return $returnInput;
    }

    public function updCreateCategory($arrInput) {
        if(isset($arrInput['categories_id'])){
            return self::updateOrCreate(['categories_id' => $arrInput['categories_id']], $arrInput)->categories_id;
        } else {
            return self::updateOrCreate($arrInput)->categories_id;
        }
    }

    public function preProcessCategoriesForSelect($categories){
        $newArray = array();
        foreach($categories as $cat){
            if(!in_array($cat->categories_id,[1,2,3])){
                $objLabel = $this->getCategoriesLabel($cat->categories_id);
                $strLabel = "";
                // get display label name 
                if(!empty($objLabel->label_2)){
                    $strLabel = $objLabel->label_2;
                } else {
                    $strLabel = $objLabel->label_1;
                }
                $cat->label = $objLabel;
                if(!isset($newArray[$strLabel])) {
                    $newArray[$strLabel] = array();
                }
                array_push($newArray[$strLabel],$cat);
            }
        }
        return $newArray;
    }

    private function _clearCategoriesImage($displayCid){
        $path = public_path('images/categories/'.$displayCid.'/');
        $this->_rrmdir($path);
    }

    private function _rrmdir($dir) {
        if (is_dir($dir)) {
          $objects = scandir($dir); 
          foreach ($objects as $object) { 
            if ($object != "." && $object != "..") {
              if (is_dir($dir."/".$object))
                $this->_rrmdir($dir."/".$object);
              else
                unlink($dir."/".$object);
            } 
          }
          rmdir($dir); 
        } 
    }

    private function _updateImagePath($strImage, $cId) {
        $catRow = self::find($cId);
        $catRow->image = $strImage;
        return $catRow->save();
    }

}
