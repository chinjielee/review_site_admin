<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;
use URL;
use App\Admin;
use App\Http\Model\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        if (env('APP_ENV') === 'production') {
            URL::forceScheme('https');
        }

        view()->composer('*', function ($view) 
        {
            $user = "";
            $share = (object)array();
            $share->total = 0;
            if(Auth::user()){
                $user = User::find(Admin::HARDCODE_USER_ID);
                $total = $user->unreadNotifications->count();
                $share->total = $total;
            }
            $view->with('share', $share ); 
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
