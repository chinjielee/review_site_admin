<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class DiscussStat extends Model
{
    //
    protected $table = 'stat_most_discuss';
    protected $primaryKey = 'stat_most_discuss_id';
    protected $timestamp = true;
    protected $fillable = [
        'product_service_id',
        'product_service_title',
        'categories_id',
        'day_month_year',
        'created_at',
        'updated_at'
    ];

    public function updateInsertTopDiscussProduct() {
        // $insertQuery = "INSERT INTO `stat_most_discuss` (`product_service_id`,`product_service_title`,`categories_id`,`day_month_year`,`created_at`,`updated_at`) SELECT `product_service_id`,`product_service_title`,`categories_id`,`day_month_year`,NOW(),NOW() FROM `discuss_log` GROUP BY product_service_id ORDER BY COUNT(`discuss_log_id`) DESC LIMIT 15";
        // $select = DB::table('discuss_log')->selectRaw("`product_service_id`,`product_service_title`,`categories_id`,`day_month_year`,NOW(),NOW()")->orderByRaw("COUNT(`discuss_log_id`) DESC")->limit(15);
        // DB::insertUsing(['product_service_id','product_service_title','categories_id','day_month_year','created_at','updated_at'], $select);
        return DB::statement('INSERT INTO `stat_most_discuss` (`product_service_id`,`product_service_title`,`categories_id`,`day_month_year`,`created_at`,`updated_at`) SELECT `product_service_id`,`product_service_title`,`categories_id`,`day_month_year`,NOW(),NOW() FROM `discuss_log` GROUP BY categories_id ORDER BY COUNT(`discuss_log_id`) DESC LIMIT 15');
    }


        // $select = User::where(...)
        //     ->where(...)
        //     ->whereIn(...)
        //     ->select(array('email','moneyOwing'));
        // /**
        // * get the binding parameters
        // **/ 
        // $bindings = $select->getBindings();
        // /**
        // * now go down to the "Network Layer"
        // * and do a hard coded select, Laravel is a little
        // * stupid here
        // */
        // $insertQuery = 'INSERT into user_debt_collection (email,dinero) '
        // . $select->toSql();

        // \DB::insert($insertQuery, $bindings);
}
